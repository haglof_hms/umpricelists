// PricelistsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMPricelists.h"
#include "PricelistsView.h"
#include "PricelistsFormView.h"

#include "AvgAssortFormView.h"

#include "ResLangFileReader.h"

// CPricelistsFormView

IMPLEMENT_DYNCREATE(CPricelistsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPricelistsFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEACTIVATE()
	ON_NOTIFY(NM_KEYDOWN, IDC_ASSORTMENTS_1, OnAssortment1KeyDown)
	ON_NOTIFY(NM_KEYDOWN, IDC_PRICELIST, OnPricelistKeyDown)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_PRICELIST, OnPricelistItemValueChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_ASSORTMENTS_1, OnAssortItemValueChanged)
END_MESSAGE_MAP()

CPricelistsFormView::CPricelistsFormView()
	: CXTResizeFormView(CPricelistsFormView::IDD),
		m_wndSplitter(NUMBER_OF_PANES,SSP_VERT,30,2)

{
	m_bInitialized = FALSE;
	m_bInitializedSplitter = FALSE;
	m_vecQualDesc.clear();
	//m_nOrientation = SSP_VERT;
	m_SplitterPaneSizes[0] = 40;
	m_SplitterPaneSizes[1] = 30;
	m_SplitterPaneSizes[2] = 30;

	m_sAbrevLangSet = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

}

CPricelistsFormView::~CPricelistsFormView()
{
}

void CPricelistsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CPricelistsFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
	m_wndPricelist.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

BOOL CPricelistsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CPricelistsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	int iSel = 0;
	BOOL bResize = FALSE;

	if (m_wndQualDescTabCtrl.GetSafeHwnd() == NULL)
	{
		// Create  the flat tab control.
		if (!m_wndQualDescTabCtrl.Create(WS_CHILD|WS_VISIBLE|FTS_XT_HASARROWS|FTS_XT_HASHOMEEND|WS_TABSTOP, CRect(0,0,0,0), this, ID_TAB_QUALDESC_CTRL))
		{
			TRACE0( "Failed to create flattab control\n" );
			return;
		}
	}	// if (m_wndQualDescTabCtrl.GetSafeHwnd() == NULL)

	if (m_wndAssortSettings.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndAssortSettings.Create(this, IDC_ASSORTMENTS_1,TRUE, FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return;
		}
	}

	if (m_wndPricelist.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndPricelist.Create(this, IDC_PRICELIST,TRUE, FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return;
		}
	}

	if (!	m_bInitialized )
	{
		// set the tab control to have a static edge border.
		m_wndQualDescTabCtrl.ModifyStyleEx(NULL, WS_EX_CLIENTEDGE);
		m_wndAssortSettings.ModifyStyleEx(NULL,WS_EX_CLIENTEDGE);
		m_wndPricelist.ModifyStyleEx(NULL,WS_EX_CLIENTEDGE);

		setupAssortSettings();
		setupPricelistReport();
		setupQualDescReport();

		m_bInitialized = TRUE;

	}	// if (!	m_bInitialized )

	m_wndSplitter.Create(this);
	m_wndSplitter.SetPane(PANE_1_INDEX,&m_wndAssortSettings);
	m_wndSplitter.SetPane(PANE_2_INDEX,&m_wndPricelist);
	m_wndSplitter.SetPane(PANE_3_INDEX,&m_wndQualDescTabCtrl);

}

void CPricelistsFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	if (m_wndSplitter.GetSafeHwnd())
	{
		setResize(&m_wndSplitter,0,0,cx,cy);

		if (!	m_bInitializedSplitter && m_bInitialized)
		{
			m_wndSplitter.SetPaneSizes(m_SplitterPaneSizes);
			m_bInitializedSplitter = TRUE;
		}
	}	// if (m_wndSplitter.GetSafeHwnd())

/*
	RECT rect;
	GetClientRect(&rect);

	if (m_wndAssortSettings.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndAssortSettings,1,1,rect.right - 1,158);
	}

	if (m_wndPricelist.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndPricelist,1,160,rect.right - 1,(rect.bottom - rect.top)/2-50);
	}

	if (m_wndQualDescTabCtrl.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndQualDescTabCtrl,1,(rect.bottom - rect.top)/2+50,rect.right - 1,rect.bottom - ((rect.bottom - rect.top)/2 + 50));
	}
*/
}

int CPricelistsFormView::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CPricelistsFormView::OnAssortment1KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;
/*
	if (lpNMKey->nVKey == VK_DOWN || lpNMKey->nVKey == VK_TAB)
	{

		// Get Rows and Columns already in m_wndPricelist and use
		// this to check if user's at the end of a column and that he's
		// on the last row, before adding a new row; 070618 p�d
		// OBS! Also check that user have entered something in the first
		// column. i.e he must add a name of the Assortment; 070618 p�d
		CXTPReportRows *pRows = m_wndAssortSettings.GetRows();
		CXTPReportColumns *pCols = m_wndAssortSettings.GetColumns();
		CXTPReportRecords *pRecs = m_wndAssortSettings.GetRecords();
		if (pCols != NULL && pRows != NULL && pRecs != NULL)
		{
			CXTPReportColumn *pCol = m_wndAssortSettings.GetFocusedColumn();
			CXTPReportRow *pRow = m_wndAssortSettings.GetFocusedRow();
			if (pCol != NULL && pRow != NULL)
			{
				CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecs->GetAt(pRow->GetIndex());
				if (pRec != NULL)
				{
//				if (pCol->GetIndex() == 0 /*pCols->GetCount() - 1* / && 
//						pRow->GetIndex() == pRows->GetCount() - 1 &&
//						pRec->getColumnText(0) != _T(""))
					if (pRec->getColumnText(0) != _T("") && 
							pRow->GetIndex() == pRows->GetCount()-1)
					{
						addEmptyRowToSelectedReportPage();			
					}	// if (pCol->GetIndex() == pCols->GetCount() - 1)
				}	// if (pCol != NULL)
			}	// if (pCol != NULL && pRow != NULL)
			else
			{
				addEmptyRowToSelectedReportPage();			
			}
		}	// if (pCols != NULL)

	}	// if (lpNMKey->nVKey == VK_TAB && GetKeySTate(VK_SHIFT) == 0)
	
	if (lpNMKey->nVKey == VK_DELETE || 
			lpNMKey->nVKey == VK_F4) // || 
//			lpNMKey->nVKey == VK_UP)
	{
		delRow();
		m_wndAssortSettings.SetFocus();
	}	// if (lpNMKey->nVKey == VK_TAB && GetKeySTate(VK_SHIFT) == 1)
	*/
}

void CPricelistsFormView::OnPricelistKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	// TAB key'll result in a new Row, if a name of quality
	// is added on previous row;  071206 p�d
/*
	if (lpNMKey->nVKey == VK_DOWN)
	{

		// Get Rows and Columns already in m_wndPricelist and use
		// this to check if user's at the end of a column and that he's
		// on the last row, before adding a new row; 070618 p�d
		CXTPReportRows *pRows = m_wndPricelist.GetRows();
		CXTPReportColumns *pCols = m_wndPricelist.GetColumns();
		CXTPReportRecords *pRecs = m_wndPricelist.GetRecords();
		if (pCols != NULL && pRows != NULL && pRecs != NULL)
		{
			CXTPReportColumn *pCol = m_wndPricelist.GetFocusedColumn();
			CXTPReportRow *pRow = m_wndPricelist.GetFocusedRow();
			CPricelistReportDataRec *pRec =	(CPricelistReportDataRec *)pRecs->GetAt(pRow->GetIndex());
			if (pCol != NULL && pRow != NULL && pRec != NULL)
			{
//				if (pCol->GetIndex() == 0 /*pCols->GetCount() - 1* / && 
//						pRow->GetIndex() == pRows->GetCount() - 1 &&
//						pRec->getColumnText(0) != _T(""))
				if (pRec->getColumnText(COLUMN_0) != _T("") && 
						pRow->GetIndex() == pRows->GetCount()-1)
				{
					addEmptyRowToSelectedReportPage();			
					m_wndPricelist.setIsDirty( TRUE );
				}	// if (pCol->GetIndex() == pCols->GetCount() - 1)
			}	// if (pCol != NULL)
		}	// if (pCols != NULL)

	}	// if (lpNMKey->nVKey == VK_TAB)

	if (lpNMKey->nVKey == VK_DELETE || 
		  lpNMKey->nVKey == VK_F4) // || 
//			lpNMKey->nVKey == VK_UP)
	{
		delRow();
		m_wndPricelist.SetFocus();
	}	// if ((lpNMKey->nVKey == VK_DELETE && GetKeyState(VK_SHIFT) == 1) || lpNMKey->nVKey == VK_F4 )
*/
}

void CPricelistsFormView::OnPricelistItemValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		// Only do this check if we are on Column 0, name of quality; 071206 p�d
		if (pItemNotify->pColumn->GetItemIndex() == 0)
		{
			CXTPReportRecords *pRecs = m_wndPricelist.GetRecords();
			if (pRecs != NULL)
			{
				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CPricelistReportDataRec *pPrlRec = (CPricelistReportDataRec *)pRecs->GetAt(i);
					if (pPrlRec != NULL)
					{
						for (UINT q = 0;q < m_vecQualDesc.size();q++)
						{
							CXTPReportControl *pRep = m_vecQualDesc[q];
							if (pRep != NULL)
							{
								CXTPReportRows *pQRows = pRep->GetRows();

								if (pQRows != NULL)
								{
									CXTPReportRow *pQRow = pQRows->GetAt(i);
									if (pQRow != NULL)
									{
										CQualDescReportDataRec *pQRec = (CQualDescReportDataRec *)pQRow->GetRecord();
										if (pQRec != NULL)
										{
											pQRec->setColumnText(0,pPrlRec->getColumnText(COLUMN_0));
										}	// if (pQRec != NULL)
									}	// if (pQRow != NULL)
								}	// if (pQRows != NULL)

								pRep->Populate();
								pRep->UpdateWindow();
							}	// if (pRep != NULL	)
						}	// for (UINT q = 0;q < m_vecQualDesc.size();q++)
					}	// if (pPrlRec != NULL)
				}	// for (int i = 0;i < pRecs->GetCount();i++)
			}	// if (pRecs != NULL)
		}	// if (pItemNotify->pColumn->GetItemIndex() == 1)
		m_wndPricelist.setIsDirty( TRUE );

	}	// if (pItemNotify != NULL)
}

void CPricelistsFormView::OnAssortItemValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bDiamOK = TRUE;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		// Only do this check if we are on Column 2; 071204 p�d
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_2)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pItemNotify->pItem->GetRecord();
			int nAssortTypeIndex = findPulpWoodTypeIndexAsInt(pRec->getColumnsInText(COLUMN_1));
			if (nAssortTypeIndex > -1)
			{
				double fMinDiam = pRec->getColumnFloat(COLUMN_2);
				if (nAssortTypeIndex == 0)	// "Timmer"
				{
					if (fMinDiam < ASSORTMENT_TIMBER_MINDIAM || fMinDiam > ASSORTMENT_TIMBER_MAXDIAM)
					{
						bDiamOK = FALSE;
					}
				}
				else	// Pulpwood
				{
					if (fMinDiam < ASSORTMENT_PULPWOOD_MINDIAM || fMinDiam > ASSORTMENT_PULPWOOD_MAXDIAM)
					{
						bDiamOK = FALSE;
					}
				}
				// Tell user that he has entered a value outside the bouderies for
				// Timber and Pulpwood min,max diamters; 071204 p�d
				if (!bDiamOK)
				{
					::MessageBox(this->GetSafeHwnd(),m_sMinMaxDiameterMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					bDiamOK = TRUE;
					m_wndAssortSettings.SetFocus();
				}
			}	// if (nAssortTypeIndex > -1)
		}	// if (pItemNotify->pColumn->GetItemIndex() == 1)

		//L�gg in en koll h�r s� att sortimentsnamnet inte anv�nds i n�got annat sortiment redan f�r tr�dslaget
		//Kollar b�de d� namnet samt typen �ndrats
		//Bug #2719 20111227 J�
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0 || pItemNotify->pColumn->GetItemIndex() == COLUMN_1)
		{			
				
			CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
			CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
			if (pRecords && pColumns)
			{
				CString strAssName=_T(""),csBuf=_T("");
				int nRow=0,nFound=0;
				//CString csCalcType1=0,csCalcType2=0;
				CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pItemNotify->pItem->GetRecord();
				strAssName=pRec->getColumnText(COLUMN_0);
				//csCalcType1=findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1));
				for (nRow = 0;nRow < pRecords->GetCount();nRow++)
				{
					CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(nRow);
					if (pRec)
					{
						
						if (!pRec->getColumnsInText(COLUMN_0).IsEmpty() || pRecords->GetCount() > 1)
						{
							csBuf=_T("");
							csBuf = pRec->getColumnsInText(COLUMN_0);
							//csCalcType2=findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1));
							//if(strAssName==csBuf && csCalcType1==csCalcType2)
							if(strAssName==csBuf)
								nFound++;

						}	// if (!pRec->getColumnsInText(COLUMN_0).IsEmpty())
					}	// if (pRec)
				}	// for (row = 0;row < pRecords->GetCount();row++)	
				if(nFound>1)
				{
					CString csMsg=_T("");
					csMsg.Format(_T("%s %s %s"),m_sMsgAssName,strAssName,m_sMsgAssName2);
					::MessageBox(this->GetSafeHwnd(),csMsg,m_sMsgAssNameCaption,MB_ICONEXCLAMATION | MB_OK);					
				}
			}
		}

		// HMS-114 kontroll av otill�tna tecken i sortimentsnamn 20250205 J�
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
		{
			CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pItemNotify->pItem->GetRecord();
			CString csBuf=pRec->getColumnText(COLUMN_0);
			if (!csBuf.IsEmpty())
			{
				int nIndex = csBuf.FindOneOf(_T("< > ' \" & /"));
				if (nIndex > -1)
				{
					csBuf.Delete(nIndex);
					do
					{
						nIndex = csBuf.FindOneOf(_T("< > ' \" & /"));
						if (nIndex > -1)
							csBuf.Delete(nIndex);
						if(csBuf.IsEmpty() || csBuf.GetLength()<=0)
							nIndex=0;
					}while(nIndex > -1);
					pRec->setColumnText(COLUMN_0,csBuf);
					CString csMsg=_T("");
					csMsg.Format(_T("%s < > ' \" & /"),m_sMsgCharError);
					::MessageBox(this->GetSafeHwnd(),csMsg,m_sMsgAssNameCaption,MB_ICONEXCLAMATION | MB_OK);	
					
				}
			}
		}
		m_wndAssortSettings.setIsDirty( TRUE );
	}
}

//Lagt in en kollfunktion som returnerar TRUE om n�got sortiment har samma namn som ett annat
//20111227 Bug #2719 J�
BOOL CPricelistsFormView::getAssSameName(CString &csAssName)
{
	CString strAssName=_T(""),csBuf=_T("");
	int nRow=0,nFound=0,nRow2=0;
	//CString csCalcType1=_T(""),csCalcType2=_T("");

	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns)
	{						
		for (nRow = 0;nRow < pRecords->GetCount();nRow++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(nRow);
			if (pRec)
			{
				strAssName=pRec->getColumnsInText(COLUMN_0);
				//csCalcType1=findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1));
				if (!strAssName.IsEmpty() || pRecords->GetCount() > 1)
				{
					nFound=0;
					for (nRow2 = 0;nRow2 < pRecords->GetCount();nRow2++)
					{
						CAssortSettingsDataRec *pRec2 = (CAssortSettingsDataRec*)pRecords->GetAt(nRow2);
						if (pRec2)
						{
							csBuf=pRec2->getColumnsInText(COLUMN_0);
							//csCalcType2=findPulpWoodTypeIndexAsStr(pRec2->getColumnsInText(COLUMN_1));
							if(!csBuf.IsEmpty())
							{
								//if(strAssName==csBuf && csCalcType1==csCalcType2)
								if(strAssName==csBuf)
									nFound++;
							}
						}
					}
					if(nFound>1)
					{
						csAssName=strAssName;
						return TRUE;
					}
				}	// if (!pRec->getColumnsInText(COLUMN_0).IsEmpty())
			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}
	return FALSE;	
}


// CPricelistsFormView diagnostics

#ifdef _DEBUG
void CPricelistsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CPricelistsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// PROTECTED


// Create and add Assortment settings reportwindow
BOOL CPricelistsFormView::setupAssortSettings(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndAssortSettings.GetSafeHwnd() != NULL)
				{
					// Add string data here; 070130 p�d
					m_sMsgCap	= (xml->str(IDS_STRING151));
					m_sDeletePrlAndQDescRow = (xml->str(IDS_STRING152));
					m_sDeleteAssortmentRow = (xml->str(IDS_STRING153));

					m_sMsgCharError.Format(_T("%s"),xml->str(IDS_STRING2100));

					m_sMsgAssName	= (xml->str(IDS_STRING2320));
					m_sMsgAssName2	= (xml->str(IDS_STRING2321));
					m_sMsgAssNameCaption= (xml->str(IDS_STRING127));

					m_sMinMaxDiameterMsg.Format(_T("%s\n%s\n\n%s\n%s"),
						(xml->str(IDS_STRING1580)),
						(xml->str(IDS_STRING1581)),
						(xml->str(IDS_STRING1582)),
						(xml->str(IDS_STRING1583)));

					m_sNotDeleteFirstRowMsg1 = (xml->str(IDS_STRING161));
					m_sNotDeleteFirstRowMsg2 = (xml->str(IDS_STRING162));

					// Add Pulpwood types to this StringArray; 070608 p�d
					m_sarrPulpWoodTypes.RemoveAll();
					m_sarrPulpWoodTypes.Add((xml->str(IDS_STRING1552)));
					m_sarrPulpWoodTypes.Add((xml->str(IDS_STRING1550)));
					m_sarrPulpWoodTypes.Add((xml->str(IDS_STRING1551)));

					m_wndAssortSettings.ShowWindow( SW_NORMAL );
          
					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING127)), 100));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING131)), 100));
					pCol->GetEditOptions()->AddConstraint(m_sarrPulpWoodTypes.GetAt(0),0);
					pCol->GetEditOptions()->AddConstraint(m_sarrPulpWoodTypes.GetAt(1),1);
					pCol->GetEditOptions()->AddConstraint(m_sarrPulpWoodTypes.GetAt(2),2);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING128)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING129)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING130)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					m_wndAssortSettings.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndAssortSettings.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndAssortSettings.GetReportHeader()->AllowColumnResize( FALSE );
					m_wndAssortSettings.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndAssortSettings.GetReportHeader()->SetAutoColumnSizing( FALSE );

					m_wndAssortSettings.SetMultipleSelection( FALSE );
					m_wndAssortSettings.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndAssortSettings.AllowEdit( TRUE );
					m_wndAssortSettings.FocusSubItems(TRUE);

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
//					setResize(&m_wndAssortSettings,1,1,rect.right - 1,158);

				}	// if (m_wndAssortSettings.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

// Create and add Pricelist reportwindow
BOOL CPricelistsFormView::setupPricelistReport(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;


	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndPricelist.GetSafeHwnd() != NULL)
				{
					m_wndPricelist.ShowWindow( SW_NORMAL );
					m_wndPricelist.setDoEditFirstColumn( FALSE );

					pCol = m_wndPricelist.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING108)), 100));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					m_wndPricelist.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndPricelist.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndPricelist.GetReportHeader()->AllowColumnResize( FALSE );
					m_wndPricelist.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndPricelist.GetReportHeader()->SetAutoColumnSizing( FALSE );
					m_wndPricelist.SetMultipleSelection( FALSE );
					m_wndPricelist.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndPricelist.AllowEdit(TRUE);
					m_wndPricelist.FocusSubItems(TRUE);

					RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
//					setResize(&m_wndPricelist,1,160,rect.right - 1,(rect.bottom - rect.top)/2-50);

				}	// if (m_wndPricelist.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;

	}	// if (fileExists(m_sLangFN))
	return TRUE;

}

// Creates a Qualitydescription in m_vecQualDesc vector; 060331 p�d
BOOL CPricelistsFormView::setupAQualDescReportWnd(void)
{
	CXTPReportColumn *pCol = NULL;
	int nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();

	m_vecQualDesc.push_back((CMyReportCtrl *)RUNTIME_CLASS(CMyReportCtrl)->CreateObject());
	UINT nSize = (UINT)m_vecQualDesc.size() - 1;

	if (m_vecQualDesc[nSize]->GetSafeHwnd() == NULL)
	{
		if (!m_vecQualDesc[nSize]->Create(&m_wndQualDescTabCtrl, IDC_QUALDESC,TRUE,TRUE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
		
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				if (m_vecQualDesc[nSize]->GetSafeHwnd() != NULL)
				{

					CXTPReportColumn *pCol = m_vecQualDesc[nSize]->AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING108)), 100));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( TRUE );
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					//pCol->SetFooterText((xml->str(IDS_STRING154)));

					m_vecQualDesc[nSize]->GetReportHeader()->AllowColumnRemove(FALSE);
					m_vecQualDesc[nSize]->GetReportHeader()->AllowColumnReorder(FALSE);
					m_vecQualDesc[nSize]->GetReportHeader()->AllowColumnResize( FALSE );
					m_vecQualDesc[nSize]->GetReportHeader()->AllowColumnSort( FALSE );
					m_vecQualDesc[nSize]->GetReportHeader()->SetAutoColumnSizing( FALSE );

					m_vecQualDesc[nSize]->SetMultipleSelection( FALSE );
					m_vecQualDesc[nSize]->SetGridStyle( TRUE, xtpReportGridSolid );
					m_vecQualDesc[nSize]->SetGridStyle( FALSE, xtpReportGridSolid );
					m_vecQualDesc[nSize]->AllowEdit(TRUE);
					m_vecQualDesc[nSize]->FocusSubItems(TRUE);
					m_vecQualDesc[nSize]->ShowFooter(FALSE);
					m_vecQualDesc[nSize]->setValidateColumns(TRUE,1,100.0);
					m_vecQualDesc[nSize]->setDoEditFirstColumn( FALSE );
					
					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
//					setResize(&m_wndQualDescTabCtrl,1,(rect.bottom - rect.top)/2+50,rect.right - 1,rect.bottom - ((rect.bottom - rect.top)/2 + 50));
				}
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	}

	return TRUE;
}


int CPricelistsFormView::getWhoHasFocus(void)
{

	HWND pFocusWnd = ::GetFocus();
	CXTPReportRecords *pAssortRecs = m_wndAssortSettings.GetRecords();
	CXTPReportRecords *pPricelist = m_wndPricelist.GetRecords();

	if (pAssortRecs->GetCount() == 0 && pPricelist->GetCount() == 0)
		return 0;

	// Check out who has focus; 060304 p�d
	if (pFocusWnd == m_wndAssortSettings.GetSafeHwnd() )
		return 1;

	if (pFocusWnd == m_wndPricelist.GetSafeHwnd() )
		return 2;

	if (m_vecQualDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecQualDesc.size();i++)
		{
			if (pFocusWnd == m_vecQualDesc[i]->GetSafeHwnd() )
				return 3;
		}	// for (UINT i = 0;i < m_vecQualDesc.size();i++)
	}	// if (m_vecQualDesc.size() > 0)

	return -1;
}

// PUBLIC METHOD

BOOL CPricelistsFormView::isQDescDataValid(void)
{
	if (m_vecQualDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecQualDesc.size();i++)
		{
			if (!m_vecQualDesc[i]->isDataValid() )
				return FALSE;
		}	// for (UINT i = 0;i < m_vecQualDesc.size();i++)
	}	// if (m_vecQualDesc.size() > 0)
	
	return TRUE;
}

BOOL CPricelistsFormView::isDirty(void)
{
	if (m_wndPricelist.isDirty() || m_wndAssortSettings.isDirty())
		return TRUE;

	if (m_vecQualDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecQualDesc.size();i++)
		{
			if (m_vecQualDesc[i]->isDirty() )
				return TRUE;
		}	// for (UINT i = 0;i < m_vecQualDesc.size();i++)
	}	// if (m_vecQualDesc.size() > 0)
	
	return FALSE;
}

void CPricelistsFormView::setIsDirty(BOOL dirty)
{
	m_wndPricelist.setIsDirty(dirty);
	m_wndAssortSettings.setIsDirty(dirty);
	if (m_vecQualDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecQualDesc.size();i++)
		{
			m_vecQualDesc[i]->setIsDirty(dirty);
		}	// for (UINT i = 0;i < m_vecQualDesc.size();i++)
	}	// if (m_vecQualDesc.size() > 0)
}

// Creates a qualtiydescription report (setupAQualDescReportWnd() )
// and adds it to the m_wndQualDescTabCtrl CXTExcelTabCtrl; 060331 p�d
BOOL CPricelistsFormView::setupQualDescReport(void)
{
	if (fileExists(m_sLangFN))
	{
		setupAQualDescReportWnd();

		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			addQualDescToTabCtrl((xml->str(IDS_STRING109)));
		}	// if (xml->Load(m_sLangFN))
		delete xml;

	}	// if (fileExists(m_sLangFN))
	
	return TRUE;
}

// Adds a Qualitydescription created in setupQualDesc (setupAQualDescReportWnd())
// to m_wndQualDescTabCtrl CXTExcelTabCtrl; 060331 p�d
BOOL CPricelistsFormView::addQualDescToTabCtrl(LPCTSTR caption)
{
	int nNumOfTabs;
	nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();

	if (m_vecQualDesc[nNumOfTabs]->GetSafeHwnd())
		m_wndQualDescTabCtrl.InsertItem(nNumOfTabs, (caption), m_vecQualDesc[nNumOfTabs]);
		
	m_wndQualDescTabCtrl.SetSelTabBackColor(nNumOfTabs, RGB(0xff,0x00,0x00));
	m_wndQualDescTabCtrl.SetSelTabTextColor(nNumOfTabs, RGB(0xff,0xff,0xff));

	m_wndQualDescTabCtrl.SetWindowPos(&wndTop, 0, 0, 0, 0,
		SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE | SWP_FRAMECHANGED);

	m_wndQualDescTabCtrl.SetCurSel(0);

	return TRUE;
}

// Add an empty row to Assortments or Pricelist report and Qualitydescription(s)
// depending on which window got the focus; 060410 p�d
BOOL CPricelistsFormView::addRow(BOOL clear)
{
	CString sMsg;
	int nRows,nColumns,nWho;
	CXTPReportColumns *pColumnsQualDesc = NULL;
	nWho = getWhoHasFocus();
	if (nWho == 0 || nWho == 2 || nWho == 3)
	{
		CXTPReportColumns *pColumnsPriceList = m_wndPricelist.GetColumns();

		CXTPReportRows *pRowsPricelist = m_wndPricelist.GetRows();

		if (pRowsPricelist != NULL)
		{
			nRows = pRowsPricelist->GetCount();
//			sMsg.Format("%d",nRows+1);
			sMsg.Empty();
		}	// if (pRows)
		if (pColumnsPriceList != NULL)
		{
			nColumns = pColumnsPriceList->GetCount();
			if (clear)
			{
				m_wndPricelist.ClearReport();
				nRows = 0;
//				sMsg.Format("%d",nRows+1);
				sMsg.Empty();
			}

			m_wndPricelist.AddRecord(new CPricelistReportDataRec(nColumns,sMsg));
	
			m_wndPricelist.Populate();
			m_wndPricelist.UpdateWindow();

		}	// if (pColumnsPriceList)

		int	nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
		for (int i = 0;i < nNumOfTabs;i++)
		{
			if (m_vecQualDesc[i]->GetSafeHwnd())
			{
				pColumnsQualDesc = m_vecQualDesc[i]->GetColumns();
				if (pColumnsQualDesc)
				{
					if (clear)
					{
						m_vecQualDesc[i]->ClearReport();
						nRows = 0;
//						sMsg.Format("%d",nRows+1);
					}

//					sMsg.Format("%d",nRows+1);
					sMsg.Empty();
					m_vecQualDesc[i]->AddRecord(new CQualDescReportDataRec(pColumnsQualDesc->GetCount(),sMsg));

					m_vecQualDesc[i]->Populate();
					m_vecQualDesc[i]->UpdateWindow();
				}	// if (pColumnsQualDesc)
			}	// if (m_vecQualDesc[i]->GetSafeHwnd())
		}	// for (int i = 0;i < nNumOfTabs;i++)
	}
	
	if (nWho == 0 || nWho == 1)
	{
		if (m_wndAssortSettings.GetSafeHwnd())
		{
			m_wndAssortSettings.AddRecord(new CAssortSettingsDataRec());
			m_wndAssortSettings.Populate();
			m_wndAssortSettings.UpdateWindow();
		}	// if (m_wndAssortSettings.GetSafeHwnd())
	}	// else if (nWho == 1)

	return TRUE;
}

// Add one row to pricelist, based on data collected from database; 060411 p�d
BOOL CPricelistsFormView::addPricelistRow(CTransaction_prl_data &rec)
{
		CXTPReportColumns *pColumnsPriceList = m_wndPricelist.GetColumns();

		if (pColumnsPriceList)
		{
			m_wndPricelist.AddRecord(new CPricelistReportDataRec(rec));
	
			m_wndPricelist.Populate();
			m_wndPricelist.UpdateWindow();
			return TRUE;
		}	// if (pColumnsPriceList)
		return FALSE;
}

BOOL CPricelistsFormView::addQualDescRow(CTransaction_prl_data &rec)
{
	UINT nIdentifer = rec.getIdentifer();
	CXTPReportColumns *pColumnsQualDesc = NULL;
	if (nIdentifer >= 0 && nIdentifer < m_vecQualDesc.size())
	{
		if (m_vecQualDesc[nIdentifer]->GetSafeHwnd())
		{
			m_vecQualDesc[nIdentifer]->AddRecord(new CQualDescReportDataRec(rec));

			m_vecQualDesc[nIdentifer]->Populate();
			m_vecQualDesc[nIdentifer]->UpdateWindow();

			return TRUE;
		}	// if (m_vecQualDesc[i]->GetSafeHwnd())
	}
	return FALSE;
}

// Reads already enterd data in Records, creates new Record(s), if e.g. diam.classes
// added to Pricelist and Qualitydesc. ; 060410 p�d
BOOL CPricelistsFormView::updRows(void)
{
	int nColumns,row,col;
	// Pricelist
	CPricelistReportDataRec *pOldPricelistRec = NULL;
	CPricelistReportDataRec *pNewPricelistRec = NULL;
	CXTPReportColumns *pColumnsPriceList = m_wndPricelist.GetColumns();
	CXTPReportRecords *pPricelistRecords = m_wndPricelist.GetRecords();
	// Quality descriptions
	CQualDescReportDataRec *pOldQualDescRec = NULL;
	CQualDescReportDataRec *pNewQualDescRec = NULL;
	CXTPReportRecords *pQualDescRecords = NULL;

	// Get number of columns. OBS! Same number of columns for Quality description also; 060405 p�d
	nColumns = pColumnsPriceList->GetCount();
	if (pPricelistRecords)
	{
		for (row = 0;row < pPricelistRecords->GetCount();row++)
		{
			// Get data already entered; 060405 p�d
			pOldPricelistRec = (CPricelistReportDataRec*)pPricelistRecords->GetAt(row);
			// Create a new record, holds new columns added (if any); 060405 p�d
			pNewPricelistRec = new CPricelistReportDataRec(nColumns);
			// Add data from old record to new record, for columns added earlier.
			// If new column(s) added a zero "0" is added to the column item(s); 060405 p�d
			for (col = 0;col < nColumns;col++)
			{
				if (col == 0)
					pNewPricelistRec->setColumnText(col,pOldPricelistRec->getColumnText(col));
				else
					pNewPricelistRec->setColumnInt(col,pOldPricelistRec->getColumnInt(col));
			}
			// Remove old record from list of records; 060405 p�d
			pPricelistRecords->RemoveAt(row);
			// Add new column to list of records; 060405 p�d
			pPricelistRecords->InsertAt(row, pNewPricelistRec);
		}	// for (int i = 0;i < pRecords->GetCount();i++)
		m_wndPricelist.Populate();
		m_wndPricelist.UpdateWindow();
	}	// if (pPricelistRecords)

	if (m_vecQualDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecQualDesc.size();i++)
		{
			// Get records in quality descrition
			pQualDescRecords = m_vecQualDesc[i]->GetRecords();

			if (pQualDescRecords)
			{
				for (row = 0;row < pQualDescRecords->GetCount();row++)
				{
					// Get data already entered; 060405 p�d
					pOldQualDescRec = (CQualDescReportDataRec*)pQualDescRecords->GetAt(row);
					// Create a new record, holds new columns added (if any); 060405 p�d
					pNewQualDescRec = new CQualDescReportDataRec(nColumns);
					// Add data from old record to new record, for columns added earlier.
					// If new column(s) added a zero "0" is added to the column item(s); 060405 p�d
					for (col = 0;col < nColumns;col++)
					{
						if (col == 0)
							pNewQualDescRec->setColumnText(col,pOldQualDescRec->getColumnText(col));
						else
							pNewQualDescRec->setColumnInt(col,pOldQualDescRec->getColumnInt(col));
					}
					// Remove old record from list of records; 060405 p�d
					pQualDescRecords->RemoveAt(row);
					// Add new column to list of records; 060405 p�d
					pQualDescRecords->InsertAt(row, pNewQualDescRec);
				}	// for (int i = 0;i < pRecords->GetCount();i++)
				m_vecQualDesc[i]->Populate();
				m_vecQualDesc[i]->UpdateWindow();
			}	// if (pPricelistRecords)

		}	// for (UINT i = 0;i < m_vecQualDesc.size();i++)
	}	// if (m_vecQualDesc.size() > 0)

	
	return TRUE;
}


// Remove a row from Pricelist and Qualitydescription(s); 060331 p�d
BOOL CPricelistsFormView::delRow(void)
{
	int nSelRow = -1;
	int nWho;
	nWho = getWhoHasFocus();

	// Delete Pricelist and Qualitydesc; 070130 p�d
	if (nWho == 0 || nWho == 2 || nWho == 3)
	{
		CXTPReportRecords *pRecsPricelist = m_wndPricelist.GetRecords();
		if (pRecsPricelist)
		{
			if (pRecsPricelist->GetCount() == 1)
			{
				::MessageBox(this->GetSafeHwnd(),m_sNotDeleteFirstRowMsg2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return FALSE;
			}

			if (::MessageBox(0,m_sDeletePrlAndQDescRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				CXTPReportRow *pRow = m_wndPricelist.GetFocusedRow();
				if (pRow == NULL) return FALSE;
				
				nSelRow = pRow->GetIndex();
				pRecsPricelist->RemoveAt(nSelRow);
				m_wndPricelist.Populate();
				m_wndPricelist.UpdateWindow();

				CXTPReportRecords *pRecsQualDesc = NULL;
				int nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
				for (int i = 0;i < nNumOfTabs;i++)
				{
					if (m_vecQualDesc[i]->GetSafeHwnd())
					{
						pRecsQualDesc = m_vecQualDesc[i]->GetRecords();
						if (pRecsQualDesc != NULL)
						{
							pRecsQualDesc->RemoveAt(nSelRow);
							m_vecQualDesc[i]->Populate();
							m_vecQualDesc[i]->UpdateWindow();
						}	// if (pRowsQualDesc)
					}	// if (m_vecQualDesc[i]->GetSafeHwnd())
				}	// for (int i = 0;i < nNumOfTabs;i++)
			}	// if (::MessageBox(0,m_sDeletePrlAndQDescRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		} // if (::MessageBox(0,_T("Ta bort rad fr�n Prislista och kvalitetsbesk. ... ?"),"Meddelande",MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	}	// if (nWho == 0 || nWho == 2 || nWho == 3)
	
	// Delete Assortments; 070130 p�d
	if (nWho == 0 || nWho == 1)
	{
		CXTPReportRecords *pRecsAssortSettings = m_wndAssortSettings.GetRecords();
		if (pRecsAssortSettings)
		{
			if (pRecsAssortSettings->GetCount() == 1)
			{
				::MessageBox(this->GetSafeHwnd(),m_sNotDeleteFirstRowMsg1,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return FALSE;
			}	// if (nRows == 1)
	
			CXTPReportRow *pRow1 = m_wndAssortSettings.GetFocusedRow();
			if (pRow1 == NULL) return FALSE;

			if (::MessageBox(0,m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				pRecsAssortSettings->RemoveAt(pRow1->GetIndex());
				m_wndAssortSettings.Populate();
				m_wndAssortSettings.UpdateWindow();
			}	// if (::MessageBox(0,m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		}	// if (pRecsAssortSettings)
	}
	return TRUE;
}

// Add columns and rows to a quality desc. created after the
// diamterclasses already been set; 060331 p�d
BOOL CPricelistsFormView::addQualDescColumnsAndRows(void)
{
	CString sMsg;
	int nNumOfTabs,nDcls,nStartDiam,nNumOfDCLS;
	CXTPReportColumns *pColumnsQualDesc = NULL;
	CXTPReportColumn *pCol = NULL;
	CXTPReportRecords *pRecsQualDesc = NULL;
	CMyReportCtrl *pReport = NULL;
	CStringArray arrQualNames;

	getQualityNamesSetInPricelist(arrQualNames);


	nDcls = nStartDiam = nNumOfDCLS = 0;

	nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
	if (nNumOfTabs > 1)
	{
		// Get data from Report; First tab; 060331 p�d
		pReport = m_vecQualDesc[0];
		pColumnsQualDesc = pReport->GetColumns();	

		// Add columns
		int nNumOfColumns = pColumnsQualDesc->GetCount();
		for (int j = 1 /*  First column already created */;j < nNumOfColumns;j++)
		{
			if (m_vecQualDesc[nNumOfTabs-1]->GetSafeHwnd())
			{
				pReport->getItemData(j-1,&nDcls,&nStartDiam,&nNumOfDCLS);
				sMsg.Format(_T("%d-"),nDcls);
				pCol = m_vecQualDesc[nNumOfTabs-1]->AddMyColumn(new CXTPReportColumn(j, 
																																	sMsg, COL_WIDTH),
																																	nDcls,
																																	nStartDiam,
																																	nNumOfDCLS);
				pCol->SetHeaderAlignment( DT_RIGHT );
				pCol->SetAlignment( DT_RIGHT );
				pCol->SetWidth( COL_WIDTH );
			}	// if (m_vecQualDesc[j]->GetSafeHwnd())
		}	// for (int j = 0;j < nNumOfTabs;j++)


		// Add rows
		if (m_vecQualDesc[nNumOfTabs-1]->GetSafeHwnd())
		{
			pRecsQualDesc = pReport->GetRecords();
			if (pRecsQualDesc)
			{
				int	nRows = pRecsQualDesc->GetCount();
				if (nRows > 0)
				{
					for (int i = 0;i < nRows;i++)
					{
						if (arrQualNames.GetCount() >= i)
							sMsg.Format(_T("%s"),arrQualNames[i]);
						else
							sMsg.Format(_T("%d"),i+1);

						m_vecQualDesc[nNumOfTabs-1]->AddRecord(new CQualDescReportDataRec(nNumOfColumns,sMsg));
					}	// for (int i = 0;i < nRows;i++)

					m_vecQualDesc[nNumOfTabs-1]->Populate();
					m_vecQualDesc[nNumOfTabs-1]->UpdateWindow();
				}
			}	// if (pRowsQualDesc)
		}	// if (m_vecQualDesc[nNumOfTabs-1]->GetSafeHwnd())
	}	// if (nNumOfTabs > 1)


	return TRUE;
}

// PUBLIC METHOD

// Add columns (diameterclasses) to Pricelist and Qualitydescription(s) for new pricelist
void CPricelistsFormView::setupNewPricelist(CTransaction_diameterclass &rec)
{

	int nDcls = 0;
	int nNumOfColumns = 0;
	int nStartDCLS = 0;
	TCHAR szStr[50];
	CXTPReportColumn* pCol = NULL;

	CXTPReportColumns *pColumns = m_wndPricelist.GetColumns();
	int nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
	if (pColumns)
	{
		nNumOfColumns = pColumns->GetCount();
		if (nNumOfColumns > 1)
		{
			nStartDCLS = 1;
		}	// if (nNumOfColumns > 1)
	}	// if (pColumns)
	for (int i = 0; i < rec.getNumOfDCLS();i++)
	{
		nDcls = rec.getStartDiam() + (nStartDCLS * rec.getDiamClass());
		_stprintf(szStr,_T("%d-"),nDcls);

		pCol = m_wndPricelist.AddMyColumn(new CXTPReportColumn(nNumOfColumns + i, 
																													szStr, COL_WIDTH),
																													nDcls,
																													rec.getDiamClass(),
																													i+1);
		pCol->SetEditable( TRUE );
		pCol->SetHeaderAlignment( DT_RIGHT );
		pCol->SetAlignment( DT_RIGHT );
		pCol->SetWidth( COL_WIDTH );
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

		for (int j = 0;j < nNumOfTabs;j++)
		{
			if (m_vecQualDesc[j]->GetSafeHwnd())
			{
				pCol = m_vecQualDesc[j]->AddMyColumn(new CXTPReportColumn(nNumOfColumns + i, 
																																	szStr, COL_WIDTH),
																																	nDcls,
																																	rec.getDiamClass(),
																																	i+1);
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_RIGHT );
				pCol->SetAlignment( DT_RIGHT );
				pCol->SetWidth( COL_WIDTH );
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

			}	// if (m_vecQualDesc[j]->GetSafeHwnd())
		}	// for (int j = 0;j < nNumOfTabs;j++)

		nStartDCLS++;
	}	// for (int i = 0; i < rec.getNumOfDCLS();i++)

	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if (pFrameWnd)
	{
		CMDIPricelistsFrame *pWnd = (CMDIPricelistsFrame *)pFrameWnd->GetActiveFrame();
		if (pWnd)
		{
			pWnd->m_wndDataEnterDlg.setStartDiam(nDcls);
		}
	}
	// Redraw and update rows; 060405 p�d
	updRows();
}

// Add columns (diameterclasses) to Pricelist and Qualitydescription(s) for open pricelist
void CPricelistsFormView::setupOpenPricelist(CTransaction_diameterclass &rec)
{
	CXTPReportColumn* pCol = NULL;
	int nDcls = 0;
	int nNumOfColumns = 0;
	TCHAR szStr[50];
	CXTPReportColumns *pColumns = m_wndPricelist.GetColumns();
	int nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
	if (pColumns)
	{
		nNumOfColumns = pColumns->GetCount();
	}	// if (pColumns)

	_stprintf(szStr,_T("%d-"),rec.getStartDiam());

	pCol = m_wndPricelist.AddMyColumn(new CXTPReportColumn(nNumOfColumns, 
																													szStr, COL_WIDTH),
																													rec.getStartDiam(),
																													rec.getDiamClass(),
																													rec.getNumOfDCLS());
	pCol->SetEditable( TRUE );
	pCol->SetHeaderAlignment( DT_RIGHT );
	pCol->SetAlignment( DT_RIGHT );
	pCol->SetWidth( COL_WIDTH );

	for (int j = 0;j < nNumOfTabs;j++)
	{
		if (m_vecQualDesc[j]->GetSafeHwnd())
		{
			pCol = m_vecQualDesc[j]->AddMyColumn(new CXTPReportColumn(nNumOfColumns, 
																																szStr, COL_WIDTH),
																																rec.getStartDiam(),
																																rec.getDiamClass(),
																																rec.getNumOfDCLS());
			pCol->SetEditable( TRUE );
			pCol->SetHeaderAlignment( DT_RIGHT );
			pCol->SetAlignment( DT_RIGHT );
			pCol->SetWidth( COL_WIDTH );
		}	// if (m_vecQualDesc[j]->GetSafeHwnd())
	}	// for (int j = 0;j < nNumOfTabs;j++)
	// Redraw and update rows; 060405 p�d
	updRows();
}

void CPricelistsFormView::setupOpenPricelistAddRow(CTransaction_prl_data &rec)
{
	addPricelistRow(rec);
}

void CPricelistsFormView::setupOpenQualDescAddRow(CTransaction_prl_data &rec)
{
	addQualDescRow(rec);
}

// Removes columns from Pricelist and Qualitydescription(s)
void CPricelistsFormView::removePricelistAndQualDesc(void)
{
	CString S;
	int nColIndex = 0;
	CXTPReportColumn *pCol = NULL;
	CXTPReportColumns *pColumnsQualDesc	 = NULL;
	CXTPReportRecords *pRecsQualDesc	= NULL;

	CXTPReportColumns *pColumnsPriceList = m_wndPricelist.GetColumns();
	CXTPReportRecords *pRecsPricelist = m_wndPricelist.GetRecords();

	int nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();

	if (pColumnsPriceList != NULL)
	{
		nColIndex = pColumnsPriceList->GetCount();
		// Don't remove first column (name of qualities); 060324 p�d
		if (nColIndex > 1)
		{
			pCol = pColumnsPriceList->GetAt(nColIndex - 1);
			if (pCol)
			{
				pColumnsPriceList->Remove(pCol);
			}	// if (pCol)
		}	// if (nColIndex > 1)
	

		for (int i = 0;i < nNumOfTabs;i++)
		{
			pColumnsQualDesc = m_vecQualDesc[i]->GetColumns();
			pRecsQualDesc	= m_vecQualDesc[i]->GetRecords();
			nColIndex = pColumnsQualDesc->GetCount();
			// Don't remove first column (name of qualities); 060324 p�d
			if (nColIndex > 1)
			{
				pCol = pColumnsQualDesc->GetAt(nColIndex - 1);
				if (pCol)
				{
					pColumnsQualDesc->Remove(pCol);
				}	// if (pCol)
			}	// if (nColIndex > 1)
		}	// for (int i = 0;i < nNumOfTabs;i++)

		m_wndPricelist.delLastItemData();
		for (int j = 0;j < nNumOfTabs;j++)
		{
			if (m_vecQualDesc[j]->GetSafeHwnd())
			{
				m_vecQualDesc[j]->delLastItemData();
			}	// if (m_vecQualDesc[j]->GetSafeHwnd())
		}	// for (int j = 0;j < nNumOfTabs;j++)
		
		setDataEnterDlgValues();
		setPrlPriceDlgValues();
	}	// if (pColumnsPriceList != NULL)
}

void CPricelistsFormView::resetDiamterClassEntries(void)
{

	// Reset values for Diameterclass etc; 081023 p�d
	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		CXTPDockingPaneManager *pPaneManager = pMDIChild->GetDockingPaneManager();
		if (pPaneManager != NULL)
		{
			CXTPDockingPane *pPane = pPaneManager->FindPane(IDC_SETTINGS_PANE);
			if (pPane != NULL)
			{
				CDataEnterDlg *pDlg = (CDataEnterDlg *)pPane->GetChild();
				if (pDlg != NULL)
				{
					pDlg->setStartDiam(0);
					pDlg->setDiamClass(0);
					pDlg->setNumOfDCLS(0);
				}	// if (pDlg != NULL)
			}	// if (pPane != NULL)
		}	// if (pPaneManager != NULL)
	}	// if (pMDIChild != NULL)
}
// Set data in CDataEnterDlg from Pricelist
void CPricelistsFormView::setDataEnterDlgValues(void)
{
	int	nDCLS,nStart,nNumOf;

	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		nDCLS = nStart = nNumOf = 0;
		m_wndPricelist.getLastItemData(&nDCLS,&nStart,&nNumOf);
		CXTPDockingPaneManager *pPaneManager = pMDIChild->GetDockingPaneManager();
		if (pPaneManager != NULL)
		{
			CXTPDockingPane *pPane = pPaneManager->FindPane(IDC_SETTINGS_PANE);
			if (pPane != NULL)
			{
				CDataEnterDlg *pDlg = (CDataEnterDlg *)pPane->GetChild();
				if (pDlg != NULL)
				{
					pDlg->setStartDiam(nDCLS);
					pDlg->setDiamClass(nStart);
					pDlg->setNumOfDCLS(nNumOf);
				}	// if (pDlg != NULL)
			}	// if (pPane != NULL)
		}	// if (pPaneManager != NULL)
	}	// if (pMDIChild != NULL)
}

// Set data in CDataEnterDlg from Pricelist
void CPricelistsFormView::setPrlPriceDlgValues(void)
{
	int	nDCLS,nStart,nNumOf;

	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		nDCLS = nStart = nNumOf = 0;
		m_wndPricelist.getLastItemData(&nDCLS,&nStart,&nNumOf);
		CXTPDockingPaneManager *pPaneManager = pMDIChild->GetDockingPaneManager();
		if (pPaneManager != NULL)
		{
			CXTPDockingPane *pPane = pPaneManager->FindPane(IDC_CHANGE_PRICE_PANE);
			if (pPane != NULL)
			{
				CChangePrlPriceDlg *pDlg = (CChangePrlPriceDlg *)pPane->GetChild();
				if (pDlg != NULL)
				{
					pDlg->setValue(0);
				}	// if (pDlg != NULL)
			}	// if (pPane != NULL)
		}	// if (pPaneManager != NULL)
	}	// if (pMDIChild != NULL)
}

// Check, what to do whan user clicks the New button in HMSShell alt. F3 or Ctrl+N; 060327 p�d
void CPricelistsFormView::addEmptyRowToSelectedReportPage(void)
{
	addRow(FALSE);
}

// Check, what to do whan user clicks the New button in HMSShell alt. F3 or Ctrl+N; 060327 p�d
void CPricelistsFormView::updRowOnSelectedReportPage(void)
{
	updRows();
}

// Check, what to do whan user clicks the New button in HMSShell alt. F3 or Ctrl+N; 060327 p�d
void CPricelistsFormView::delRowFromSelectedReportPage(void)
{
	delRow();
}

void CPricelistsFormView::addEmptyRowToAssortment(void)
{
	if (m_wndAssortSettings.GetSafeHwnd())
	{
		m_wndAssortSettings.AddRecord(new CAssortSettingsDataRec());
		m_wndAssortSettings.Populate();
		m_wndAssortSettings.UpdateWindow();
	}
}

void CPricelistsFormView::delRowFromAssortment(void)
{
	CXTPReportRecords *pRecsAssortSettings = m_wndAssortSettings.GetRecords();
	if (pRecsAssortSettings)
	{
		if (pRecsAssortSettings->GetCount() == 1)
		{
			::MessageBox(this->GetSafeHwnd(),m_sNotDeleteFirstRowMsg1,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return;
		}	// if (nRows == 1)

		CXTPReportRow *pRow1 = m_wndAssortSettings.GetFocusedRow();
		if (pRow1 == NULL) return;

		if (::MessageBox(0,m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			pRecsAssortSettings->RemoveAt(pRow1->GetIndex());
			m_wndAssortSettings.Populate();
			m_wndAssortSettings.UpdateWindow();
		}	// if (::MessageBox(0,m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	}	// if (pRecsAssortSettings)

}

void CPricelistsFormView::addEmptyRowToPricelistAndQuality(void)
{
	CXTPReportColumns *pColumnsPriceList = m_wndPricelist.GetColumns();
	if (pColumnsPriceList != NULL)
	{
		int	nColumns = pColumnsPriceList->GetCount();
		m_wndPricelist.AddRecord(new CPricelistReportDataRec(nColumns,_T("")));
		m_wndPricelist.Populate();
		m_wndPricelist.UpdateWindow();
	}	// if (pColumnsPriceList)

	int	nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
	for (int i = 0;i < nNumOfTabs;i++)
	{
		if (m_vecQualDesc[i]->GetSafeHwnd())
		{
			CXTPReportColumns *pColumnsQualDesc = pColumnsQualDesc = m_vecQualDesc[i]->GetColumns();
			if (pColumnsQualDesc)
			{
				m_vecQualDesc[i]->AddRecord(new CQualDescReportDataRec(pColumnsQualDesc->GetCount(),_T("")));
				m_vecQualDesc[i]->Populate();
				m_vecQualDesc[i]->UpdateWindow();
			}	// if (pColumnsQualDesc)
		}	// if (m_vecQualDesc[i]->GetSafeHwnd())
	}	// for (int i = 0;i < nNumOfTabs;i++)
}

void CPricelistsFormView::delRowFromPricelistAndQuality(void)
{
	CXTPReportRecords *pRecsPricelist = m_wndPricelist.GetRecords();
	if (pRecsPricelist)
	{
		if (pRecsPricelist->GetCount() == 1)
		{
			::MessageBox(this->GetSafeHwnd(),m_sNotDeleteFirstRowMsg2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return;
		}

		if (::MessageBox(0,m_sDeletePrlAndQDescRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			CXTPReportRow *pRow = m_wndPricelist.GetFocusedRow();
			if (pRow == NULL) return;
			
			int nSelRow = pRow->GetIndex();
			pRecsPricelist->RemoveAt(nSelRow);
			m_wndPricelist.Populate();
			m_wndPricelist.UpdateWindow();

			CXTPReportRecords *pRecsQualDesc = NULL;
			int nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
			for (int i = 0;i < nNumOfTabs;i++)
			{
				if (m_vecQualDesc[i]->GetSafeHwnd())
				{
					pRecsQualDesc = m_vecQualDesc[i]->GetRecords();
					if (pRecsQualDesc != NULL)
					{
						pRecsQualDesc->RemoveAt(nSelRow);
						m_vecQualDesc[i]->Populate();
						m_vecQualDesc[i]->UpdateWindow();
					}	// if (pRowsQualDesc)
				}	// if (m_vecQualDesc[i]->GetSafeHwnd())
			}	// for (int i = 0;i < nNumOfTabs;i++)
		}	// if (::MessageBox(0,m_sDeletePrlAndQDescRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	} // if (::MessageBox(0,_T("Ta bort rad fr�n Prislista och kvalitetsbesk. ... ?"),"Meddelande",MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)

}

// Get name of Quslitydescription tabs; 060329 p�d
void CPricelistsFormView::getQualDescName(CStringArray& sarray)
{
	int nNumOfQDesc;

	nNumOfQDesc = m_wndQualDescTabCtrl.GetItemCount();
	if (nNumOfQDesc > 0)
	{
		for (int i = 0;i < nNumOfQDesc;i++)
		{
			sarray.Add(m_wndQualDescTabCtrl.GetItemText(i));
		}	// for (int i = 0;i < nNumOfQDesc;i++)
	}	// if (nNumOfQDesc > 0)
}

// Set tab name/Create Qualitydescription tabs; 060330 p�d
void CPricelistsFormView::setQualDescName(CStringArray& sarray)
{
	int nNumOfQDesc = m_wndQualDescTabCtrl.GetItemCount();

	for (int i = 0;i < sarray.GetCount();i++)
	{
		if (i < nNumOfQDesc)
		{
			m_wndQualDescTabCtrl.SetItemText(i,(sarray[i]));
		}	// if (i < nNumOfQDesc)
		else
		{
			setupAQualDescReportWnd();
			addQualDescToTabCtrl((sarray[i]));
			addQualDescColumnsAndRows();
		}
	}	// for (int i = 0;i < nNumOfQDesc;i++)
}

// Remove tabs from m_wndQualDescTabCtrl. If user removes a tab in the
// CQualitedescDlg; 060331 p�d
void CPricelistsFormView::delQualDescName(int tab_idx,CStringArray& sarray)
{
/*
	CString S;
	S.Format(_T("CPricelistsFormView::delQualDescName\ntab_idx %d"),tab_idx);
	AfxMessageBox(S);
*/
	if (tab_idx == 0)
	{
		int nNumOfTabs;
		int nNumOfTabsInArray;
		nNumOfTabs = m_wndQualDescTabCtrl.GetItemCount();
		nNumOfTabsInArray = (int)sarray.GetCount();
		// Can't remove the first tab; 060330 p�d
		if (nNumOfTabs > 1 && nNumOfTabs > nNumOfTabsInArray)
		{
			for (int i = nNumOfTabs; i > nNumOfTabsInArray;i--)
			{
				if (m_vecQualDesc[i-1]->GetSafeHwnd())
				{
					m_vecQualDesc[i-1]->delAllItemData();
				}	// if (m_vecQualDesc[i-1]->GetSafeHwnd())

				// Remove tab; 060331 p�d
				m_wndQualDescTabCtrl.DeleteItem(i-1);
				
				// Remove last item in vector; 060331 p�d
				m_vecQualDesc.pop_back();
			}	// for (int i = 0; i < nNumOfTabsToDelete;i++)
		}
	}
	else
	{
		int nTabCount = 0;
		// Remove tab; 081010 p�d
		if (tab_idx >= 0 && tab_idx < m_wndQualDescTabCtrl.GetItemCount())
		{
			if (m_vecQualDesc[tab_idx]->GetSafeHwnd())
			{
				m_vecQualDesc[tab_idx]->delAllItemData();
				m_wndQualDescTabCtrl.DeleteItem(tab_idx);
				// Remove last item in vector; 060331 p�d
				for (m_vecQualDesc_iterator = m_vecQualDesc.begin();m_vecQualDesc_iterator != m_vecQualDesc.end();m_vecQualDesc_iterator++)
				{
					if (nTabCount == tab_idx)
					{
						m_vecQualDesc.erase(m_vecQualDesc_iterator);
						break;
					}
					nTabCount++;
				}
			}	// if (m_vecQualDesc[i-1]->GetSafeHwnd())
		}
	}
}

// Get data set for Assortments and add to CStringArray arr; 060405 p�d
BOOL CPricelistsFormView::getAssortments(CStringArray& arr)
{
	int row;
	CString sData, S, csBuf;
	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns)
	{
		arr.RemoveAll();	// Clear array
		for (row = 0;row < pRecords->GetCount();row++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(row);
			if (pRec)
			{
				// Setup xml tag for Assortments; 060405 p�d
				sData = _T("<Assortments ");
				// Check that there's a assortment name entered; 071214 p�d
				// We'll add the assortment, even if there's no name added; 080703 p�d
				//
				// Or, if there's only one row in assortments, save it anyway; 080930 p�d
				if (!pRec->getColumnsInText(COLUMN_0).IsEmpty() || pRecords->GetCount() == 1)
				{
					csBuf = pRec->getColumnsInText(COLUMN_0);
					TextToHtml(&csBuf);
					sData += _T("assortname=\"") + csBuf + _T("\" ");
					sData += _T("min_diam=\"") + pRec->getColumnsInText(COLUMN_2) + _T("\" ");
					sData += _T("price_m3fub=\"") + pRec->getColumnsInText(COLUMN_3) + _T("\" ");
					sData += _T("price_m3to=\"") + pRec->getColumnsInText(COLUMN_4) + _T("\" ");
					sData += _T("massa=\"") + findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1)) + _T("\" ");
					sData += _T("/>");
					arr.Add(sData);
				}	// if (!pRec->getColumnsInText(COLUMN_0).IsEmpty())
			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}	// if (pRecords && pColumns)

//	AfxMessageBox("CPricelistsFormView::getAssortments\n" + sData);

	return (arr.GetCount() > 0);
}

BOOL CPricelistsFormView::checkAssortments(int prl_status)
{
	int row;
	double fMinDiam = 0.0;
	std::vector<double> checkValue;
	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns && prl_status == ID_RUNE_OLLAS_TYPEOF_PRICELIST_1)
	{
		for (row = 0;row < pRecords->GetCount();row++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(row);
			if (pRec)
			{
				fMinDiam = pRec->getColumnFloat(COLUMN_2);	// Min diamter for exchange in this column; 061117 p�d
				// Check if fMinDiam already is set in checkValue vector; 061117 p�d
				if (checkValue.size() > 0)
				{
					for (UINT i = 0;i < checkValue.size();i++)
					{
						if (fMinDiam == checkValue[i])
							return FALSE;
					}	// for (UINT i = 0;i < checkValue.size();i++)
				}	// if (checkValue.size() > 0)
				// Allow user to add zero values in Pricelist assortments.
				// A zero value equals don't use this value; 061117 p�d
				if (fMinDiam > 0.0)
					checkValue.push_back(fMinDiam);
				// Check if there's a Assortment entered if there's a value added; 080703 p�d
				if (pRec->getColumnFloat(COLUMN_2) > 0.0 && 
					  pRec->getColumnText(COLUMN_0).IsEmpty())
					return FALSE;

			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}	// if (pRecords && pColumns)

	return TRUE;
}

// Get diamterclasses set in Pricelist and add to CStringArray arr; 060405 p�d
BOOL CPricelistsFormView::getDiamClasses(LPTSTR dcls)
{
	int col,nDcls,nStartDiam,nNumOfDCLS,nColumns;
	CString sData,sAttr;
	CXTPReportColumns *pColumns =  m_wndPricelist.GetColumns();
	nColumns = pColumns->GetCount();
	if (pColumns && nColumns > 0)
	{
		nDcls = nStartDiam = nNumOfDCLS = 0;
		// Setup xml tag for Assortments; 060405 p�d
		sData = _T("<DiamClasses ");
		for (col = 1;col < pColumns->GetCount();col++)
		{
			m_wndPricelist.getItemData(col-1,&nDcls,&nStartDiam,&nNumOfDCLS);
			sAttr.Format(_T("d%d=\"%d\" "),col,nDcls);
			sData += sAttr;
		}	// for (col = 0;col < pColumns->GetCount();col++)
		sData += _T("/>");
		_tcscpy(dcls,sData);
	}	// if (pColumns)

	return (nColumns > 0);
}

// Get data set for Pricelist and add to CStringArray arr; 060405 p�d
BOOL CPricelistsFormView::getPricelist(CStringArray& arr)
{
	int row,col;
	CString sData, sAttr, csBuf;
	CXTPReportColumns *pColumns =  m_wndPricelist.GetColumns();
	CXTPReportRecords *pRecords = m_wndPricelist.GetRecords();
	if (pRecords && pColumns)
	{
		arr.RemoveAll();	// Clear array
		for (row = 0;row < pRecords->GetCount();row++)
		{
			CPricelistReportDataRec *pRec = (CPricelistReportDataRec*)pRecords->GetAt(row);
			if (pRec)
			{
				// Setup xml tag for Assortments; 060405 p�d
				sData = _T("<PricePerQuality ");
				for (col = 0;col < pColumns->GetCount();col++)
				{
					sAttr.Format(_T("d%d=\" "),col);
					csBuf = pRec->getColumnsInText(col);
					TextToHtml(&csBuf);
					switch(col)
					{
						case 0 :
							sData += _T("qualname=\"") + csBuf + _T("\" ");
							break;
						default :	sData += sAttr + pRec->getColumnsInText(col) + _T("\" "); break;
					};
				}	// for (col = 0;col < pColumns->GetCount();col++)
				sData += _T("/>");
				arr.Add(sData);
			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}	// if (pRecords && pColumns)

	return (arr.GetCount() > 0);
}


// Get data set for Pricelist and add to CStringArray arr; 060405 p�d
BOOL CPricelistsFormView::getQualDesc(CStringArray& arr)
{
	int row,col;
	UINT cnt;
	CString sData, sAttr, csBuf;

	if (m_vecQualDesc.size() > 0)
	{
		arr.RemoveAll();	// Clear array
		for (cnt = 0;cnt < m_vecQualDesc.size();cnt++)
		{
			CXTPReportColumns *pColumns =  m_vecQualDesc[cnt]->GetColumns();
			CXTPReportRecords *pRecords = m_vecQualDesc[cnt]->GetRecords();
			if (pRecords && pColumns)
			{
				csBuf = m_wndQualDescTabCtrl.GetItemText(cnt);
				TextToHtml(&csBuf);
				sData.Format(_T("<QualityDesc name=\"%s\">"), csBuf);
				for (row = 0;row < pRecords->GetCount();row++)
				{
					CQualDescReportDataRec *pRec = (CQualDescReportDataRec*)pRecords->GetAt(row);
					if (pRec)
					{
						// Setup xml tag for Assortments; 060405 p�d
						sData += _T("<Percent ");
						for (col = 0;col < pColumns->GetCount();col++)
						{
							sAttr.Format(_T("d%d=\" "),col);
							csBuf = pRec->getColumnsInText(col);
							TextToHtml(&csBuf);
							switch(col)
							{
								case 0 :
									sData += _T("qualname=\"") + csBuf + _T("\" ");
									break;
								default :	sData += sAttr + csBuf + _T("\" "); break;
							};
						}	// for (col = 0;col < pColumns->GetCount();col++)
						sData += _T("/>");
					}	// if (pRec)
				}	// for (row = 0;row < pRecords->GetCount();row++)

				sData += _T("</QualityDesc>");
				arr.Add(sData);
			}	// if (pRecords && pColumns)
		}	// for (cnt = 0;m_vecQualDesc.size();cnt++)
	}	// if (m_vecQualDesc.size() > 0)

	return (arr.GetCount() > 0);
}

void CPricelistsFormView::addAssortmentsToSelectedReportPage(int spc_id,vecTransactionAssort &vec)
{
	CString sPulpwoodTypeOf;

	if (m_wndAssortSettings.GetSafeHwnd() && vec.size() > 0)
	{
		// Clear report
		m_wndAssortSettings.ClearReport();
		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_assort rec = vec[i];		
			if (rec.getSpcID() == spc_id)
			{
				// Try to set Pulpwood type of, set in XML-file; 070608 p�d
				if (rec.getPulpType() < m_sarrPulpWoodTypes.GetCount() && rec.getPulpType() > -1)
				{
					sPulpwoodTypeOf = m_sarrPulpWoodTypes.GetAt(rec.getPulpType());
				}
				m_wndAssortSettings.AddRecord(new CAssortSettingsDataRec(NULL,
																																 rec.getAssortName(),
						                                                     rec.getMinDiam(),
																																 rec.getPriceM3fub(),
																																 rec.getPriceM3to(),
																																 sPulpwoodTypeOf ));
			}	// if (rec.getSpcID() == spc_id)
		}	// for (UINT i = 0;i < vec.size();i++)

		m_wndAssortSettings.Populate();
		m_wndAssortSettings.UpdateWindow();
	}	// if (m_wndAssortSettings.GetSafeHwnd())

}

CString CPricelistsFormView::findPulpWoodTypeIndexAsStr(LPCTSTR name)
{
	CString sValue = _T("");
	for (int i = 0;i < m_sarrPulpWoodTypes.GetCount();i++)
	{
		if (m_sarrPulpWoodTypes.GetAt(i).Compare(name) == 0)
		{
			sValue.Format(_T("%d"),i);
			return sValue;
		};
	}

	return sValue;
}

int CPricelistsFormView::findPulpWoodTypeIndexAsInt(LPCTSTR name)
{
	for (int i = 0;i < m_sarrPulpWoodTypes.GetCount();i++)
	{
		if (m_sarrPulpWoodTypes.GetAt(i).Compare(name) == 0)
		{
			return i;
		};
	}

	return -1;
}


void CPricelistsFormView::getQualityNamesSetInPricelist(CStringArray& names)
{
	CXTPReportRecords *pRecs = m_wndPricelist.GetRecords();
	if (pRecs != NULL)
	{
		names.RemoveAll();	// Clear list; 071206 p�d
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CPricelistReportDataRec *pRec = (CPricelistReportDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				names.Add(pRec->getColumnText(0));
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)
}

// Calculate price in pricelist, for specie. Either in percent or kronor; 090414 p�d
void CPricelistsFormView::recalculatePricelist_up(BOOL in_percnt,int value)
{
	CString sValue;
	int nValue = value;
	int nTableValue = 0;
	double fPercent = 0.0;
	double fCalcValue = 0.0;
	CXTPReportColumns *pColumnsPriceList = m_wndPricelist.GetColumns();
	CXTPReportRecords *pRecords = m_wndPricelist.GetRecords();
	if (pColumnsPriceList != NULL && pRecords != NULL)
	{
		for (int i = 0;i < pRecords->GetCount();i++)
		{
			CPricelistReportDataRec *pRec = (CPricelistReportDataRec*)pRecords->GetAt(i);
			if (pRec != NULL)
			{
				for (int i1 = 1;i1 < pColumnsPriceList->GetCount();i1++)
				{
					if (in_percnt)
					{
						fPercent = 1 + (nValue/100.0);
						nTableValue = pRec->getColumnInt(i1);
						fCalcValue = nTableValue * fPercent;
						sValue.Format(_T("%.0f"),fCalcValue);
						pRec->setColumnInt(i1,_tstoi(sValue));
					}	// if (in_percent)
					else
					{
						nTableValue = pRec->getColumnInt(i1);
						fCalcValue = nTableValue + nValue;
						sValue.Format(_T("%.0f"),fCalcValue);
						pRec->setColumnInt(i1,_tstoi(sValue));
					}	// if (in_percent)
				}
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
		m_wndPricelist.Populate();
		m_wndPricelist.UpdateWindow();
	}	// if (pRecs != NULL)
}

// Calculate price in pricelist, for specie. Either in percent or kronor; 090414 p�d
void CPricelistsFormView::recalculatePricelist_down(BOOL in_percnt,int value)
{
	CString sValue;
	int nValue = value;
	int nTableValue = 0;
	double fPercent = 0.0;
	double fCalcValue = 0.0;
	CXTPReportColumns *pColumnsPriceList = m_wndPricelist.GetColumns();
	CXTPReportRecords *pRecords = m_wndPricelist.GetRecords();
	if (pColumnsPriceList != NULL && pRecords != NULL)
	{
		for (int i = 0;i < pRecords->GetCount();i++)
		{
			CPricelistReportDataRec *pRec = (CPricelistReportDataRec*)pRecords->GetAt(i);
			if (pRec != NULL)
			{
				for (int i1 = 1;i1 < pColumnsPriceList->GetCount();i1++)
				{
					if (in_percnt)
					{
						fPercent = 1 + (nValue/100.0);
						nTableValue = pRec->getColumnInt(i1);
						fCalcValue = nTableValue / fPercent;
						sValue.Format(_T("%.0f"),fCalcValue);
						pRec->setColumnInt(i1,_tstoi(sValue));
					}	// if (in_percent)
					else
					{
						nTableValue = pRec->getColumnInt(i1);
						fCalcValue = nTableValue - nValue;
						sValue.Format(_T("%.0f"),fCalcValue);
						pRec->setColumnInt(i1,_tstoi(sValue));
					}	// if (in_percent)

				}
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
		m_wndPricelist.Populate();
		m_wndPricelist.UpdateWindow();
	}	// if (pRecs != NULL)
}

//Lagt in en kollfunktion som returnerar false n�got massa sortiment saknar fub pris, ber�knas det bara med fub pris
//20111122 Bug #2583 J�
BOOL CPricelistsFormView::getPulpFubPriceIsSet()
{
	int row,nAssortTypeIndex=-1;
	BOOL bFubPriceSet=TRUE;
	float fPriceFub=0.0;
	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns)
	{
		for (row = 0;row < pRecords->GetCount();row++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(row);
			if (pRec)
			{
				nAssortTypeIndex=findPulpWoodTypeIndexAsInt(pRec->getColumnsInText(COLUMN_1));
				if(nAssortTypeIndex>0)
				{
					fPriceFub=pRec->getColumnFloat(COLUMN_3);
					if(fPriceFub<=0.0)
						bFubPriceSet=FALSE;
				}
			}
		}
	}
	return bFubPriceSet;
}

//Lagt in en kollfunktion som returnerar TRUE om det finns fler �n ett sortiment som har mindia samt priser satt till 0
//20111228 Bug #2717 J�
BOOL CPricelistsFormView::getNumZeroDiaPrice(CString &csAss)
{
	int nRow=0,nCount=0,nAssortTypeIndex=-1;
	double fMinDiam=0.0,fPriceFub=0.0,fPriceTo=0.0;
	CString strAssName=_T("");
	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns)
	{						
		for (nRow = 0;nRow < pRecords->GetCount();nRow++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(nRow);
			if (pRec)
			{
				nAssortTypeIndex = findPulpWoodTypeIndexAsInt(pRec->getColumnsInText(COLUMN_1));
				if (nAssortTypeIndex == 0)//Timmer
				{
					fMinDiam = pRec->getColumnFloat(COLUMN_2);					
					fPriceFub = pRec->getColumnFloat(COLUMN_3);
					fPriceTo = pRec->getColumnFloat(COLUMN_4);
					if(fMinDiam>0.0 && fPriceFub<=0.0 &&  fPriceTo<=0.0)
					{
						nCount++;
						if(nCount>1)
						{
							strAssName=pRec->getColumnsInText(COLUMN_0);
							csAss=strAssName;
							return TRUE;
						}
					}
				}
			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}
	return FALSE;	
}