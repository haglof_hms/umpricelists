// MDIDataBaseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "MDIPricelistsFrame.h"
#include "PricelistsView.h"
#include "PrlViewAndPrint.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
}

CMDIFrameDoc::~CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

void CMDIFrameDoc::doc_setTransaction_pricelist(CTransaction_pricelist rec)
{
	m_recPricelist = rec;
}

CTransaction_pricelist &CMDIFrameDoc::doc_getTransaction_pricelist(void)
{
	return m_recPricelist;
}



/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands


/////////////////////////////////////////////////////////////////////////////
// CMDIPricelistsFrame


XTPDockingPanePaintTheme CMDIPricelistsFrame::m_themeCurrent = xtpPaneThemeOffice2003;

IMPLEMENT_DYNCREATE(CMDIPricelistsFrame, CMDIChildWnd)


BEGIN_MESSAGE_MAP(CMDIPricelistsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPricelistsFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_SYSCOMMAND()
	ON_WM_CHAR()
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	// Toolbar button events; 060403 p�d
	ON_COMMAND(ID_TBTN_GENERAL_INFO, OnTBtnOpenGeneralInfo)
	ON_COMMAND(ID_TBTN_DIAMCLASS_SETTINGS, OnTBtnOpenDiamClassSettings)
	ON_COMMAND(ID_TBTN_IMPORT, OnTBtnImportPricelist)
	ON_COMMAND(ID_TBTN_EXPORT, OnTBtnExportPricelist)
	ON_COMMAND(ID_TBTN_ADDSPC, OnTBtnAddSpecie)
	ON_COMMAND(ID_TBTN_DELSPC, OnTBtnRemoveSpecie)
	ON_COMMAND(ID_TBTN_VIEW, OnTBtnPreview)

	ON_UPDATE_COMMAND_UI(ID_TBTN_IMPORT, OnUpdateImportTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_EXPORT, OnUpdateExportTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADDSPC, OnUpdateAddSpcTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DELSPC, OnUpdateRemoveSpcTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_VIEW, OnUpdatePreviewTBtn)

	ON_WM_GETMINMAXINFO()
	ON_WM_SETFOCUS()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPricelistsFrame::CMDIPricelistsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bAlreadySaidOkToClose = FALSE;

	m_bIsImportTBtn = TRUE;
	m_bIsExportTBtn = TRUE;
	m_bIsAddSpcTBtn = TRUE;
	m_bIsRemoveTBtn = TRUE;
	m_bIsPreviewTBtn = TRUE;

}

CMDIPricelistsFrame::~CMDIPricelistsFrame()
{
}

void CMDIPricelistsFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_CREATE_PRICELIST_KEY1);
	SavePlacement(this, csBuf);

	// Save state of docking pane(s)
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	m_paneManager.GetLayout(&layoutNormal);
	layoutNormal.Save((REG_WP_PRICELIST_LAYOUT_KEY1));

}

void CMDIPricelistsFrame::OnClose(void)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070410 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	// Send messages to HMSShell, disable buttons on toolbar; 070410 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	if (m_bAlreadySaidOkToClose)
		CMDIChildWnd::OnClose();
	else	if (okToClose())
		CMDIChildWnd::OnClose();
}

void CMDIPricelistsFrame::OnSysCommand( UINT id, LONG lp)
{
	if ((id & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (okToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(id,lp);
		}
	}
	else if ((id & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(id,lp);
}

BOOL CMDIPricelistsFrame::okToClose(void)
{
	// Do save here, so e.g. "ESCAPE"'ll work on savin'; 080114 p�d
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView)
	{
		return pView->saveData(CPricelistsView::ON_QUIT);
	}	// if (pView)

	return FALSE;
}

void CMDIPricelistsFrame::OnChar(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CMDIChildWnd::OnChar(nChar,nRepCnt,nFlags);
}

int CMDIPricelistsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;
	CString sToolTip3;
	CString sToolTip4;
	CString sToolTip5;
	CString sToolTip6;
	CString sToolTip7;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;


	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	if (!m_wndGeneralInfoDlg.Create(CGeneralInfoDlg::IDD, this))
	{
		return -1;
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sToolTip1 = xml->str(IDS_STRING121);
			sToolTip2 = xml->str(IDS_STRING122);
			sToolTip3 = xml->str(IDS_STRING140);
			sToolTip4 = xml->str(IDS_STRING141);
			sToolTip5 = xml->str(IDS_STRING142);
			sToolTip6 = xml->str(IDS_STRING143);
			sToolTip7 = xml->str(IDS_STRING1901);
			
			m_sMsgCap  = xml->str(IDS_STRING151);
			m_sMsgImportError.Format(_T("%s\n%s\n\n%s\n"),
					xml->str(IDS_STRING1700),
					xml->str(IDS_STRING1701),
					xml->str(IDS_STRING1704));
			m_sMsgWindowsOpen.Format(_T("%s\n%s\n\n"),
					xml->str(IDS_STRING2300),
					xml->str(IDS_STRING2301));

		}
		delete xml;
	}

	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_INFO,sToolTip1);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(1),RES_TB_GENERAL,sToolTip2);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(2),RES_TB_IMPORT,sToolTip3);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(3),RES_TB_EXPORT,sToolTip4);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(4),RES_TB_ADD,sToolTip5);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(5),RES_TB_DEL,sToolTip6);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(6),RES_TB_PREVIEW,sToolTip7);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))


	// Get window settings from registry; 060113 p�d

	// control bars objects have been created and docked.
	m_paneManager.InstallDockingPanes(this);
	m_paneManager.SetStickyFloatingFrames(TRUE);
	m_paneManager.SetThemedFloatingFrames(TRUE);

	m_paneManager.EnableKeyboardNavigate();

	// Create docking panes.
	CXTPDockingPane *paneSettings = m_paneManager.CreatePane(IDC_SETTINGS_PANE, CRect(0, 0,140, 120), xtpPaneDockLeft);
	paneSettings->SetOptions( xtpPaneNoCloseable );
	// Create docking panes.
	CXTPDockingPane *paneChangePrice = m_paneManager.CreatePane(IDC_CHANGE_PRICE_PANE, CRect(0, 0,140, 120), xtpPaneDockLeft,paneSettings);
	paneChangePrice->SetOptions( xtpPaneNoCloseable );

	CXTPDockingPane *paneGeneralData = m_paneManager.CreatePane(IDC_GENERAL_INFO_PANE_1, CRect(0, 0, 0, 140), xtpPaneDockTop);
	paneGeneralData->SetOptions( xtpPaneNoCloseable );

	m_paneManager.AttachPane(paneChangePrice, paneSettings);

	int nIDs[] = {IDC_SETTINGS_PANE, IDC_CHANGE_PRICE_PANE, IDC_GENERAL_INFO_PANE_1};
	GetDockingPaneManager()->SetIcons(IDB_BITMAP4, nIDs, 3, RGB(0, 255, 0));

	setLanguage();

	m_paneManager.SetTheme(m_themeCurrent);
	// Load the previous state for docking panes.
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	if (layoutNormal.Load((REG_WP_PRICELIST_LAYOUT_KEY1)))
	{
		m_paneManager.SetLayout(&layoutNormal);
	}

	SetupPricelist();

	/////////////////////////////////////////////////////////////////////////////////////
	// Always select Settings pane; 090414 p�d
	CXTPDockingPane* pPaneProp = GetDockingPaneManager()->FindPane(IDC_SETTINGS_PANE);
	if (pPaneProp != NULL) pPaneProp->Select();

	m_bFirstOpen = TRUE;

	BOOL bWindowsOpen = FALSE;
  CString sDocName = L"", sMsg = L"", sWindows = L"";
  CDocTemplate *pTemplate = NULL;
  CWinApp *pApp = AfxGetApp();

	// Loop through doc templates
  POSITION pos = pApp->GetFirstDocTemplatePosition();
  while(pos)
  {
		pTemplate = pApp->GetNextDocTemplate(pos);
    pTemplate->GetDocString(sDocName, CDocTemplate::docName);

    // Check for instances of this document (ignore database manager document)
    POSITION posDOC = pTemplate->GetFirstDocPosition();
    if (posDOC && (sDocName.CompareNoCase(_T("View5000")) == 0 || // UMEstimate
									 sDocName.CompareNoCase(_T("Module5100")) == 0 ||	// UMTemplates (Best�ndsmall)
									 sDocName.CompareNoCase(_T("Module5200")) == 0))	// UMLandValue
    {
			CDocument* pDoc = (CDocument*)pTemplate->GetNextDoc(posDOC);
			sWindows += _T(" * ") + pDoc->GetTitle() + _T("\n");
			bWindowsOpen = TRUE;
    }
  }
  if( bWindowsOpen )
  {
		// Display list of open child windows
     sMsg.Format(_T("%s\n%s\n"),m_sMsgWindowsOpen,sWindows);
     ::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
  }

	return 0; // creation ok
}

BOOL CMDIPricelistsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CMDIPricelistsFrame::PreTranslateMessage(MSG* pMsg)
{
	return CMDIChildWnd::PreTranslateMessage(pMsg);
}

void CMDIPricelistsFrame::OnSetFocus(CWnd *wnd)
{
	int nIndex,nItems;
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	CDocument *pDoc = GetActiveDocument();
	
	nIndex = nItems = 0;
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
			if (pView)
			{
				nIndex = pView->getDBNavigationCounter();
				nItems = pView->getNumOfItemsInPricelist();
				pView->doSetNavigationBar();
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,nIndex > 0);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,nIndex > 0);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,nIndex < nItems - 1);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,nIndex < nItems - 1);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);
}


// CMDIPricelistsFrame diagnostics

#ifdef _DEBUG
void CMDIPricelistsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPricelistsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIPricelistsFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

// PRIVATE

void CMDIPricelistsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIPricelistsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_CREATE_PRICELIST_KEY1);
		LoadPlacement(this, csBuf);
  }
}

void CMDIPricelistsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PRICELISTS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PRICELISTS;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPricelistsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

LRESULT CMDIPricelistsFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		// get a pointer to the docking pane being shown.
		CXTPDockingPane* pPane = (CXTPDockingPane*)lParam;
		if (!pPane->IsValid())
		{
			if (pPane->GetID() == IDC_SETTINGS_PANE)
			{
				if (m_wndDataEnterDlg.Create(CDataEnterDlg::IDD, this))
				{
					pPane->Attach(&m_wndDataEnterDlg);
					// We need to do this, only if the Pane is hidden. Otherwise
					// information is displayed anyway; 070208 p�d
					if (pPane->IsHidden())
					{
						// Add information on End diamterclass, diameterclass and
						// number of on last entered; 070208 p�d
						CXTPTabManagerItem *pSelectedPage = NULL;
						CPricelistsView* pViewPrl = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
						if (pViewPrl != NULL)
						{
							pSelectedPage = pViewPrl->m_wndTabControl.getSelectedTabPage();

							if (pSelectedPage)
							{
								// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
								CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
								if (pView)
								{
									pView->setDataEnterDlgValues();
									return TRUE;
								}	// if (pView)
							}	// if (pSelectedPage)
						}	// if (pViewPrl != NULL)
					}	// if (pPane->IsHidden())
				}	// if (m_wndDataEnterDlg.Create(CDataEnterDlg::IDD, this))
			}	// if (pPane->GetID() == IDC_SETTINGS_PANE)

			if (pPane->GetID() == IDC_CHANGE_PRICE_PANE)
			{
				if (m_wndChangePrlPriceDlg.Create(CChangePrlPriceDlg::IDD, this))
				{
					pPane->Attach(&m_wndChangePrlPriceDlg);
					// We need to do this, only if the Pane is hidden. Otherwise
					// information is displayed anyway; 070208 p�d
					if (pPane->IsHidden())
					{
						// Add information on End diamterclass, diameterclass and
						// number of on last entered; 070208 p�d
						CXTPTabManagerItem *pSelectedPage = NULL;
						CPricelistsView* pViewPrl = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
						if (pViewPrl != NULL)
						{
							pSelectedPage = pViewPrl->m_wndTabControl.getSelectedTabPage();

							if (pSelectedPage)
							{

								// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
								CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
								if (pView)
								{
									pView->setPrlPriceDlgValues();
									return TRUE;
								}	// if (pView)

							}	// if (pSelectedPage)
						}	// if (pViewPrl != NULL)
					}	// if (pPane->IsHidden())
				}	// if (m_wndDataEnterDlg.Create(CDataEnterDlg::IDD, this))
			}	// if (pPane->GetID() == IDC_CHANGE_PRICE_PANE)

			if (pPane->GetID() == IDC_GENERAL_INFO_PANE_1)
			{
				if (m_wndGeneralInfoDlg.GetSafeHwnd())
				{
					pPane->Attach(&m_wndGeneralInfoDlg);
				}	// if (m_wndGeneralInfoDlg.Create(CGeneralInfoDlg::IDD, this))
			}	// if (pPane->GetID() == IDC_SETTINGS_PANE)
		}	// if (!pPane->IsValid())
		
		return TRUE; // Handled
	}	// if (wParam == XTP_DPN_SHOWWINDOW)

	
	return FALSE;
}


// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPricelistsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW1,m_sLangFN,lParam);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			if (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}


void CMDIPricelistsFrame::OnTBtnOpenGeneralInfo()
{
	ToggleGeneralInfoPane();
}

void CMDIPricelistsFrame::OnTBtnOpenDiamClassSettings()
{
	ToggleDclsSettingsPane();
}

void CMDIPricelistsFrame::OnTBtnImportPricelist(void)
{
	CString sName;
	CString sXMLFile;
	CString sFilter;
	CDocument *pDoc = GetActiveDocument();
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING100),PRICELIST_FN_EXTE,PRICELIST_FN_EXTE);
		}
		delete xml;
	}
			
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
			if (pView)
			{
				// Handles clik on open button
				CFileDialog dlg( TRUE, PRICELIST_FN_EXTE, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
												sFilter, this);
	
				if(dlg.DoModal() == IDOK)
				{
					PricelistParser prlParser;
					if (prlParser.LoadFromFile(dlg.GetPathName()))
					{
						int nDummy;
						if (!prlParser.getHeaderPriceIn(&nDummy))
						{
							::MessageBox(this->GetSafeHwnd(),m_sMsgImportError,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
						}
						else
						{
							pView->saveFromXMLFileToPricelistDB(dlg.GetPathName());
							pView->doSetNavigationBar();
						}
					}

				}
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIPricelistsFrame::OnTBtnExportPricelist(void)
{
	CString sName;
	CString sXMLFile;
	CString sFilter;
	CDocument *pDoc = GetActiveDocument();
	//--------------------------------------------------------------------------
	// Save pricelist, before doin' an export; 091012 p�d
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView)
	{
		if (!pView->saveData(CPricelistsView::ON_SAVE)) return;
	}	// if (pView)
	//--------------------------------------------------------------------------

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING100),PRICELIST_FN_EXTE,PRICELIST_FN_EXTE);
		}
		delete xml;
	}
			
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
			if (pView)
			{
				if (pView->getPricelistXML(sName,sXMLFile))
				{
					scanFileName(sName);
					// Check if fileextension, if not add to filename; 090304 p�d
					if (sName.Right(6) != PRICELIST_FN_EXTE)
						sName += PRICELIST_FN_EXTE;
					// Handles clik on open button
					CFileDialog dlg( FALSE, PRICELIST_FN_EXTE, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
													sFilter, this);
	
					if(dlg.DoModal() == IDOK)
					{
						sName = dlg.GetPathName();
						if (sName.Right(6) != PRICELIST_FN_EXTE)
							sName += PRICELIST_FN_EXTE;
						pView->savePricelistToXMLFile(sName,sXMLFile);
					}
				}	// if (pView->getPricelistXML(sData))
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIPricelistsFrame::OnTBtnAddSpecie(void)
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
			if (pView)
			{
				pView->addSpecieToPricelist();
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIPricelistsFrame::OnTBtnRemoveSpecie(void)
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
			if (pView)
			{
				pView->removeSpecieFromPricelist();
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIPricelistsFrame::OnTBtnPreview(void)
{
	CString sName;
	CString sXMLFile;
	CPricelistsView *pPrlView = NULL;
	CPrlViewAndPrint *pPrintView = NULL;

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			pPrlView = (CPricelistsView*)pDoc->GetNextView(pos);
			if (pPrlView)
			{
				pPrlView->getPricelistXML(sName,sXMLFile);
				pPrintView = (CPrlViewAndPrint*)showFormView(IDD_FORMVIEW4,m_sLangFN,(LPARAM)(LPCTSTR)sXMLFile);
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIPricelistsFrame::OnUpdateImportTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsImportTBtn );
}

void CMDIPricelistsFrame::OnUpdateExportTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsExportTBtn );
}

void CMDIPricelistsFrame::OnUpdateAddSpcTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsAddSpcTBtn );
}

void CMDIPricelistsFrame::OnUpdateRemoveSpcTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsRemoveTBtn );
}

void CMDIPricelistsFrame::OnUpdatePreviewTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPreviewTBtn );
}

BOOL CMDIPricelistsFrame::isGeneralInofDlgHidden(void)
{
	CXTPDockingPane* pPane = GetDockingPaneManager()->FindPane(IDC_GENERAL_INFO_PANE_1);
	ASSERT(pPane);
	if (pPane) 
	{
		return m_paneManager.IsPaneHidden(pPane);
	}
	return FALSE;
}

// MY METHODS
void CMDIPricelistsFrame::setLanguage()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			/////////////////////////////////////////////////////////////////////////////////////
			// Set caption for Properties pane; 060207 p�d
			CXTPDockingPane* pPaneProp = GetDockingPaneManager()->FindPane(IDC_SETTINGS_PANE);
			ASSERT(pPaneProp);
			if (pPaneProp) 
			{
				pPaneProp->SetTitle((xml->str(IDS_STRING107)));
			}
			CXTPDockingPane* pPaneProp1 = GetDockingPaneManager()->FindPane(IDC_CHANGE_PRICE_PANE);
			ASSERT(pPaneProp1);
			if (pPaneProp1) 
			{
				pPaneProp1->SetTitle((xml->str(IDS_STRING1070)));
			}

			/////////////////////////////////////////////////////////////////////////////////////
			// Set caption for Geanral information pane; 060403 p�d
			CXTPDockingPane* pPaneGeneralData = GetDockingPaneManager()->FindPane(IDC_GENERAL_INFO_PANE_1);
			ASSERT(pPaneGeneralData);
			if (pPaneGeneralData) 
			{
				pPaneGeneralData->SetTitle((xml->str(IDS_STRING119)));
				//pPaneGeneralData->Hide();
			}
		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
}

void CMDIPricelistsFrame::ToggleGeneralInfoPane(void)
{
	CXTPDockingPane* pPane = GetDockingPaneManager()->FindPane(IDC_GENERAL_INFO_PANE_1);
	ASSERT(pPane);
	if (pPane) 
	{
		m_paneManager.ToggleAutoHide(pPane);
	}
}

void CMDIPricelistsFrame::ToggleDclsSettingsPane(void)
{
	/////////////////////////////////////////////////////////////////////////////////////
	// Set caption for Properties pane; 060207 p�d
	CXTPDockingPane* pPane = GetDockingPaneManager()->FindPane(IDC_SETTINGS_PANE);
	ASSERT(pPane);
	if (pPane) 
	{
		m_paneManager.ToggleAutoHide(pPane);
	}
}

void CMDIPricelistsFrame::SetupPricelist(void)
{
	CPricelistsView *pView = (CPricelistsView *)GetActiveFrame()->GetActiveView();
	if (pView)
	{
		pView->setupPricelistOnStartup();
	}
}


/////////////////////////////////////////////////////////////////////////////
// CMDIPricelistsListFrame


IMPLEMENT_DYNCREATE(CMDIPricelistsListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIPricelistsListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPricelistsListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPricelistsListFrame::CMDIPricelistsListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIPricelistsListFrame::~CMDIPricelistsListFrame()
{
}

void CMDIPricelistsListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LIST_PRICELIST_KEY1);
	SavePlacement(this, csBuf);
}

int CMDIPricelistsListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIPricelistsListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIPricelistsListFrame diagnostics

#ifdef _DEBUG
void CMDIPricelistsListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPricelistsListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIPricelistsListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CMDIPricelistsListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIPricelistsListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LIST_PRICELIST_KEY1);
		LoadPlacement(this, csBuf);
  }
}

void CMDIPricelistsListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIPricelistsListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PRICELISTS_LIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PRICELISTS_LIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPricelistsListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPricelistsListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDIPricelistsListFrame::setLanguage()
{
/*
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
*/
}

