#pragma once

#include "Resource.h"

// CGeneralInfoDlg dialog

class CGeneralInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CGeneralInfoDlg)
	//private:

	//Lagt till en variablen vars syfte �r att se om status �r �ndrad till Klar att anv�nda eller �r under utveckling. Bug#2583 20111122 J�
	int  bDoSave;
	CString m_sLangFN;

	CString	m_sStatusLblOK;
	CString m_sStatusLblDevelop;

	CString m_sMsgCap;
	CString m_sMsgCharError;
	CString m_sPricelistErrMsg;

protected:
		CXTResizeGroupBox m_wndGrp3;

		CMyExtStatic m_wndLbl1;
		CMyExtStatic m_wndLbl2;
		CMyExtStatic m_wndLbl3;
		CMyExtStatic m_wndLbl4;
		CMyExtStatic m_wndLbl5;
		CMyExtStatic m_wndLbl6;

		CMyExtEdit m_wndEdit1;
		CMyExtEdit m_wndEdit2;
		CMyExtEdit m_wndEdit3;

		CButton m_wndInM3FUB;
		CButton m_wndInM3TO;

		CComboBox m_wndCBox1;

		CDatePickerCombo m_wndDateTimeCtrl;

		void setLanguage();

		BOOL m_bEnableWindows;

		BOOL m_bIncludedInStandTmplate;

public:
	CGeneralInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGeneralInfoDlg();

// Dialog Data
	enum { IDD = IDD_DIALOGBAR1 };

	// Prevent Enter and Cancel on this Dialog; 060403 p�d
	virtual void OnOK()	{}
	virtual void OnCancel() {}

	// -------------------------------------
  // the following code declares the 
  // required elements for our AnchorMap
  // -------------------------------------
  DECLARE_ANCHOR_MAP();

	//Lagt funktioner vars syfte �r att se om status �r �ndrad till Klar att anv�nda eller �r under utveckling. Bug#2583 20111122 J�
	void setDoSave(int nSave);
	BOOL getDoSave();

	// Get data from fields; 060405 p�d
	CString getNameOfPricelist(void);
	void setNameOfPricelist(LPCTSTR value);
	void enableName(BOOL enable);

	
	CString getNameOfCreator(void);
	void setNameOfCreator(LPCTSTR value);
	
	void setStatusOfPricelist(int status,int doCmbBoxChange);

	CString getCreateDate(void);				

	int getTimberPriceIn(void);
	void setTimberPriceIn(int idx);

	CString getNotes(void);
	void setNotes(LPCTSTR value);

	BOOL isFocus(CWnd *pWnd)
	{
		return (pWnd->GetSafeHwnd() == m_wndEdit1.GetSafeHwnd() || 
						pWnd->GetSafeHwnd() == m_wndEdit2.GetSafeHwnd() ||
						pWnd->GetSafeHwnd() == m_wndDateTimeCtrl.GetSafeHwnd() ||
						pWnd->GetSafeHwnd() == m_wndEdit3.GetSafeHwnd());
	}

	void clearDlg(void)
	{
		TCHAR szTmp[50];
		szTmp[0] = '\0';
		m_wndCBox1.SelectString(0,m_sStatusLblDevelop);
		m_wndEdit1.SetWindowText(szTmp); //_T(" "));
		m_wndEdit2.SetWindowText(getUserName().MakeUpper()); //_T(" "));
		m_wndEdit3.SetWindowText(szTmp); //_T(" "));
		setTimberPriceIn(2);	// On clear, set default price in to m3to; 080423 p�d
	}
	
	BOOL isDirty(void)
	{
		return m_wndEdit1.isDirty() || m_wndEdit2.isDirty() || m_wndEdit3.isDirty();
	}

	void resetIsDirty(void)
	{
		m_wndEdit1.resetIsDirty(); 
		m_wndEdit2.resetIsDirty(); 
		m_wndEdit3.resetIsDirty(); 
	}

	void setEnableWindows(BOOL enabled,BOOL w1_also,BOOL lock)
	{
		m_bEnableWindows = enabled;
		if (w1_also)
		{
			m_wndEdit1.EnableWindow( m_bEnableWindows );
			m_wndEdit1.SetReadOnly(!m_bEnableWindows);
		}

		if (!lock)
		{
			m_wndCBox1.EnableWindow( m_bEnableWindows );
			m_wndEdit2.EnableWindow( m_bEnableWindows );
			m_wndEdit2.SetReadOnly(!m_bEnableWindows);
			m_wndEdit3.EnableWindow( m_bEnableWindows );
			m_wndEdit3.SetReadOnly(!m_bEnableWindows);
			m_wndInM3FUB.EnableWindow( m_bEnableWindows );
			m_wndInM3TO.EnableWindow( m_bEnableWindows );
			m_wndDateTimeCtrl.EnableWindow( m_bEnableWindows );
		}

		setLock(lock);
	}

	int getStatus(void);
	void setStatus(void);

	void setLock(BOOL lock)
	{
		if (lock && !m_bIncludedInStandTmplate)
		{
			m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
			m_wndEdit1.EnableWindow( FALSE );
			m_wndEdit1.SetReadOnly( TRUE );
		}
		if (lock && m_bIncludedInStandTmplate)
		{
			m_wndEdit1.SetDisabledColor(  BLACK, INFOBK );
			m_wndEdit1.EnableWindow( FALSE );
			m_wndEdit1.SetReadOnly( TRUE );
		}
		if (!lock && !m_bIncludedInStandTmplate)
		{
			m_wndEdit1.EnableWindow( TRUE );
			m_wndEdit1.SetReadOnly( FALSE );
		}
		m_wndEdit2.EnableWindow( !lock );
		m_wndEdit2.SetReadOnly(lock);
		m_wndEdit3.EnableWindow( !lock );
		m_wndEdit3.SetReadOnly(lock);
		m_wndInM3FUB.EnableWindow( !lock );
		m_wndInM3TO.EnableWindow( !lock );
		m_wndDateTimeCtrl.EnableWindow( !lock );
	}
protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnSize(UINT nType,int cx,int cy);
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg LRESULT CGeneralInfoDlg::OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnCBox1Changed(void);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

// CGeneralInfoDlg_2 dialog

class CGeneralInfoDlg_2 : public CDialog
{
	DECLARE_DYNAMIC(CGeneralInfoDlg_2)
	//private:
	int  bDoSave;
	CString m_sMsgCap;
	CString m_sMissingInfoMsg;
	CString m_sMissingInfoMsg1;
	CString m_sMissingInfoMsg2;
	CString m_sMissingInfoMsg3;

	CString m_sLangFN;

	CString	m_sStatusLblOK;
	CString m_sStatusLblDevelop;

	CString m_sMsgCharError;

	CString m_sPricelistErrMsg;

protected:
		CXTResizeGroupBox m_wndGrp3;

		CMyExtStatic m_wndLbl1;
		CMyExtStatic m_wndLbl2;
		CMyExtStatic m_wndLbl3;
		CMyExtStatic m_wndLbl4;
		CMyExtStatic m_wndLbl5;

		CMyExtEdit m_wndEdit1;
		CMyExtEdit m_wndEdit2;
		CMyExtEdit m_wndEdit3;
		CMyExtEdit m_wndEdit4;

		CComboBox m_wndCBox2;

		CDatePickerCombo m_wndDateTimeCtrl;

		void setLanguage();

		BOOL m_bEnableWindows;

		BOOL m_bIncludedInStandTmplate;
public:
	CGeneralInfoDlg_2(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGeneralInfoDlg_2();

// Dialog Data
	enum { IDD = IDD_DIALOGBAR2 };

	// Prevent Enter and Cancel on this Dialog; 060403 p�d
	virtual void OnOK()	{}
	virtual void OnCancel() {}

	//Lagt funktioner vars syfte �r att se om status �r �ndrad till Klar att anv�nda eller �r under utveckling. Bug#2583 20111122 J�
	void setDoSave(int nSave);
	BOOL getDoSave();

	// -------------------------------------
  // the following code declares the 
  // required elements for our AnchorMap
  // -------------------------------------
  DECLARE_ANCHOR_MAP();

	// Get data from fields; 060405 p�d
	CString getNameOfPricelist(void);
	void setNameOfPricelist(LPCTSTR value);
	void enableName(BOOL enable);
	
	CString getNameOfCreator(void);
	void setNameOfCreator(LPCTSTR value);

	void setStatusOfPricelist(int status,int doCmbBoxChange);

	CString getCreateDate(void);				
	
	CString getNotes(void);
	void setNotes(LPCTSTR value);

	BOOL isFocus(CWnd *pWnd)
	{
		return (pWnd->GetSafeHwnd() == m_wndEdit1.GetSafeHwnd() || 
						pWnd->GetSafeHwnd() == m_wndEdit2.GetSafeHwnd() ||
						pWnd->GetSafeHwnd() == m_wndDateTimeCtrl.GetSafeHwnd() ||
						pWnd->GetSafeHwnd() == m_wndEdit3.GetSafeHwnd());
	}

	void clearDlg(void)
	{
		TCHAR szTmp[50];
		szTmp[0] = '\0';
		if (m_wndCBox2.GetSafeHwnd()) m_wndCBox2.SelectString(0,m_sStatusLblDevelop);
		if (m_wndEdit1.GetSafeHwnd()) m_wndEdit1.SetWindowText(szTmp); //_T(""));
		if (m_wndEdit2.GetSafeHwnd()) m_wndEdit2.SetWindowText(getUserName().MakeUpper()); //_T(""));
		if (m_wndEdit3.GetSafeHwnd()) m_wndEdit3.SetWindowText(szTmp); //_T(""));
	}
	
	BOOL isDirty(void)
	{
		return m_wndEdit1.isDirty() || 
					 m_wndEdit2.isDirty() || 
					 m_wndEdit3.isDirty();
	}

	void resetIsDirty(void)
	{
		m_wndEdit1.resetIsDirty(); 
		m_wndEdit2.resetIsDirty(); 
		m_wndEdit3.resetIsDirty(); 
	}

	void setEnableWindows(BOOL enabled,BOOL w1_also,BOOL lock)
	{
		m_bEnableWindows = enabled;
		m_wndCBox2.EnableWindow( m_bEnableWindows );
		if (w1_also)
		{
			m_wndEdit1.EnableWindow( m_bEnableWindows );
			m_wndEdit1.SetReadOnly(!m_bEnableWindows);
		}
		else if (lock)
		{
			m_wndEdit1.EnableWindow( !lock );
			m_wndEdit1.SetReadOnly( lock );
		}

		if (!lock)
		{
			m_wndEdit2.EnableWindow( m_bEnableWindows );
			m_wndEdit2.SetReadOnly(!m_bEnableWindows);
			m_wndEdit3.EnableWindow( m_bEnableWindows );
			m_wndEdit3.SetReadOnly(!m_bEnableWindows);
			m_wndEdit4.EnableWindow( m_bEnableWindows );
			m_wndEdit4.SetReadOnly(!m_bEnableWindows);
			m_wndDateTimeCtrl.EnableWindow( m_bEnableWindows );
		}
		setLock(lock);
	}


	int getStatus(void);
	void setStatus(void);

	void setLock(BOOL lock)
	{
		if (lock && !m_bIncludedInStandTmplate)
		{
			m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
			m_wndEdit1.EnableWindow( FALSE );
			m_wndEdit1.SetReadOnly( TRUE );
		}
		if (lock && m_bIncludedInStandTmplate)
		{
			m_wndEdit1.SetDisabledColor(  BLACK, INFOBK );
			m_wndEdit1.EnableWindow( FALSE );
			m_wndEdit1.SetReadOnly( TRUE );
		}
		if (!lock && !m_bIncludedInStandTmplate)
		{
			m_wndEdit1.EnableWindow( TRUE );
			m_wndEdit1.SetReadOnly( FALSE );
		}
		m_wndEdit2.EnableWindow( !lock );
		m_wndEdit2.SetReadOnly(lock);
		m_wndEdit3.EnableWindow( !lock );
		m_wndEdit3.SetReadOnly(lock);
		m_wndEdit4.EnableWindow( !lock );
		m_wndEdit4.SetReadOnly(lock);
		m_wndDateTimeCtrl.EnableWindow( !lock );
	}

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnSize(UINT nType,int cx,int cy);
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnEnKillfocusEdit2_1();
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnCBox2Changed(void);
	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};
