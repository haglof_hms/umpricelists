#pragma once

#include "GeneralInfoDlg.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortDoc

class CMDIAvgAssortDoc : public CDocument
{
protected: // create from serialization only
	CMDIAvgAssortDoc();
	DECLARE_DYNCREATE(CMDIAvgAssortDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIAvgAssortDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIAvgAssortDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIAvgAssortDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortFrame frame

class CMDIAvgAssortFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIAvgAssortFrame)

//private:
	BOOL m_bAlreadySaidOkToClose;

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgImportError;
	CString m_sMsgWindowsOpen;

	CXTPToolBar m_wndToolBar;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	BOOL m_bIsImportTBtn;
	BOOL m_bIsExportTBtn;
	BOOL m_bIsAddSpcTBtn;
	BOOL m_bIsRemoveTBtn;
	BOOL m_bIsPreviewTBtn;

	void setLanguage(void);

	void ToggleGeneralInfoPane(void);
	void ToggleDclsSettingsPane(void);

	void SetupPricelist(void);

	BOOL okToClose(void);

protected:

	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;


	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:
	CGeneralInfoDlg_2 m_wndGeneralInfoDlg;

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}

	BOOL isGeneralInofDlgHidden(void)
	{
		CXTPDockingPane* pPane = GetDockingPaneManager()->FindPane(IDC_GENERAL_INFO_PANE_2);
		ASSERT(pPane);
		if (pPane) 
		{
			return m_paneManager.IsPaneHidden(pPane);
		}
		return FALSE;
	}

	void setToolbarBtns(BOOL enable,BOOL locked)
	{
		m_bIsImportTBtn = TRUE;
		if (!locked)
		{
			m_bIsExportTBtn = enable;
			m_bIsAddSpcTBtn = enable;
			m_bIsRemoveTBtn = enable;
			m_bIsPreviewTBtn = enable;
		}
		else
		{
//			m_bIsImportTBtn = FALSE;
//			m_bIsExportTBtn = FALSE;
			m_bIsAddSpcTBtn = FALSE;
			m_bIsRemoveTBtn = FALSE;
//			m_bIsPreviewTBtn = FALSE;
		}
	}

	CMDIAvgAssortFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIAvgAssortFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;
// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint();
	afx_msg void OnClose();

	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSysCommand(UINT id,LONG param);
	afx_msg void OnChar(UINT nChar,UINT nRepCnt,UINT nFlags);

	// Toolbar button events; 060403 p�d
	afx_msg void OnTBtnOpenGeneralInfo();
	afx_msg void OnTBtnImportPricelist(void);
	afx_msg void OnTBtnExportPricelist(void);
	afx_msg void OnTBtnAddSpecie(void);
	afx_msg void OnTBtnRemoveSpecie(void);
	afx_msg void OnTBtnPreview(void);

	afx_msg void OnUpdateImportTBtn(CCmdUI*);
	afx_msg void OnUpdateExportTBtn(CCmdUI*);
	afx_msg void OnUpdateAddSpcTBtn(CCmdUI*);
	afx_msg void OnUpdateRemoveSpcTBtn(CCmdUI*);
	afx_msg void OnUpdatePreviewTBtn(CCmdUI*);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};




///////////////////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortListFrame frame

class CMDIAvgAssortListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIAvgAssortListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDIAvgAssortListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIAvgAssortListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

