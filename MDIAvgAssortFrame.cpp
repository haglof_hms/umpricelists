// MDIDataBaseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "MDIAvgAssortFrame.h"
#include "AvgAssortView.h"
#include "PrlViewAndPrint.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortDoc

IMPLEMENT_DYNCREATE(CMDIAvgAssortDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIAvgAssortDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIAvgAssortDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortDoc construction/destruction

CMDIAvgAssortDoc::CMDIAvgAssortDoc()
{
}

CMDIAvgAssortDoc::~CMDIAvgAssortDoc()
{
}

BOOL CMDIAvgAssortDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortDoc serialization

void CMDIAvgAssortDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortDoc diagnostics

#ifdef _DEBUG
void CMDIAvgAssortDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIAvgAssortDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIAvgAssortDoc commands


/////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortFrame


XTPDockingPanePaintTheme CMDIAvgAssortFrame::m_themeCurrent = xtpPaneThemeOffice2003;

IMPLEMENT_DYNCREATE(CMDIAvgAssortFrame, CMDIChildWnd)


BEGIN_MESSAGE_MAP(CMDIAvgAssortFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIAvgAssortFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_SYSCOMMAND()
	ON_WM_CHAR()
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	// Toolbar button events; 060403 p�d
	ON_COMMAND(ID_TBTN_GENERAL_INFO, OnTBtnOpenGeneralInfo)
	ON_COMMAND(ID_TBTN_IMPORT, OnTBtnImportPricelist)
	ON_COMMAND(ID_TBTN_EXPORT, OnTBtnExportPricelist)
	ON_COMMAND(ID_TBTN_ADDSPC, OnTBtnAddSpecie)
	ON_COMMAND(ID_TBTN_DELSPC, OnTBtnRemoveSpecie)
	ON_COMMAND(ID_TBTN_VIEW, OnTBtnPreview)

	ON_UPDATE_COMMAND_UI(ID_TBTN_IMPORT, OnUpdateImportTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_EXPORT, OnUpdateExportTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADDSPC, OnUpdateAddSpcTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DELSPC, OnUpdateRemoveSpcTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_VIEW, OnUpdatePreviewTBtn)

	ON_WM_GETMINMAXINFO()
	ON_WM_SETFOCUS()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIAvgAssortFrame::CMDIAvgAssortFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bIsImportTBtn = TRUE;
	m_bIsExportTBtn = TRUE;
	m_bIsAddSpcTBtn = TRUE;
	m_bIsRemoveTBtn = TRUE;
	m_bAlreadySaidOkToClose = FALSE;

}

CMDIAvgAssortFrame::~CMDIAvgAssortFrame()
{
}

void CMDIAvgAssortFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_CREATE_PRICELIST_KEY2);
	SavePlacement(this, csBuf);

	// Save state of docking pane(s)
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	m_paneManager.GetLayout(&layoutNormal);
	layoutNormal.Save((REG_WP_PRICELIST_LAYOUT_KEY2));

}

void CMDIAvgAssortFrame::OnClose(void)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070410 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	// Send messages to HMSShell, disable buttons on toolbar; 070410 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	if (m_bAlreadySaidOkToClose)
		CMDIChildWnd::OnClose();
	else	if (okToClose())
		CMDIChildWnd::OnClose();
}


void CMDIAvgAssortFrame::OnSysCommand( UINT id, LONG lp)
{
	if ((id & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (okToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(id,lp);
		}
	}
	else if ((id & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(id,lp);
}

BOOL CMDIAvgAssortFrame::okToClose(void)
{
	// Do save here, so e.g. "ESCAPE"'ll work on savin'; 080114 p�d
	CAvgAssortView *pView = (CAvgAssortView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView)
	{
		return pView->saveData(CAvgAssortView::ON_QUIT);
	}	// if (pView)

	return FALSE;
}


void CMDIAvgAssortFrame::OnChar(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CMDIChildWnd::OnChar(nChar,nRepCnt,nFlags);
}

int CMDIAvgAssortFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;
	CString sToolTip3;
	CString sToolTip4;
	CString sToolTip5;
	CString sToolTip6;
	CString sToolTip7;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sToolTip1 = xml->str(IDS_STRING121);
			sToolTip2 = xml->str(IDS_STRING122);
			sToolTip3 = xml->str(IDS_STRING140);
			sToolTip4 = xml->str(IDS_STRING141);
			sToolTip5 = xml->str(IDS_STRING142);
			sToolTip6 = xml->str(IDS_STRING143);
			sToolTip7 = xml->str(IDS_STRING1901);

			m_sMsgCap  = xml->str(IDS_STRING151);
			m_sMsgImportError.Format(_T("%s\n%s\n\n%s\n"),
					xml->str(IDS_STRING1702),
					xml->str(IDS_STRING1703),
					xml->str(IDS_STRING1704));
			m_sMsgWindowsOpen.Format(_T("%s\n%s\n\n"),
					xml->str(IDS_STRING2300),
					xml->str(IDS_STRING2301));

		}
		delete xml;
	}

	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_INFO,sToolTip1);	//
					// D�lj knapp
					setToolbarBtnIcon(sTBResFN,p->GetAt(1),-1,_T(""),FALSE);	// 2010-05-20 p�d
// Visa knapp
//					setToolbarBtnIcon(sTBResFN,p->GetAt(1),RES_TB_GENERAL,sToolTip2);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(2),RES_TB_IMPORT,sToolTip3);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(3),RES_TB_EXPORT,sToolTip4);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(4),RES_TB_ADD,sToolTip5);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(5),RES_TB_DEL,sToolTip6);	//
					setToolbarBtnIcon(sTBResFN,p->GetAt(6),RES_TB_PREVIEW,sToolTip7);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))


	// Get window settings from registry; 060113 p�d

	// control bars objects have been created and docked.
	m_paneManager.InstallDockingPanes(this);

	CXTPDockingPane *paneGeneralData = m_paneManager.CreatePane(IDC_GENERAL_INFO_PANE_2, CRect(0, 0, 0, 140), xtpPaneDockTop);
	paneGeneralData->SetOptions( xtpPaneNoCloseable );

	setLanguage();

	m_paneManager.SetTheme(m_themeCurrent);
	// Load the previous state for docking panes.
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	if (layoutNormal.Load((REG_WP_PRICELIST_LAYOUT_KEY2)))
	{
		m_paneManager.SetLayout(&layoutNormal);
	}

	SetupPricelist();

	m_bFirstOpen = TRUE;

	BOOL bWindowsOpen = FALSE;
  CString sDocName = L"", sMsg = L"", sWindows = L"";
  CDocTemplate *pTemplate = NULL;
  CWinApp *pApp = AfxGetApp();

	// Loop through doc templates
  POSITION pos = pApp->GetFirstDocTemplatePosition();
  while(pos)
  {
		pTemplate = pApp->GetNextDocTemplate(pos);
    pTemplate->GetDocString(sDocName, CDocTemplate::docName);

    // Check for instances of this document (ignore database manager document)
    POSITION posDOC = pTemplate->GetFirstDocPosition();
    if (posDOC && (sDocName.CompareNoCase(_T("View5000")) == 0 || // UMEstimate
									 sDocName.CompareNoCase(_T("Module5100")) == 0 ||	// UMTemplates (Best�ndsmall)
									 sDocName.CompareNoCase(_T("Module5200")) == 0))	// UMLandValue
    {
			CDocument* pDoc = (CDocument*)pTemplate->GetNextDoc(posDOC);
			sWindows += _T(" * ") + pDoc->GetTitle() + _T("\n");
			bWindowsOpen = TRUE;
    }
  }
  if( bWindowsOpen )
  {
		// Display list of open child windows
     sMsg.Format(_T("%s\n%s\n"),m_sMsgWindowsOpen,sWindows);
     ::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
  }

	return 0; // creation ok
}

BOOL CMDIAvgAssortFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CMDIAvgAssortFrame::PreTranslateMessage(MSG* pMsg)
{
	return CMDIChildWnd::PreTranslateMessage(pMsg);
}

void CMDIAvgAssortFrame::OnSetFocus(CWnd *wnd)
{
	int nIndex,nItems;
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	CDocument *pDoc = GetActiveDocument();
	
	nIndex = nItems = 0;
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CAvgAssortView *pView = (CAvgAssortView*)pDoc->GetNextView(pos);
			if (pView)
			{
				nIndex = pView->getDBNavigationCounter();
				nItems = pView->getNumOfItemsInPricelist();
				pView->doSetNavigationBar();
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,nIndex > 0);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,nIndex > 0);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,nIndex < nItems - 1);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,nIndex < nItems - 1);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);
}


// CMDIAvgAssortFrame diagnostics

#ifdef _DEBUG
void CMDIAvgAssortFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIAvgAssortFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIAvgAssortFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

// PRIVATE

void CMDIAvgAssortFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIAvgAssortFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_CREATE_PRICELIST_KEY2);
		LoadPlacement(this, csBuf);
  }
}

void CMDIAvgAssortFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PRICELISTS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PRICELISTS;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIAvgAssortFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

LRESULT CMDIAvgAssortFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		// get a pointer to the docking pane being shown.
		CXTPDockingPane* pPane = (CXTPDockingPane*)lParam;
		if (!pPane->IsValid())
		{
			if (pPane->GetID() == IDC_GENERAL_INFO_PANE_2)
			{
				if (m_wndGeneralInfoDlg.Create(CGeneralInfoDlg_2::IDD, this))
				{
					pPane->Attach(&m_wndGeneralInfoDlg);
/*
					// We need to do this, only if the Pane is hidden. Otherwise
					// information is displayed anyway; 070208 p�d
					if (pPane->IsHidden())
					{
						// Add information on General data. E.g. Name,Date,Done by; 070208 p�d
						CAvgAssortView* pView = (CAvgAssortView*)getFormViewByID(IDD_FORMVIEW2);
						if (pView != NULL)
						{
							TCHAR szName[127];
							TCHAR szDoneBy[127];
							TCHAR szDate[127];
							TCHAR szNotes[512];
							int nRetVal;

							pView->getXMLHeaderData(szName,szDoneBy,szDate,szNotes,&nRetVal);
							if (nRetVal > -1)
								m_wndGeneralInfoDlg.setStatusOfPricelist(nRetVal);
							else if (nRetVal == -2)
								m_wndGeneralInfoDlg.setStatusOfPricelist(nRetVal);
							m_wndGeneralInfoDlg.setNameOfPricelist(szName);
							m_wndGeneralInfoDlg.setNameOfCreator(szDoneBy);
							m_wndGeneralInfoDlg.setNotes(szNotes);
						}	// if (pView != NULL)
					}	// if (pPane->IsHidden())
*/
				}	// if (m_wndGeneralInfoDlg.Create(CGeneralInfoDlg::IDD, this))
			}	// if (pPane->GetID() == IDC_GENERAL_INFO_PANE_2)
		}	// if (!pPane->IsValid())
		
		return TRUE; // Handled
	}	// if (wParam == XTP_DPN_SHOWWINDOW)

	
	return FALSE;
}


// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIAvgAssortFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW3,m_sLangFN,lParam);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{	
			POSITION pos = pDoc->GetFirstViewPosition();
			if (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}


void CMDIAvgAssortFrame::OnTBtnOpenGeneralInfo()
{
	ToggleGeneralInfoPane();
}

void CMDIAvgAssortFrame::OnTBtnImportPricelist(void)
{
	CString sName;
	CString sXMLFile;
	CString sFilter;
	CDocument *pDoc = GetActiveDocument();
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING100),PRICELIST_FN_EXTA,PRICELIST_FN_EXTA);
		}
		delete xml;
	}
			
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CAvgAssortView *pView = (CAvgAssortView*)pDoc->GetNextView(pos);
			if (pView)
			{
				// Handles clik on open button
				CFileDialog dlg( TRUE, PRICELIST_FN_EXTA, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
												sFilter, this);
	
				if(dlg.DoModal() == IDOK)
				{
					PricelistParser prlParser;
					if (prlParser.LoadFromFile(dlg.GetPathName()))
					{
						int nDummy;
						if (prlParser.getHeaderPriceIn(&nDummy))
						{
							::MessageBox(this->GetSafeHwnd(),m_sMsgImportError,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
						}
						else
						{
							pView->saveFromXMLFileToPricelistDB(dlg.GetPathName());
//							pView->saveFromXMLFileToPricelistDB(dlg.GetFileName());
							pView->doSetNavigationBar();
						}
					}
				}
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIAvgAssortFrame::OnTBtnExportPricelist(void)
{
	CString sName;
	CString sXMLFile;
	CString sFilter;
	CDocument *pDoc = GetActiveDocument();

	//--------------------------------------------------------------------------
	// Save pricelist, before doin' an export; 091012 p�d
	CAvgAssortView *pView = (CAvgAssortView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView)
	{
		if (!pView->saveData(CAvgAssortView::ON_SAVE)) return;
	}	// if (pView)
	//--------------------------------------------------------------------------

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING100),PRICELIST_FN_EXTA,PRICELIST_FN_EXTA);
		}
		delete xml;
	}
			
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CAvgAssortView *pView = (CAvgAssortView*)pDoc->GetNextView(pos);
			if (pView)
			{
				if (pView->getPricelistXML(sName,sXMLFile))
				{
					scanFileName(sName);
					// Check if fileextension, if not add to filename; 090304 p�d
					if (sName.Right(6) != PRICELIST_FN_EXTA)
						sName += PRICELIST_FN_EXTA;
					// Handles clik on open button
					CFileDialog dlg( FALSE, PRICELIST_FN_EXTA, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
													sFilter, this);
	
					if(dlg.DoModal() == IDOK)
					{
						sName = dlg.GetPathName();
						if (sName.Right(6) != PRICELIST_FN_EXTA)
							sName += PRICELIST_FN_EXTA;
						pView->savePricelistToXMLFile(sName,sXMLFile);
					}
				}	// if (pView->getPricelistXML(sData))
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIAvgAssortFrame::OnTBtnAddSpecie(void)
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CAvgAssortView *pView = (CAvgAssortView*)pDoc->GetNextView(pos);
			if (pView)
			{
				pView->addSpecieToPricelist();
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIAvgAssortFrame::OnTBtnRemoveSpecie(void)
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			CAvgAssortView *pView = (CAvgAssortView*)pDoc->GetNextView(pos);
			if (pView)
			{
				pView->removeSpecieFromPricelist();
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIAvgAssortFrame::OnTBtnPreview(void)
{
	CString sName;
	CString sXMLFile;
	CAvgAssortView *pPrlView = NULL;
	CPrlViewAndPrint *pPrintView = NULL;

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if (pos != NULL)
		{
			pPrlView = (CAvgAssortView*)pDoc->GetNextView(pos);
			if (pPrlView)
			{
				pPrlView->getPricelistXML(sName,sXMLFile);
				pPrintView = (CPrlViewAndPrint*)showFormView(IDD_FORMVIEW4,m_sLangFN,(LPARAM)(LPCTSTR)sXMLFile);
			}	// if (pView)
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
}

void CMDIAvgAssortFrame::OnUpdateImportTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsImportTBtn );
}

void CMDIAvgAssortFrame::OnUpdateExportTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsExportTBtn );
}

void CMDIAvgAssortFrame::OnUpdateAddSpcTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsAddSpcTBtn );
}

void CMDIAvgAssortFrame::OnUpdateRemoveSpcTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsRemoveTBtn );
}

void CMDIAvgAssortFrame::OnUpdatePreviewTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPreviewTBtn );
}

// MY METHODS
void CMDIAvgAssortFrame::setLanguage()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			/////////////////////////////////////////////////////////////////////////////////////
			// Set caption for Geanral information pane; 060403 p�d
			CXTPDockingPane* pPaneGeneralData = GetDockingPaneManager()->FindPane(IDC_GENERAL_INFO_PANE_2);
			ASSERT(pPaneGeneralData);
			if (pPaneGeneralData) 
			{
				pPaneGeneralData->SetTitle((xml->str(IDS_STRING119)));
				pPaneGeneralData->Hide();
			}
		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
}

void CMDIAvgAssortFrame::ToggleGeneralInfoPane(void)
{
	CXTPDockingPane* pPane = GetDockingPaneManager()->FindPane(IDC_GENERAL_INFO_PANE_2);
	ASSERT(pPane);
	if (pPane) 
	{
		m_paneManager.ToggleAutoHide(pPane);
	}
}

void CMDIAvgAssortFrame::SetupPricelist(void)
{
	CAvgAssortView *pView = (CAvgAssortView *)GetActiveFrame()->GetActiveView();
	if (pView)
	{
		pView->setupPricelistOnStartup();
	}
}


/////////////////////////////////////////////////////////////////////////////
// CMDIAvgAssortListFrame


IMPLEMENT_DYNCREATE(CMDIAvgAssortListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIAvgAssortListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIAvgAssortListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIAvgAssortListFrame::CMDIAvgAssortListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIAvgAssortListFrame::~CMDIAvgAssortListFrame()
{
}

void CMDIAvgAssortListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LIST_PRICELIST_KEY2);
	SavePlacement(this, csBuf);
}

int CMDIAvgAssortListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIAvgAssortListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIAvgAssortListFrame diagnostics

#ifdef _DEBUG
void CMDIAvgAssortListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIAvgAssortListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIAvgAssortListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CMDIAvgAssortListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIAvgAssortListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LIST_PRICELIST_KEY2);
		LoadPlacement(this, csBuf);
  }
}

void CMDIAvgAssortListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIAvgAssortListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PRICELISTS_LIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PRICELISTS_LIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIAvgAssortListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIAvgAssortListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDIAvgAssortListFrame::setLanguage()
{
/*
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
*/
}

