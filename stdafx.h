// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN        // Exclude rarely-used stuff from Windows headers

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>         // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include <SQLAPI.h> // main SQLAPI++ header

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include "pad_hms_miscfunc.h"			// HMSFuncLib
#include "pad_transaction_classes.h"	// HMSFuncLib
#include "pricelistparser.h"			// HMSFuncLib
#include "templateparser.h"	

#include "DatePickerCombo.h"

#include "MDIPricelistsFrame.h"

#include "UMPricelistDB.h"


/////////////////////////////////////////////////////////////////////
// My own includes

#define WM_USER_MSG_SUITE				(WM_USER + 2)		// Used for messages sent from a Suite/usermodule
#define USER_MSG_SHELL_TREE 			(WM_USER + 3)		// This is a identifer used when communication from a
#define USER_MSG_EXECUTE_SHELL			(WM_USER + 4)		// This is a identifer used when communication from a

#define MSG_IN_SUITE				 	(WM_USER + 10)		// This identifer's used to send messages internally
#define MSG_IN_SUITE2				 	(WM_USER + 11)		// This identifer's used to send messages internally

/////////////////////////////////////////////////////////////////////
// String id
#define IDS_STRING100								100
#define IDS_STRING101								101
#define IDS_STRING102								102
#define IDS_STRING103								103
#define IDS_STRING104								104
#define IDS_STRING105								105
#define IDS_STRING106								106
#define IDS_STRING107								107
#define IDS_STRING1070							1070
#define IDS_STRING1071							1071
#define IDS_STRING1072							1072
#define IDS_STRING1073							1073
#define IDS_STRING1074							1074
#define IDS_STRING1075							1075
#define IDS_STRING1076							1076
#define IDS_STRING1077							1077
#define IDS_STRING1078							1078
#define IDS_STRING108								108
#define IDS_STRING1080							1080
#define IDS_STRING109								109
#define IDS_STRING110								110
#define IDS_STRING111								111
#define IDS_STRING112								112
#define IDS_STRING113								113
#define IDS_STRING114								114
#define IDS_STRING115								115
#define IDS_STRING116								116
#define IDS_STRING117								117
#define IDS_STRING118								118
#define IDS_STRING119								119
#define IDS_STRING120								120
#define IDS_STRING121								121
#define IDS_STRING122								122
#define IDS_STRING123								123
#define IDS_STRING124								124
#define IDS_STRING125								125
#define IDS_STRING126								126
#define IDS_STRING127								127
#define IDS_STRING128								128
#define IDS_STRING129								129
#define IDS_STRING130								130
#define IDS_STRING131								131
#define IDS_STRING132								132
#define IDS_STRING133								133
#define IDS_STRING134								134
#define IDS_STRING135								135
#define IDS_STRING136								136
#define IDS_STRING137								137
#define IDS_STRING138								138
#define IDS_STRING139								139
#define IDS_STRING140								140
#define IDS_STRING141								141
#define IDS_STRING142								142
#define IDS_STRING143								143
#define IDS_STRING144								144
#define IDS_STRING145								145
#define IDS_STRING1451							1451
#define IDS_STRING146								146
#define IDS_STRING147								147
#define IDS_STRING148								148
#define IDS_STRING149								149
#define IDS_STRING150								150
#define IDS_STRING151								151
#define IDS_STRING152								152
#define IDS_STRING153								153
#define IDS_STRING154								154
#define IDS_STRING1540							1540
#define IDS_STRING1541							1541
#define IDS_STRING1542							1542
#define IDS_STRING1543							1543
#define IDS_STRING1544							1544
#define IDS_STRING1545							1545
#define IDS_STRING1550							1550
#define IDS_STRING1551							1551
#define IDS_STRING1552							1552	// 071204 p�d

#define IDS_STRING156								156

#define IDS_STRING1570							1570
#define IDS_STRING1571							1571

#define IDS_STRING1580							1580
#define IDS_STRING1581							1581
#define IDS_STRING1582							1582
#define IDS_STRING1583							1583

#define IDS_STRING159								159
#define IDS_STRING1590							1590
#define IDS_STRING1591							1591
#define IDS_STRING1592							1592
#define IDS_STRING1593							1593
#define IDS_STRING1594							1594
#define IDS_STRING1595							1595
#define IDS_STRING1596							1596
#define IDS_STRING1597							1597
#define IDS_STRING1598							1598
#define IDS_STRING1599							1599

#define IDS_STRING160								160
#define IDS_STRING1600							1600
#define IDS_STRING1601							1601
#define IDS_STRING1602							1602

#define IDS_STRING161								161
#define IDS_STRING162								162

#define IDS_STRING163								163
#define IDS_STRING1630							1630
#define IDS_STRING1631							1631

#define IDS_STRING1640							1640
#define IDS_STRING1641							1641

#define IDS_STRING1650							1650

#define IDS_STRING166								166
#define IDS_STRING1660							1660
#define IDS_STRING1661							1661

#define IDS_STRING1670							1670
#define IDS_STRING1671							1671
#define IDS_STRING1672							1672
#define IDS_STRING1673							1673

#define IDS_STRING1674							1674
#define IDS_STRING1675							1675
#define IDS_STRING1676							1676

#define IDS_STRING1700							1700
#define IDS_STRING1701							1701
#define IDS_STRING1702							1702
#define IDS_STRING1703							1703
#define IDS_STRING1704							1704

#define IDS_STRING1800							1800
#define IDS_STRING1801							1801

#define IDS_STRING1900							1900
#define IDS_STRING1901							1901
#define IDS_STRING1902							1902
#define IDS_STRING1903							1903
#define IDS_STRING1904							1904

#define IDS_STRING2000							2000
#define IDS_STRING2001							2001
#define IDS_STRING2002							2002

#define IDS_STRING2100							2100

#define IDS_STRING2200							2200
#define IDS_STRING2201							2201
#define IDS_STRING2202							2202

#define IDS_STRING2300							2300
#define IDS_STRING2301							2301

#define IDS_STRING2310	2310
#define IDS_STRING2311	2311
#define IDS_STRING2312	2312
#define IDS_STRING2313	2313
#define IDS_STRING2314	2314

#define IDS_STRING2320	2320
#define IDS_STRING2321	2321
#define IDS_STRING2322	2322
#define IDS_STRING2323	2323
#define IDS_STRING2324	2324

#define IDS_STRING2325	2325
#define IDS_STRING2326	2326

#define IDC_TABCONTROL_1						0x8101
#define IDC_DATAENTER_DLG						0x8102
#define IDC_TABCONTROL_2						0x8103

#define IDC_PRICELIST								0x8104
#define IDC_QUALDESC								0x8105
#define IDC_SETTINGS_PANE						0x8106
#define IDC_PRICELIST_QUAL					0x8107
#define IDC_GENERAL_INFO_PANE_1			0x8108
#define IDC_ASSORTMENTS_PANE				0x8109
#define IDC_PRICELISTS_LIST					0x8110
#define IDC_GENERAL_INFO_PANE_2			0x8111
#define IDC_ASSORTMENTS_1						0x8112
#define IDC_ASSORTMENTS_2						0x8113
#define IDC_PRINT_PRL								0x8114
#define IDC_CHANGE_PRICE_PANE				0x8115

#define ID_SHOWVIEW_MSG							0x8116

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6
};


// Message in suite, from Tab control to by MSG_IN_SUITE
#define ID_TAB_CLICKED								0x8209

#define ID_REPORT_CTRL								0x8210

#define ID_MSG_SELECT_PRICELIST				0x8211

#define ID_POPUPMENU_QUALITY					0x8212

#define ID_TAB_QUALDESC_CTRL					0x8213

// Identifies that this is a pricelist used for calulation (utbyte)
// using Rune Ollas calculations; 070430 p�d
#define ID_PRICELIST_IN_DEVELOPEMENT_1			-1	// Pricelist isn't ready yet; 071219 p�d
#define ID_PRICELIST_IN_DEVELOPEMENT_2			-2	// Pricelist isn't ready yet; 071219 p�d
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_1		1		// Pricelist for R. Ollars
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_2		2		// "Simple" avg. prices per assortment

//////////////////////////////////////////////////////////////////////////////////////////
// Number of panes in splitter; 090127 p�d
#define NUMBER_OF_PANES							3
#define PANE_1_INDEX							0
#define PANE_2_INDEX							1
#define PANE_3_INDEX							2

//////////////////////////////////////////////////////////////////////////////////////////
// this enuerated type is used to set Action: New or Update; 060329 p�d
typedef enum { 
								PRICELIST_NEW,
								PRICELIST_OPEN,
						 } enumPricelistState;

/////////////////////////////////////////////////////////////////////
// Registry settingd; 060209 p�d
//const LPCTSTR REG_LANG_KEY			= "Language";
//const LPCTSTR REG_LANG_ITEM			= "LANGSET";					// Item in registry, for abbrevated language; 051114 p�d

//const LPCTSTR INIT_MODULE_FUNC		= "InitModule";				// Functionname in Module, for init; 051129 p�d
const LPCTSTR PROGRAM_NAME				= _T("UMPricelists");			// Used for Languagefile, registry entries etc.; 051114 p�d

const LPCTSTR PRICELISTS_NAME			= _T("PricelistFrame");		// Used for Languagefile.; 051114 p�d
const LPCTSTR PRICELISTS_LIST_NAME		= _T("PricelistListFrame");		// Used for Languagefile; 060412 p�d

//const LPCTSTR LANGUAGE_FN_EXT			= ".xml";							// Uses ordinary xm-file
const LPCTSTR PRICELIST_FN_EXTE			= _T(".hepxl");				// Extension for Exported/Imported templates; 081201 p�d
const LPCTSTR PRICELIST_FN_EXTA			= _T(".hapxl");				// Extension for Exported/Imported templates; 081201 p�d

const LPCTSTR UMDATABASE_NAME			= _T("UMDataBase.dll");		// Dll handling ALL database transactions; 060209 p�d
const LPCSTR GET_EXPORTED_FUNC_NAME		= "getExportedFuncName";


const LPCTSTR REG_CREATE_PRICELIST_KEY1		= _T("UMPricelists\\CreatePricelist\\Placement");
const LPCTSTR REG_LIST_PRICELIST_KEY1		= _T("UMPricelists\\ListPricelists\\Placement");

const LPCTSTR REG_CREATE_PRICELIST_KEY2		= _T("UMAvgAssort\\CreatePricelist\\Placement");
const LPCTSTR REG_LIST_PRICELIST_KEY2		= _T("UMAvgAssort\\ListPricelists\\Placement");

const LPCTSTR REG_WP_PRICELIST_LAYOUT_KEY1	= _T("UMPricelist_Layout");	// 070206 p�d
const LPCTSTR REG_WP_PRICELIST_LAYOUT_KEY2	= _T("UMAvgAssort_Layout");	// 070607 p�d

const LPCTSTR REG_PRINT_PRICELIST_KEY		= _T("UMAvgAssort\\PrintPricelists\\Placement");


//////////////////////////////////////////////////////////////////////////////////////////
//	Main resource dll, for toolbar icons; 051219 p�d

//const LPCTSTR TOOLBAR_RES_DLL				= _T("HMSToolBarIcons32.dll");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d
const LPCTSTR TOOLBAR_RES_DLL				= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 091021 p�d

const int RES_TB_PRINT						= 36;
const int RES_TB_IMPORT						= 19;
const int RES_TB_EXPORT						= 9;
const int RES_TB_INFO						= 20;
const int RES_TB_GENERAL					= 40;
const int RES_TB_PREVIEW					= 31;
const int RES_TB_ADD						= 0;
const int RES_TB_DEL						= 28;
//////////////////////////////////////////////////////////////////////////////////////////
// Setup pricelist xml file tags; 060405 p�d

const LPCTSTR TAG_FIRST_ROW					= _T("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

const LPCTSTR TAG_START_PRICELIST			= _T("<Pricelist>");
const LPCTSTR TAG_END_PRICELIST				= _T("</Pricelist>");

const LPCTSTR TAG_START_HEADER				= _T("<Header>");
const LPCTSTR TAG_HEADER_NAME				= _T("<Name>%s</Name>");
const LPCTSTR TAG_HEADER_DONE_BY			= _T("<Done_by>%s</Done_by>");
const LPCTSTR TAG_HEADER_DATE				= _T("<Date>%s</Date>");
const LPCTSTR TAG_HEADER_NOTES				= _T("<Notes>%s</Notes>");
const LPCTSTR TAG_HEADER_PRICE_IN			= _T("<Timberprice_in>%d</Timberprice_in>");	// Added 080423 p�d
const LPCTSTR TAG_END_HEADER				= _T("</Header>");

const LPCTSTR TAG_START_SPECIE				= _T("<Specie id=\"%d\" name=\"%s\">");
const LPCTSTR TAG_END_SPECIE				= _T("</Specie>");

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s); 060209 p�d

const int MIN_X_SIZE_PRICELISTS				= 700;
const int MIN_Y_SIZE_PRICELISTS				= 500;

const int MIN_X_SIZE_PRICELISTS_LIST		= 450;
const int MIN_Y_SIZE_PRICELISTS_LIST		= 300;

//////////////////////////////////////////////////////////////////////////////////////////
// Define column width; 060331 p�d
const int COL_WIDTH							= 50;

//////////////////////////////////////////////////////////////////////////////////////////
//	Min,Max vaules for Assortments
//	Pulpwood min diam = 5.0 cm
//	Timber between 10.0 - 25.0 cm
const double ASSORTMENT_PULPWOOD_MINDIAM	= 5.0;
const double ASSORTMENT_PULPWOOD_MAXDIAM	= 7.0;
const double ASSORTMENT_TIMBER_MINDIAM		= 10.0;
const double ASSORTMENT_TIMBER_MAXDIAM		= 25.0;

//////////////////////////////////////////////////////////////////////////////////////////
// Name of Tables in Forrest database; 061002 p�d

const LPCTSTR SQL_SERVER_SQRIPT_DIRECTORY	= _T("Microsoft SQL Server");

const LPCTSTR TBL_PRICELISTS				= _T("prn_pricelist_table");
const LPCTSTR TBL_SPECIES						= _T("fst_species_table");

const LPCTSTR TBL_TEMPLATE					= _T("tmpl_template_table");
const LPCTSTR TBL_TRAKT_MISC_DATA		= _T("esti_trakt_misc_data_table");

//////////////////////////////////////////////////////////////////////////////////////////
// SQL Script files, used in GallBas; 061109 p�d
const LPCTSTR PRICELIST_TABLE					= _T("Create_pricelist_table.sql");

/////////////////////////////////////////////////////////////////////////////
// CAssortSettingsDataRec
const TCHAR sz1dec[] = _T("%.1f");
const TCHAR sz2dec[] = _T("%.2f");

/////////////////////////////////////////////////////////////////////////////
//	Create table; 080701 p�d
#ifdef UNICODE
const LPCTSTR table_Pricelist = _T("CREATE TABLE dbo.%s (")
															 _T("id INT NOT NULL IDENTITY(1,1),")
															 _T("name NVARCHAR(50),")
															 _T("type_of int,")				/* Idetifies type of pricelist. ex. 1 = Rune Ollas type of pricelist */\
															 _T("pricelist NTEXT,")			/* Pricelist in xml format */\
															 _T("created_by NVARCHAR(20),")
															 _T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
															 _T("PRIMARY KEY (ID));");
#else
const LPCTSTR table_Pricelist = _T("CREATE TABLE dbo.%s (")
															 _T("id INT NOT NULL IDENTITY(1,1),")
															 _T("name VARCHAR(50),")
															 _T("type_of int,")				/* Idetifies type of pricelist. ex. 1 = Rune Ollas type of pricelist */\
															 _T("pricelist NTEXT,")			/* Pricelist in xml format */\
															 _T("created_by VARCHAR(20),")
															 _T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
															 _T("PRIMARY KEY (ID));");
#endif

const LPCTSTR PRL_VIEW_FN									= _T("tmp_prl_view.htm");

/////////////////////////////////////////////////////////////////////////////
// CPricelistReportDataRec

class CPricelistReportDataRec : public CXTPReportRecord
{
	//private:
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	
		{ 
			return m_sText; 
		}
		
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};


	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	
		{ 
			return m_nValue; 
		}
		
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(m_nValue);
		}
	};


public:
	CPricelistReportDataRec(void)
	{
		AddItem(new CTextItem(_T("")));
	}

	CPricelistReportDataRec(int num,LPCTSTR value = _T(""))
	{
		for (int i = 0;i < num;i++)
		{
			if (i == 0)
			{
				AddItem(new CTextItem(value));
			}
			else
			{
				AddItem(new CIntItem());
			}
		}
	}

	CPricelistReportDataRec(CTransaction_prl_data rec)
	{
		AddItem(new CTextItem(rec.getQualName()));
		if (rec.getInts().size() > 0)
		{
			for (UINT i = 0;i < rec.getInts().size();i++)
			{
				AddItem(new CIntItem(rec.getInts()[i]));
			}
		}
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

// Special method; returns all colum items as text; 060405 p�d
	CString getColumnsInText(int item)	
	{ 
		CString sData;
		switch (item)
		{
			case 0 : sData = getColumnText(item); break;
			default : sData.Format(_T("%d"),getColumnInt(item)); break;
		};
		return sData;
	}

	int getColumnInt(int item)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;
	}
	
	void setColumnInt(int item,int value)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			((CIntItem*)GetItem(item))->setIntItem(value);
	}

};

/////////////////////////////////////////////////////////////////////////////
// CPricelistsListReportDataRec 060418 p�d

class CPricelistsListReportDataRec : public CXTPReportRecord
{
	//private:
	int m_nID;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	
		{ 
			return m_sText; 
		}
		
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};


	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	
		{ 
			return m_nValue; 
		}
		
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(m_nValue);
		}
	};


public:
	CPricelistsListReportDataRec(void)
	{
		m_nID = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}


	CPricelistsListReportDataRec(CTransaction_pricelist rec)
	{
		m_nID = rec.getID();
		AddItem(new CTextItem((rec.getName())));
		AddItem(new CTextItem((rec.getCreatedBy())));
		AddItem(new CTextItem((rec.getCreated())));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	int getColumnInt(int item)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;
	}
	
	void setColumnInt(int item,int value)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			((CIntItem*)GetItem(item))->setIntItem(value);
	}

	int getID(void)
	{
		return m_nID;
	}
};

/////////////////////////////////////////////////////////////////////////////
// CQualDescReportDataRec

class CQualDescReportDataRec : public CXTPReportRecord
{
	//private:
	CString m_sPaintMsg;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	
		{ 
			return m_sText; 
		}
		
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};


	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	
		{ 
			return m_nValue; 
		}
		
		void setIntItem(int value)	
		{ 
			m_nValue = value; 
			SetValue(m_nValue);
		}

	};

public:
	CQualDescReportDataRec(void)
	{
		AddItem(new CTextItem(_T("")));
	}

	CQualDescReportDataRec(CTransaction_prl_data rec)
	{
		AddItem(new CTextItem(rec.getQualName()));
		if (rec.getInts().size() > 0)
		{
			for (UINT i = 0;i < rec.getInts().size();i++)
			{
				AddItem(new CIntItem(rec.getInts()[i]));
			}
		}
	}

	CQualDescReportDataRec(int num,LPCTSTR value = _T(""))
	{
		for (int i = 0;i < num;i++)
		{
			if (i == 0)
			{
				AddItem(new CTextItem(value));
			}
			else
			{
				AddItem(new CIntItem());
			}
		}
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

// Special method; returns all colum items as text; 060405 p�d
	CString getColumnsInText(int item)	
	{ 
		CString sData;
		switch (item)
		{
			case 0 : sData = getColumnText(item); break;
			default : sData.Format(_T("%d"),getColumnInt(item)); break;
		};
		return sData;
	}

	int getColumnInt(int item)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;
	}
	
	void setColumnInt(int item,int value)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			((CIntItem*)GetItem(item))->setIntItem(value);
	}
};

//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

CString getResStr(UINT id);

CString getToolBarResourceFN(void);

// Get name of an Exported function in UMDataBase usermodule; 060317 p�d
CString getDBExportedFuncName(enumFuncNames);

BOOL getSpecies(DB_CONNECTION_DATA &conn,vecTransactionSpecies &);

BOOL savePricelistToDB(DB_CONNECTION_DATA &conn,vecTransactionPricelist &);
BOOL savePricelistToDB(DB_CONNECTION_DATA &conn,CTransaction_pricelist &);
BOOL removePricelistFromDatabase(DB_CONNECTION_DATA &conn,CTransaction_pricelist &);
BOOL getPricelists(DB_CONNECTION_DATA &conn,vecTransactionPricelist &,int);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp);
CView *getFormViewByID(int idd);
CWnd *getFormViewParentByID(int idd);

BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name);

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);

void setGlobalLock_matris(BOOL lock);
void setGlobalLock_avg(BOOL lock);

BOOL checkPricelist(LPCTSTR prl_xml);
BOOL checkPricelist_matris();
BOOL checkPricelist_avg();

