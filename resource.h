//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMPricelists.rc
//
#define IDD_FORMVIEW                    620
#define IDD_FORMVIEW1                   621
#define IDD_FORMVIEW2                   622
#define IDD_FORMVIEW3                   623
#define IDD_FORMVIEW4                   624
#define IDD_DIALOGBAR3                  21996
#define IDD_DIALOGBAR                   21997
#define IDD_DIALOGBAR1                  21998
#define IDD_DIALOGBAR2                  21999
#define IDI_FORMICON                    22000
#define IDC_EDIT1                       22001
#define IDB_TAB_ICONS                   22002
#define IDC_EDIT2                       22002
#define IDC_EDIT3                       22003
#define IDR_TOOLBAR1                    22003
#define IDC_LBL1                        22004
#define IDC_LBL2                        22005
#define IDD_DIALOG1                     22005
#define IDC_LBL3                        22006
#define IDD_DIALOG2                     22006
#define IDC_BUTTON1                     22007
#define IDC_BUTTON2                     22008
#define IDB_BITMAP1                     22009
#define IDC_BUTTON4                     22009
#define IDC_LIST2                       22010
#define IDB_BITMAP2                     22010
#define IDC_BUTTON5                     22010
#define IDC_BUTTON3                     22011
#define ID_TBTN_GENERAL_INFO            22012
#define ID_TBTN_DIAMCLASS_SETTINGS      22013
#define IDC_DATETIMEPICKER1             22013
#define IDR_TOOLBAR2                    22013
#define IDC_EDIT4                       22014
#define IDC_BUTTON6                     22014
#define IDC_BUTTON7                     22015
#define IDD_DIALOG3                     22015
#define IDC_LBL4                        22016
#define IDB_BITMAP3                     22016
#define ID_TBTN_IMPORT                  22017
#define IDC_LBL6                        22017
#define IDB_BITMAP4                     22017
#define ID_TBTN_EXPORT                  22018
#define ID_TBTN_ADDSPC                  22019
#define IDC_GRP1                        22019
#define ID_TBTN_DELSPC                  22020
#define IDC_GRP2                        22020
#define IDC_GRP3                        22021
#define IDB_BMP_PLUS                    22021
#define IDC_LBL2_1                      22022
#define IDB_BMP_MINUS                   22022
#define IDC_LBL2_2                      22023
#define IDB_BITMAP5                     22023
#define IDB_BMP_MAGNI_GLASS             22023
#define IDC_LBL2_3                      22024
#define IDC_EDIT2_4                     22025
#define IDC_LBL2_4                      22026
#define IDC_EDIT2_1                     22027
#define IDC_EDIT2_2                     22028
#define IDC_DATETIMEPICKER2_1           22029
#define IDC_GRP2_3                      22030
#define IDC_LBL5                        22033
#define IDC_COMBO1                      22034
#define IDC_COMBO2                      22035
#define IDC_LBL2_5                      22036
#define IDC_RADIO1                      22037
#define IDC_RADIO2                      22038
#define IDC_LIST1                       22039
#define IDC_CHECK1                      22040
#define IDC_GRP2_1                      22041
#define IDC_BUTTON2_1                   22042
#define IDC_BUTTON2_2                   22043
#define IDC_GRP4                        22044
#define IDD_DIALOGBAR4                  22045
#define ID_TBTN_SETTINGS                32781
#define ID_PRINT_OUT_PRL                33779
#define ID_TBTN_PRINT                   33780
#define ID_TBTN_VIEW                    33781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        22046
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         22046
#define _APS_NEXT_SYMED_VALUE           22046
#endif
#endif
