#if !defined(AFX_UMDATABASEDB_H)
#define AFX_UMDATABASEDB_H

#include "DBBaseClass_SQLApi.h"	// ... in DBTransaction_lib

class CUMPricelistDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
//private:
	BOOL pricelistExist(CTransaction_pricelist &);
public:
	CUMPricelistDB(void);
	CUMPricelistDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CUMPricelistDB(DB_CONNECTION_DATA &db_connection);

	BOOL getPricelists(vecTransactionPricelist &,int prl_type);
	BOOL addPricelist(CTransaction_pricelist &);
	BOOL updPricelist(CTransaction_pricelist &);
	BOOL delPricelist(CTransaction_pricelist &);

	BOOL updGeneralData(CTransaction_pricelist &);

	BOOL getSpecies(vecTransactionSpecies &);


	// Template database items; 070824 p�d
	BOOL getTemplates(vecTransactionTemplate &);

	BOOL isPricelistInStand(LPCTSTR prl_name,int type_of);

};


#endif