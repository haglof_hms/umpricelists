// PrlViewAndPrintSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMPricelists.h"
#include "PrlViewAndPrintSettingsDlg.h"

#include "ResLangFileReader.h"

// CPrlViewAndPrintSettingsDlg dialog

IMPLEMENT_DYNAMIC(CPrlViewAndPrintSettingsDlg, CDialog)

BEGIN_MESSAGE_MAP(CPrlViewAndPrintSettingsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPrlViewAndPrintSettingsDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CPrlViewAndPrintSettingsDlg::CPrlViewAndPrintSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrlViewAndPrintSettingsDlg::IDD, pParent)
{

}

CPrlViewAndPrintSettingsDlg::~CPrlViewAndPrintSettingsDlg()
{
}

void CPrlViewAndPrintSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LIST2, m_wndListCtrl);
	DDX_Control(pDX, IDC_CHECK1, m_wndCBox1);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP

}

BOOL CPrlViewAndPrintSettingsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CPrlViewAndPrintSettingsDlg::OnInitDialog()
{
	CString sMsg;
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setupListCtrl();
	setLanguage();

	return TRUE;
}

void CPrlViewAndPrintSettingsDlg::setupListCtrl(void)
{
	int nCnt = 0;
	CString sSpcID;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndListCtrl.InsertColumn(0, (xml->str(IDS_STRING113)), LVCFMT_CENTER, 40);
			m_wndListCtrl.InsertColumn(1, (xml->str(IDS_STRING114)), LVCFMT_LEFT, 130);
		}
		delete xml;
	}

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndListCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_header.SubclassWindow(hWndHeader);

	m_header.SetTheme(new CXTHeaderCtrlThemeOfficeXP());

	m_wndListCtrl.ModifyExtendedStyle(0,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);

	if (m_vecSpc.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpc.size();i++)
		{
			sSpcID.Format(_T("%d"),m_vecSpc[i].getSpcID());
			m_wndListCtrl.InsertItem(nCnt, _T(""), 0);
			m_wndListCtrl.SetItem(nCnt,1, LVIF_TEXT, m_vecSpc[i].getSpcName(), 0, NULL, NULL, NULL);
			m_wndListCtrl.SetItemData(nCnt,m_vecSpc[i].getSpcID());
			m_wndListCtrl.SetCheck(nCnt,isSpcSel(m_vecSpc[i].getSpcID()));
			nCnt++;
		}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	}	// 

	m_wndCBox1.SetCheck(m_bShowPriceByQual);

}

void CPrlViewAndPrintSettingsDlg::setLanguage(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING107)));
			m_wndCBox1.SetWindowText((xml->str(IDS_STRING1904)));

			m_wndBtnOK.SetWindowText((xml->str(IDS_STRING112)));
			m_wndBtnCancel.SetWindowText((xml->str(IDS_STRING135)));
		}
		delete xml;
	}

}

BOOL CPrlViewAndPrintSettingsDlg::isSpcSel(int id)
{
	if (m_vecSpcSelected.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpcSelected.size();i++)
			if (m_vecSpcSelected[i].getSpcID() == id) return TRUE;
	}
	else
		return TRUE;

	return FALSE;
}

// Return CTransaction_species from m_vecSpecies vector
// depending on specie id, selected; 070502 p�d
CTransaction_species* CPrlViewAndPrintSettingsDlg::getSpecie(int spc_id)
{
	if (m_vecSpc.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpc.size();i++)
		{
			if (m_vecSpc[i].getSpcID() == spc_id)
				return &m_vecSpc[i];
		}
	}
	return NULL;
}

void CPrlViewAndPrintSettingsDlg::OnBnClickedOk()
{
	// Get selected species and add to m_vecSpeciesSelected; 060328 p�d
	CString S;
	int nSpcID;
	CTransaction_species *pRecSpc;
	m_vecSpcSelected.clear();

	if (m_wndListCtrl.GetItemCount() > 0)
	{
		for (int i = 0;i < m_wndListCtrl.GetItemCount();i++)
		{
			nSpcID = m_wndListCtrl.GetItemData(i);
			if (m_wndListCtrl.GetCheck(i))
			{
				pRecSpc = getSpecie(nSpcID);
				if (pRecSpc != NULL)
				{
					m_vecSpcSelected.push_back(*pRecSpc);
				}
			}
		}
	}

	m_bShowPriceByQual = (m_wndCBox1.GetCheck() == 1);
	OnOK();
}
