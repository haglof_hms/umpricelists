// GeneralInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMPricelists.h"
#include "GeneralInfoDlg.h"

#include "PricelistsView.h"
#include "AvgAssortView.h"

#include "ResLangFileReader.h"

// CGeneralInfoDlg dialog

IMPLEMENT_DYNAMIC(CGeneralInfoDlg, CDialog)

BEGIN_MESSAGE_MAP(CGeneralInfoDlg, CDialog)
	ON_WM_SIZE()
  ON_WM_ERASEBKGND()
	ON_EN_CHANGE(IDC_EDIT1, &CGeneralInfoDlg::OnEnChangeEdit1)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnCBox1Changed)
END_MESSAGE_MAP()

// ------------------------------------------------
// The following code declares our AnchorMap
// and defines how each control should be handled
// ------------------------------------------------

BEGIN_ANCHOR_MAP(CGeneralInfoDlg)
    ANCHOR_MAP_ENTRY(IDC_LBL1,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL2,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL3,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL4,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_DATETIMEPICKER1,  ANF_TOP | ANF_LEFT)
END_ANCHOR_MAP()

CGeneralInfoDlg::CGeneralInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGeneralInfoDlg::IDD, pParent)
{
	m_bEnableWindows = TRUE;
	m_bIncludedInStandTmplate = FALSE;
	bDoSave=1;
}

CGeneralInfoDlg::~CGeneralInfoDlg()
{
}

void CGeneralInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GRP3, m_wndGrp3);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit3);

	DDX_Control(pDX, IDC_RADIO1, m_wndInM3FUB);
	DDX_Control(pDX, IDC_RADIO2, m_wndInM3TO);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);

	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_wndDateTimeCtrl);
	//}}AFX_DATA_MAP

}

BOOL CGeneralInfoDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CGeneralInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_wndEdit1.SetEnabledColor( BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit1.SetLimitText(45);
	m_wndEdit2.SetEnabledColor( BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit3.SetEnabledColor( BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(  BLACK, COL3DFACE );

	// --------------------------------------
	// At this point, we�ve set everything
	// up for our dialog except the 
	// anchoring/docking. We�ll do this now
	// with a call to InitAnchors()
	// --------------------------------------
	InitAnchors();

	// Add data to Combobox; 080114 p�d
	m_wndCBox1.ResetContent();
	m_wndCBox1.AddString(m_sStatusLblOK);
	m_wndCBox1.AddString(m_sStatusLblDevelop);


	return 0;
}

void CGeneralInfoDlg::OnSize(UINT nType,int cx,int cy)
{
  CDialog::OnSize(nType, cx, cy);
  
  CRect rcWnd;
  GetWindowRect(&rcWnd);
  
  HandleAnchors(&rcWnd);     // you can alternatively pass NULL for &rcWnd
  Invalidate(false);         // this is for ensuring the controls to be redrawn correctly 
}

BOOL CGeneralInfoDlg::OnEraseBkgnd(CDC* pDC) 
{

  // Here we call the EraseBackground-Handler from the
  // anchor-map which will reduce the flicker.  

  return(m_bpfxAnchorMap.EraseBackground(pDC->m_hDC));
}

LRESULT CGeneralInfoDlg::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

// MY METHODS

	// Get data from fields; 060405 p�d
CString CGeneralInfoDlg::getNameOfPricelist(void)			
{	
	if (m_wndEdit1.getText().IsEmpty())
		return _T("");	
	else
		return m_wndEdit1.getText();	
}

void CGeneralInfoDlg::setNameOfPricelist(LPCTSTR value)	
{	
	if (_tcslen(value) > 0)
		m_wndEdit1.SetWindowText(value);	
	else
		m_wndEdit1.SetWindowText(_T(""));	
}

void CGeneralInfoDlg::enableName(BOOL enable)	
{ 
	if (enable)
		m_wndEdit1.SetDisabledColor(  BLACK, INFOBK );
	else
		m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );

	m_wndEdit1.EnableWindow(!enable); 
	m_wndEdit1.SetReadOnly(enable); 

	m_bIncludedInStandTmplate = enable;

}

CString CGeneralInfoDlg::getNameOfCreator(void)		
{	
	if (m_wndEdit2.getText().IsEmpty())
		return _T("");	
	else
		return m_wndEdit2.getText();	
}

void CGeneralInfoDlg::setNameOfCreator(LPCTSTR value)	
{	
	if (_tcslen(value) > 0)
		m_wndEdit2.SetWindowText(value);	
	else
		m_wndEdit2.SetWindowText(_T(""));	
}

int CGeneralInfoDlg::getTimberPriceIn(void)
{
	if (m_wndInM3FUB.GetCheck() == BST_CHECKED)
		return 1;
	else if (m_wndInM3TO.GetCheck() == BST_CHECKED)
		return 2;
	else
		return -1;
}

void CGeneralInfoDlg::setTimberPriceIn(int idx)
{
	if (idx == 1)
	{
		m_wndInM3FUB.SetCheck(BST_CHECKED);
		m_wndInM3TO.SetCheck(BST_UNCHECKED);
	}
	else	if (idx == 2)
	{
		m_wndInM3TO.SetCheck(BST_CHECKED);
		m_wndInM3FUB.SetCheck(BST_UNCHECKED);
	}
}

void CGeneralInfoDlg::setStatusOfPricelist(int status,int doCboxChanged)
{
	if (status == ID_RUNE_OLLAS_TYPEOF_PRICELIST_1)
	{
		bDoSave=0;
		m_wndCBox1.SelectString(0,m_sStatusLblOK);
		//Kan inte anropa OnCBoxChanged j�mt eftersom d� h�nder det n�got som f�r HMS att kolla prislistekorrekthet vid fel tillf�llen. Bug#2583 20111122 J�
		if(doCboxChanged)
			OnCBox1Changed();
	}
	else if (status == ID_PRICELIST_IN_DEVELOPEMENT_1)
	{
		bDoSave=1;
		m_wndCBox1.SelectString(0,m_sStatusLblDevelop);
		//Kan inte anropa OnCBoxChanged j�mt eftersom d� h�nder det n�got som f�r HMS att kolla prislistekorrekthet vid fel tillf�llen. Bug#2583 20111122 J�
		if(doCboxChanged)
			OnCBox1Changed();
	}
}

//Lagt funktioner vars syfte �r att se om status �r �ndrad till Klar att anv�nda eller �r under utveckling. Bug#2583 20111122 J�
//Detta f�r att slippa k�ra sparfunktioner �ven om inget beh�ver sparas, dvs du kikar p� prislistor som �r satta till klar att anv�nda
BOOL CGeneralInfoDlg::getDoSave()
{
	if(bDoSave)
		return TRUE;
	else
		return FALSE;
}
void CGeneralInfoDlg::setDoSave(int nSave)
{
	bDoSave=nSave;
}


CString CGeneralInfoDlg::getCreateDate(void)				
{	
	CString sText;
	m_wndDateTimeCtrl.GetWindowText(sText);
	return sText;
}

CString CGeneralInfoDlg::getNotes(void)		
{		
	if (m_wndEdit3.getText().IsEmpty())
		return _T("");	
	else
		return m_wndEdit3.getText();	
}

void CGeneralInfoDlg::setNotes(LPCTSTR value)	
{	
	m_wndEdit3.SetWindowText(value);	
}

int CGeneralInfoDlg::getStatus(void)
{
	int nIndex = m_wndCBox1.GetCurSel();
	if (nIndex > -1)
	{
		return nIndex;
	}

	return -1;
}

void CGeneralInfoDlg::setStatus(void)
{
	if (m_wndCBox1.GetSafeHwnd()) 
	{
		m_wndCBox1.SelectString(0,m_sStatusLblDevelop);
		m_bIncludedInStandTmplate = FALSE;
	}
}

void CGeneralInfoDlg::setLanguage()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText((xml->str(IDS_STRING123)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING124)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING125)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING126)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING163)));
			m_wndLbl6.SetWindowText((xml->str(IDS_STRING166)));
			m_wndInM3FUB.SetWindowText((xml->str(IDS_STRING1660)));
			m_wndInM3TO.SetWindowText((xml->str(IDS_STRING1661)));

			m_sStatusLblOK = 	(xml->str(IDS_STRING1630));
			m_sStatusLblDevelop = 	(xml->str(IDS_STRING1631));

			m_sMsgCap = xml->str(IDS_STRING151);
			m_sMsgCharError.Format(_T("%s < > /"),xml->str(IDS_STRING2100));

			m_sPricelistErrMsg.Format(_T("%s\n%s\n%s\n\n"),
					(xml->str(IDS_STRING2200)),
					(xml->str(IDS_STRING2201)),
					(xml->str(IDS_STRING2202)));


		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
}

void CGeneralInfoDlg::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());
		m_wndEdit1.SetFocus();
	}
}



void CGeneralInfoDlg::OnCBox1Changed(void)
{
	int nSel = m_wndCBox1.GetCurSel();
	if(nSel>-1)
	{
		bDoSave=1;
	}
	if (!checkPricelist_matris())
	{
		::MessageBox(this->GetSafeHwnd(),m_sPricelistErrMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		m_wndCBox1.SetCurSel(1);
	}
	else
	{
		setLock(nSel == 0);
		setGlobalLock_matris(nSel == 0);

		CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
		if (pView && nSel == 0)
			pView->saveData(CPricelistsView::ON_SAVE);

	}


}

//*************************************************************************
// CGeneralInfoDlg_2 dialog

IMPLEMENT_DYNAMIC(CGeneralInfoDlg_2, CDialog)

BEGIN_MESSAGE_MAP(CGeneralInfoDlg_2, CDialog)
	ON_WM_SIZE()
  ON_WM_ERASEBKGND()
//	ON_EN_KILLFOCUS(IDC_EDIT2_1, &CGeneralInfoDlg_2::OnEnKillfocusEdit2_1)
	ON_EN_CHANGE(IDC_EDIT2_1, &CGeneralInfoDlg_2::OnEnChangeEdit1)
	ON_CBN_SELCHANGE(IDC_COMBO2, OnCBox2Changed)
END_MESSAGE_MAP()

// ------------------------------------------------
// The following code declares our AnchorMap
// and defines how each control should be handled
// ------------------------------------------------

BEGIN_ANCHOR_MAP(CGeneralInfoDlg_2)
    ANCHOR_MAP_ENTRY(IDC_LBL1,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL2,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL3,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL4,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_DATETIMEPICKER2_1,  ANF_TOP | ANF_LEFT)
END_ANCHOR_MAP()

CGeneralInfoDlg_2::CGeneralInfoDlg_2(CWnd* pParent /*=NULL*/)
	: CDialog(CGeneralInfoDlg_2::IDD, pParent)
{
	m_bEnableWindows = TRUE;
	m_bIncludedInStandTmplate = FALSE;
	bDoSave=1;
}

CGeneralInfoDlg_2::~CGeneralInfoDlg_2()
{
}

void CGeneralInfoDlg_2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GRP2_3, m_wndGrp3);

	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL2_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL2_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL2_5, m_wndLbl5);
	
	DDX_Control(pDX, IDC_EDIT2_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT2_4, m_wndEdit3);

	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);

	DDX_Control(pDX, IDC_DATETIMEPICKER2_1, m_wndDateTimeCtrl);
	//}}AFX_DATA_MAP

}

BOOL CGeneralInfoDlg_2::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CGeneralInfoDlg_2::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_wndEdit1.SetEnabledColor( BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit1.SetReadOnly(TRUE);
	m_wndEdit1.SetLimitText(45);
	m_wndEdit2.SetEnabledColor( BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit3.SetEnabledColor( BLACK, WHITE );
	m_wndEdit3.SetDisabledColor(  BLACK, COL3DFACE );

	// --------------------------------------
	// At this point, we�ve set everything
	// up for our dialog except the 
	// anchoring/docking. We�ll do this now
	// with a call to InitAnchors()
	// --------------------------------------
	InitAnchors();

	// Add data to Combobox; 080114 p�d
	m_wndCBox2.ResetContent();
	m_wndCBox2.AddString(m_sStatusLblOK);
	m_wndCBox2.AddString(m_sStatusLblDevelop);

	return 0;
}

void CGeneralInfoDlg_2::OnSize(UINT nType,int cx,int cy)
{
  CDialog::OnSize(nType, cx, cy);
  
  CRect rcWnd;
  GetWindowRect(&rcWnd);
  
  HandleAnchors(&rcWnd);     // you can alternatively pass NULL for &rcWnd
  Invalidate(false);         // this is for ensuring the controls to be redrawn correctly 
}

BOOL CGeneralInfoDlg_2::OnEraseBkgnd(CDC* pDC) 
{

  // Here we call the EraseBackground-Handler from the
  // anchor-map which will reduce the flicker.  

  return(m_bpfxAnchorMap.EraseBackground(pDC->m_hDC));
}

// MY METHODS

	// Get data from fields; 060405 p�d
CString CGeneralInfoDlg_2::getNameOfPricelist(void)			
{	
	return m_wndEdit1.getText();	
}

void CGeneralInfoDlg_2::setNameOfPricelist(LPCTSTR value)	
{	
	m_wndEdit1.SetWindowText(value);	
}

void CGeneralInfoDlg_2::enableName(BOOL enable)	
{ 
	if (enable)
		m_wndEdit1.SetDisabledColor(  BLACK, INFOBK );
	else
		m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );

	m_wndEdit1.EnableWindow(!enable); 
	m_wndEdit1.SetReadOnly(enable); 

	m_bIncludedInStandTmplate = enable;
}


CString CGeneralInfoDlg_2::getNameOfCreator(void)		
{		
	return m_wndEdit2.getText();	
}

void CGeneralInfoDlg_2::setNameOfCreator(LPCTSTR value)	
{	
	m_wndEdit2.SetWindowText(value);	
}

void CGeneralInfoDlg_2::setStatusOfPricelist(int status, int doCboxChanged)
{

	if (status == ID_RUNE_OLLAS_TYPEOF_PRICELIST_2)
	{
		bDoSave=0;
		m_wndCBox2.SelectString(0,m_sStatusLblOK);
		//Kan inte anropa OnCBoxChanged j�mt eftersom d� h�nder det n�got som f�r HMS att kolla prislistekorrekthet vid fel tillf�llen. Bug#2583 20111122 J�
		if(doCboxChanged)
			OnCBox2Changed();
	}
	else if (status == ID_PRICELIST_IN_DEVELOPEMENT_2)
	{
		bDoSave=1;
		m_wndCBox2.SelectString(0,m_sStatusLblDevelop);
		//Kan inte anropa OnCBoxChanged j�mt eftersom d� h�nder det n�got som f�r HMS att kolla prislistekorrekthet vid fel tillf�llen. Bug#2583 20111122 J�
		if(doCboxChanged)
			OnCBox2Changed();

	}
}

//Lagt funktioner vars syfte �r att se om status �r �ndrad till Klar att anv�nda eller �r under utveckling. Bug#2583 20111122 J�
//Detta f�r att slippa k�ra sparfunktioner �ven om inget beh�ver sparas, dvs du kikar p� prislistor som �r satta till klar att anv�nda
BOOL CGeneralInfoDlg_2::getDoSave()
{
	if(bDoSave)
		return TRUE;
	else
		return FALSE;
}
void CGeneralInfoDlg_2::setDoSave(int nSave)
{
	bDoSave=nSave;
}


CString CGeneralInfoDlg_2::getCreateDate(void)				
{	
	CString sText;
	m_wndDateTimeCtrl.GetWindowText(sText);
	return sText;
}

CString CGeneralInfoDlg_2::getNotes(void)		
{		
	return m_wndEdit3.getText();	
}

void CGeneralInfoDlg_2::setNotes(LPCTSTR value)	
{	
	m_wndEdit3.SetWindowText(value);	
}

int CGeneralInfoDlg_2::getStatus(void)
{
	int nIndex = m_wndCBox2.GetCurSel();
	if (nIndex > -1)
	{
		return nIndex;
	}

	return -1;
}

void CGeneralInfoDlg_2::setStatus(void)
{
	if (m_wndCBox2.GetSafeHwnd()) 
	{
		m_wndCBox2.SelectString(0,m_sStatusLblDevelop);
		m_bIncludedInStandTmplate = FALSE;
	}
}


void CGeneralInfoDlg_2::setLanguage()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText((xml->str(IDS_STRING123)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING124)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING125)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING126)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING163)));
			m_sMsgCap = (xml->str(IDS_STRING151));
			m_sMissingInfoMsg = (xml->str(IDS_STRING160));
			m_sMissingInfoMsg1 = (xml->str(IDS_STRING1600));
			m_sMissingInfoMsg2 = (xml->str(IDS_STRING1601));
			m_sMissingInfoMsg3 = (xml->str(IDS_STRING1602));

			m_sStatusLblOK = 	(xml->str(IDS_STRING1630));
			m_sStatusLblDevelop = 	(xml->str(IDS_STRING1631));

			m_sMsgCharError.Format(_T("%s < > /"),xml->str(IDS_STRING2100));

			m_sPricelistErrMsg.Format(_T("%s\n%s\n%s\n\n"),
					(xml->str(IDS_STRING2200)),
					(xml->str(IDS_STRING2201)),
					(xml->str(IDS_STRING2202)));

		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))
}

// Added 071031 p�d
// When m_wndEdit loses focus, check that the
// name of the Template doesn't already exist; 071031 p�d
void CGeneralInfoDlg_2::OnEnKillfocusEdit2_1()
{
	CString sMsg;
	CString sPricelistName = m_wndEdit1.getText();
	CAvgAssortView *pView = (CAvgAssortView *)getFormViewByID(IDD_FORMVIEW2);
	if (pView)
	{
		if (pView->checkName(sPricelistName))
		{
			sMsg.Format(_T("%s\n\n- %s\n\n%s\n\n%s"),
				m_sMissingInfoMsg1,
				sPricelistName,
				m_sMissingInfoMsg2,
				m_sMissingInfoMsg3);
			::MessageBox(this->GetSafeHwnd(),(sMsg),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			m_wndEdit1.SetWindowText(_T(""));
			m_wndEdit1.SetFocus();
			return;
		}
	}
}

void CGeneralInfoDlg_2::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());
		m_wndEdit1.SetFocus();
	}
}

void CGeneralInfoDlg_2::OnCBox2Changed(void)
{
	int nSel = m_wndCBox2.GetCurSel();
	if(nSel>-1)
	{
		bDoSave=1;
	}
	if (!checkPricelist_avg())
	{
		::MessageBox(this->GetSafeHwnd(),m_sPricelistErrMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		m_wndCBox2.SetCurSel(1);
	}
	else
	{
		setLock(nSel == 0);
		setGlobalLock_avg(nSel == 0);
		CAvgAssortView *pView = (CAvgAssortView *)getFormViewByID(IDD_FORMVIEW2);
		if (pView && nSel == 0)
			pView->saveData(CAvgAssortView::ON_SAVE);
	}
}
