// QualitedescDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMPricelists.h"
#include "QualitedescDlg.h"

#include "ResLangFileReader.h"
#include ".\qualitedescdlg.h"


// CQualitedescDlg dialog

IMPLEMENT_DYNAMIC(CQualitedescDlg, CDialog)
CQualitedescDlg::CQualitedescDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CQualitedescDlg::IDD, pParent)
{
}

CQualitedescDlg::~CQualitedescDlg()
{
}

void CQualitedescDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
//	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtnPlus);
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnMinus);
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CQualitedescDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
END_MESSAGE_MAP()


// CQualitedescDlg message handlers

BOOL CQualitedescDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);


	m_wndBtnPlus.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
	m_wndBtnPlus.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndBtnMinus.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
	m_wndBtnMinus.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	setupListCtrl();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

// PROTECTED
BOOL CQualitedescDlg::setupListCtrl(void)
{
	if (m_wndReportCtrl.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndReportCtrl.Create(this, ID_REPORT_CTRL ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	if (m_wndReportCtrl.GetSafeHwnd() != NULL)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				SetWindowText((xml->str(IDS_STRING117)));
				m_wndReportCtrl.ShowWindow( SW_NORMAL );
				CXTPReportColumn *pCol = m_wndReportCtrl.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING116)), 100));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( TRUE );

				m_wndReportCtrl.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReportCtrl.GetReportHeader()->AllowColumnReorder(FALSE);
				m_wndReportCtrl.GetReportHeader()->AllowColumnResize( FALSE );
				m_wndReportCtrl.GetReportHeader()->AllowColumnSort( FALSE );
				m_wndReportCtrl.GetReportHeader()->SetAutoColumnSizing( TRUE );
				m_wndReportCtrl.SetMultipleSelection( FALSE );
				m_wndReportCtrl.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReportCtrl.AllowEdit(TRUE);
				m_wndReportCtrl.FocusSubItems(TRUE);

			}	// if (xml->Load(sLangFN))
			delete xml;
		}	// if (fileExists(sLangFN))
		RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

		RECT rect;
		GetClientRect(&rect);
		// resize window = display window in tab; 060327 p�d
		setResize(GetDlgItem(IDC_STATIC),5,5,rect.right - 10,rect.bottom - 45);
		setResize(GetDlgItem(IDC_BUTTON1),40,14,25,25);
		setResize(GetDlgItem(IDC_BUTTON2),12,14,25,25);
		setResize(&m_wndReportCtrl,15,40,rect.right - 30,rect.bottom - 90);

		if (m_QualDescNames.GetCount() > 0)
		{
			for (int i = 0;i < m_QualDescNames.GetCount();i++)
			{
				m_wndReportCtrl.AddRecord(new CQualDescDataRec((m_QualDescNames[i])));
			}
		}
		m_wndReportCtrl.Populate();
		m_wndReportCtrl.UpdateWindow();

	}	// if (m_wndReportCtrl.GetSafeHwnd() != NULL)

	return TRUE;
}

// PUBLIC
void CQualitedescDlg::setQualDescFromArray(CStringArray &arr)
{
	if (arr.GetCount() > 0)
	{
		// Clear
		m_QualDescNames.RemoveAll();
		for (int i = 0;i < arr.GetCount();i++)
		{
			m_QualDescNames.Add(arr[i]);
		}
	}
}

void CQualitedescDlg::getQualDescFromArray(CStringArray &arr)
{
	if (m_QualDescNames.GetCount() > 0)
	{
		// Clear
		arr.RemoveAll();
		for (int i = 0;i < m_QualDescNames.GetCount();i++)
		{
			arr.Add(m_QualDescNames[i]);
		}
	}
}

void CQualitedescDlg::OnBnClickedOk()
{
	CXTPReportRecords *pRecs = m_wndReportCtrl.GetRecords();	

	if (pRecs)
	{
		m_QualDescNames.RemoveAll();
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CQualDescDataRec *pRec = (CQualDescDataRec *)pRecs->GetAt(i);
			m_QualDescNames.Add(pRec->getColumnText(0));
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs)

	OnOK();
}

// Add a row to report
void CQualitedescDlg::OnBnClickedButton2()
{
	m_wndReportCtrl.AddRecord(new CQualDescDataRec(_T("")));
	m_wndReportCtrl.Populate();
	m_wndReportCtrl.UpdateWindow();
}
// Remove last row from report
void CQualitedescDlg::OnBnClickedButton1()
{
	int nNumOfRecs;
	CXTPReportRow *pRow = m_wndReportCtrl.GetFocusedRow();
	if (pRow != NULL)
	{
		CXTPReportRecords *pRecs = m_wndReportCtrl.GetRecords();
		if (pRecs)
		{
			nNumOfRecs = pRecs->GetCount();
			// Can't remove first quality description; 060330 p�d
			if (nNumOfRecs > 1)
			{
				pRecs->RemoveAt(pRow->GetIndex());
				nIdxRemovedQualityDesc = pRow->GetIndex();
			}	// if (nNumOfRecs > 1)
		}	// if (pRecs)
		m_wndReportCtrl.Populate();
		m_wndReportCtrl.UpdateWindow();
	}	// if (pRow != NULL)
}
