#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CQualDescDataRec

class CQualDescDataRec : public CXTPReportRecord
{
	//private:
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:
	CQualDescDataRec(void)
	{
		AddItem(new CTextItem(_T("")));
	}

	CQualDescDataRec(LPCTSTR value)
	{
		AddItem(new CTextItem(value));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

};


// CQualitedescDlg dialog

class CQualitedescDlg : public CDialog
{
	DECLARE_DYNAMIC(CQualitedescDlg)
//private:
	CString m_sAbrevLangSet;
	// Setup language filename; 051214 p�d
  CString m_sLangFN;

	CStringArray m_QualDescNames;

	CXTButton m_wndBtnPlus;
	CXTButton m_wndBtnMinus;

	int nIdxRemovedQualityDesc;
protected:
	CMyReportCtrl m_wndReportCtrl;

	BOOL setupListCtrl(void);

public:

	BOOL isDirty(void)
	{	
		return m_wndReportCtrl.isDirty();
	}

	CQualitedescDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CQualitedescDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

	void setQualDescFromArray(CStringArray &array);
	void getQualDescFromArray(CStringArray &array);

	int getRemovedQualityDescIdx(void)		{return nIdxRemovedQualityDesc; }
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton1();
};
