#pragma once

#include "Resource.h"

// CPrlViewAndPrintSettingsDlg dialog

class CPrlViewAndPrintSettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CPrlViewAndPrintSettingsDlg)

	// Setup language filename; 051214 p�d
  CString m_sLangFN;

	CXTListCtrl m_wndListCtrl;
	CXTHeaderCtrl   m_header;

	CButton m_wndCBox1;
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	vecTransactionSpecies m_vecSpc;
	vecTransactionSpecies m_vecSpcSelected;

	BOOL m_bShowPriceByQual;

	void setupListCtrl(void);
	void setLanguage(void);

	CTransaction_species* getSpecie(int spc_id);

	BOOL isSpcSel(int id);

public:
	CPrlViewAndPrintSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPrlViewAndPrintSettingsDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

	void setSpcies(vecTransactionSpecies &vec)	{ m_vecSpc = vec; }
	void setSelSpcies(vecTransactionSpecies &vec)	{ m_vecSpcSelected = vec; }
	void setShowPriceByQual(BOOL v)	{ m_bShowPriceByQual = v; }

	vecTransactionSpecies getSpeciesSelected(void) { return m_vecSpcSelected; }
	BOOL getShowPriceByQual(void)	{ return m_bShowPriceByQual; }
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrlViewAndPrintSettingsDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
