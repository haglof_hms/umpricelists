#pragma once

#include "stdafx.h"

#include "DataEnterDlg.h"
#include "GeneralInfoDlg.h"
#include "ChangePrlPriceDlg.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

	CTransaction_pricelist m_recPricelist;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void doc_setTransaction_pricelist(CTransaction_pricelist rec);

	CTransaction_pricelist &doc_getTransaction_pricelist(void);

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIPricelistsFrame frame

class CMDIPricelistsFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIPricelistsFrame)

//private:
//	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgImportError;
	CString m_sMsgWindowsOpen;

	CXTPToolBar m_wndToolBar;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	BOOL m_bIsImportTBtn;
	BOOL m_bIsExportTBtn;
	BOOL m_bIsAddSpcTBtn;
	BOOL m_bIsRemoveTBtn;
	BOOL m_bIsPreviewTBtn;

	void setLanguage(void);

	void ToggleGeneralInfoPane(void);
	void ToggleDclsSettingsPane(void);

	void SetupPricelist(void);

	BOOL m_bAlreadySaidOkToClose;
	BOOL okToClose(void);
protected:

	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;


	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:
	CGeneralInfoDlg m_wndGeneralInfoDlg;
	CDataEnterDlg m_wndDataEnterDlg;
	CChangePrlPriceDlg m_wndChangePrlPriceDlg;

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}

	BOOL isGeneralInofDlgHidden(void);

	void setToolbarBtns(BOOL enable,BOOL locked)
	{
		m_bIsImportTBtn = TRUE;
		if (!locked)
		{
			m_bIsExportTBtn = enable;
			m_bIsAddSpcTBtn = enable;
			m_bIsRemoveTBtn = enable;
			m_bIsPreviewTBtn = enable;
		}
		else
		{
//			m_bIsImportTBtn = FALSE;
//			m_bIsExportTBtn = FALSE;
			m_bIsAddSpcTBtn = FALSE;
			m_bIsRemoveTBtn = FALSE;
//			m_bIsPreviewTBtn = FALSE;
		}
	}

	CMDIPricelistsFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIPricelistsFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;
// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint();
	afx_msg void OnClose();

	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSysCommand(UINT id,LONG param);
	afx_msg void OnChar(UINT nChar,UINT nRepCnt,UINT nFlags);

	// Toolbar button events; 060403 p�d
	afx_msg void OnTBtnOpenGeneralInfo();
	afx_msg void OnTBtnOpenDiamClassSettings();
	afx_msg void OnTBtnImportPricelist(void);
	afx_msg void OnTBtnExportPricelist(void);
	afx_msg void OnTBtnAddSpecie(void);
	afx_msg void OnTBtnRemoveSpecie(void);
	afx_msg void OnTBtnPreview(void);

	afx_msg void OnUpdateImportTBtn(CCmdUI*);
	afx_msg void OnUpdateExportTBtn(CCmdUI*);
	afx_msg void OnUpdateAddSpcTBtn(CCmdUI*);
	afx_msg void OnUpdateRemoveSpcTBtn(CCmdUI*);
	afx_msg void OnUpdatePreviewTBtn(CCmdUI*);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIPricelistsListFrame frame

class CMDIPricelistsListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIPricelistsListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDIPricelistsListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIPricelistsListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
