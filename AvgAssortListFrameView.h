#pragma once



// CAvgAssortListFrameView form view

class CAvgAssortListFrameView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CAvgAssortListFrameView)

		//private:
	BOOL m_bInitialized;

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	vecTransactionPricelist m_vecPricelist;
	vecTransactionPricelist m_vecSelectedPricelist;

	int m_nDBIndex;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CMyReportCtrl m_wndPricelistsList;
//	CXTPReportControl m_wndPricelistsList;

	// Methods
	BOOL setupPricelistsListReport(void);

	BOOL addPricelistDataToReport(void);
	
	void setupDoPopulate(void);
public:
	CAvgAssortListFrameView();           // protected constructor used by dynamic creation
	virtual ~CAvgAssortListFrameView();

	enum { IDD = IDD_FORMVIEW3 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnReportClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


