#pragma once

#include "Resource.h"
#include "bpctrlanchormap.h"

#include "MDIPricelistsFrame.h"

// CDataEnterDlg form view

class CDataEnterDlg : public CDialog
{
	DECLARE_DYNCREATE(CDataEnterDlg)

//private:
	void setLanguage(void);

protected:
	CXTResizeGroupBox m_wndGrp1;
	CXTResizeGroupBox m_wndGrp2;
	CXTResizeGroupBox m_wndGrp3;
	CXTResizeGroupBox m_wndGrp4;
	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;

	CStatic m_wndLbl1;
	CStatic m_wndLbl2;
	CStatic m_wndLbl3;

	CXTButton m_wndBtn1;
	CXTButton m_wndBtn2;
	CXTButton m_wndBtn3;
	CXTButton m_wndBtn4;
	CXTButton m_wndBtn5;
	CXTButton m_wndBtn6;
	CXTButton m_wndBtn7;

	BOOL m_bEnableWindows;
public:
	CDataEnterDlg();           // protected constructor used by dynamic creation
	virtual ~CDataEnterDlg();

	enum { IDD = IDD_DIALOGBAR };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// Prevent Enter and Cancel on this Dialog; 060403 p�d
	virtual void OnOK()	{}
	virtual void OnCancel() {}

  // -------------------------------------
  // the following code declares the 
  // required elements for our AnchorMap
  // -------------------------------------
  DECLARE_ANCHOR_MAP();

	void setDiamClass(int dcls);
	void setStartDiam(int start);
	void setNumOfDCLS(int num_of);

	BOOL isFocus(CWnd *pWnd)
	{
		return (pWnd->GetSafeHwnd() == m_wndEdit1.GetSafeHwnd() || 
						pWnd->GetSafeHwnd() == m_wndEdit2.GetSafeHwnd());
						//pWnd->GetSafeHwnd() == m_wndEdit3.GetSafeHwnd());
	}

	void setEnableWindows(BOOL enabled)
	{
		m_bEnableWindows = enabled;

		m_wndEdit1.EnableWindow( m_bEnableWindows );
		m_wndEdit1.SetReadOnly(!m_bEnableWindows);
		m_wndEdit2.EnableWindow( m_bEnableWindows );
		m_wndEdit2.SetReadOnly(!m_bEnableWindows);
//		m_wndEdit3.EnableWindow( m_bEnableWindows );
//		m_wndEdit3.SetReadOnly(!m_bEnableWindows);

		m_wndBtn1.EnableWindow( m_bEnableWindows );
		m_wndBtn2.EnableWindow( m_bEnableWindows );
		m_wndBtn3.EnableWindow( m_bEnableWindows );
		m_wndBtn4.EnableWindow( m_bEnableWindows );
		m_wndBtn5.EnableWindow( m_bEnableWindows );
		m_wndBtn6.EnableWindow( m_bEnableWindows );
		m_wndBtn7.EnableWindow( m_bEnableWindows );

	}

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnSize(UINT nType,int cx,int cy);
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
};


