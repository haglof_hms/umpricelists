#if !defined(__AVGASSORTVIEW_H__)
#define __AVGASSORTVIEW_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MDIAvgAssortFrame.h"

#include "AvgAssortFormView.h"
#include "AvgAssortListFrameView.h"
#include "AvgAssortAddDelDlg.h"

#include "UMPricelistDB.h"

// CAvgAssortView view
class CMDIAvgAssortDoc;

class CAvgAssortView : public CView
{
	DECLARE_DYNCREATE(CAvgAssortView)

//private:
	BOOL m_bInitialized;
	BOOL m_bIsEnabledData;

	BOOL m_bIspricelistLocked;

	CFont m_fntTab;

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	vecTransactionPricelist m_vecPricelist;
	vecTransactionSpecies m_vecSpecies;
	vecTransactionAssort m_vecAssort;

	CString m_sMsgCap;
	CString m_sMissingInfoMsg;
	CString m_sMissingInfoMsg2;
	CString m_sMissingInfoMsg3;
	CString m_sMissingInfoMsg4;
	CString m_sAtLeastOneSpecie;
	CString m_sMissingInfoMsg1;
	CString m_sPricelistExistsMsg;
	CString m_sNoPricelistExistsMsg;
	CString m_sPricelistErrMsg;

	CStringArray m_arrQualDescNames;

	CTransaction_pricelist m_recSelectedPricelist;

	vecTransactionSpecies m_vecSpeciesInDB;

	enumPricelistState m_enumPricelistState;

	UINT m_nDBNavigationCounter;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	int m_nPricelistStatus;

	AvgAssortAddDelDlg m_AvgAssortAddDelDlg;

protected:
	CAvgAssortView();           // protected constructor used by dynamic creation
	virtual ~CAvgAssortView();

	BOOL setupNewPricelist(void);
	void setupOpenPricelist(void);

	void addRowToSelectedReportPage(void);
	void delRowFromSelectedReportPage(void);

	BOOL checkAssortments(void);
	
	void setToDoOnNewBtn(void);
	BOOL setToDoOnSaveBtn(void);
	void setToDoOnDelBtn(void);

	void actionDBNavigationMessage(WPARAM id);
	void setupDBNaivgationToolBar(BOOL start_prev,BOOL end_next);

	void openPricelistListFormView(void);

	//BOOL isPricelistNameOK(LPCTSTR name);

public:

	BOOL saveXMLFileFromPricelistInfoToDB(bool only_check = false);

	typedef enum {ON_SAVE,ON_PREV_NEXT,ON_QUIT} enumSaveMsg;

	void doSetNavigationBar();

	void setEnableWindows(BOOL enabled,BOOL w1_also,BOOL lock);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate( );
	protected:
	//}}AFX_VIRTUAL
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
	CMDIAvgAssortDoc* GetDocument();

	CMyTabControl m_wndTabControl;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int spc_id, int nIcon);
	void UpdateDocTitle();

	BOOL setupNewPricelist(CTransaction_diameterclass &rec);
	BOOL setGeneralInfoDlgValues(BOOL lock);

	BOOL setupPricelistOnStartup(void);

	BOOL getPricelistXML(CString &name,CString &data);

	BOOL savePricelistToXMLFile(LPCTSTR fn,LPCTSTR data);
	BOOL saveFromXMLFileToPricelistDB(LPCTSTR xml_fn);

	BOOL addSpecieToPricelist(void);
	BOOL removeSpecieFromPricelist(void);

	BOOL removePricelistFromDB(void);

	BOOL isAvgPriceListInStandTemplate();

	void doPouplate(UINT idx);
	int getDBNavigationCounter(void);
	int getNumOfItemsInPricelist(void);

	BOOL isOKToClose(void);

	BOOL saveData(enumSaveMsg show_msg_type);

	BOOL getXMLHeaderData(LPTSTR name,LPTSTR done_by,LPTSTR date,LPTSTR notes,int *status);

	// Check if entered name havn't already been used; 071217 p�d
	BOOL checkName(LPCTSTR name);

	// Check if entered name havn't already been used; 090210 p�d
	short isPricelistNameOK(enumSaveMsg show_msg_type);

	// Add an empty row to assortments
	void addRowToAssortmentPerSpecies();
	void delRowFromAssortmentPerSpecies();

protected:
	//{{AFX_MSG(CTabbedViewView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
};


#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDIAvgAssortDoc* CAvgAssortView::GetDocument()
	{ return (CMDIAvgAssortDoc*)m_pDocument; }
#endif


#endif