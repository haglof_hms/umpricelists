// ChangePrlPriceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PricelistsView.h"
#include "UMPricelists.h"
#include "ChangePrlPriceDlg.h"

// CChangePrlPriceDlg dialog

IMPLEMENT_DYNAMIC(CChangePrlPriceDlg, CDialog)

BEGIN_MESSAGE_MAP(CChangePrlPriceDlg, CDialog)
	ON_BN_CLICKED(IDC_RADIO1, &CChangePrlPriceDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CChangePrlPriceDlg::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_BUTTON2_1, &CChangePrlPriceDlg::OnBnClickedButton21)
	ON_BN_CLICKED(IDC_BUTTON2_2, &CChangePrlPriceDlg::OnBnClickedButton22)
END_MESSAGE_MAP()

CChangePrlPriceDlg::CChangePrlPriceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChangePrlPriceDlg::IDD, pParent)
{
	m_bEnableWindows = TRUE;
}

CChangePrlPriceDlg::~CChangePrlPriceDlg()
{
	xml.clean();
}

void CChangePrlPriceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GRP2_1, m_wndGrp1);

	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2);

	DDX_Control(pDX, IDC_EDIT2_1, m_wndEdit1);

	DDX_Control(pDX, IDC_RADIO1, m_wndRadio1);
	DDX_Control(pDX, IDC_RADIO2, m_wndRadio2);
		
	DDX_Control(pDX, IDC_BUTTON2_1, m_wndBtn1);
	DDX_Control(pDX, IDC_BUTTON2_2, m_wndBtn2);
	//}}AFX_DATA_MAP

}

BOOL CChangePrlPriceDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CChangePrlPriceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_wndEdit1.SetEnabledColor( BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit1.SetAsNumeric();

	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));

	if (fileExists(sLangFN))
	{
		if (xml.Load(sLangFN))
		{
			m_wndRadio1.SetWindowText(xml.str(IDS_STRING1071));
			m_wndRadio2.SetWindowText(xml.str(IDS_STRING1072));
			
			m_wndLbl1.SetWindowText(xml.str(IDS_STRING1073));

			//m_wndBtn1.SetWindowText(xml.str(IDS_STRING1074));
			//m_wndBtn2.SetWindowText(xml.str(IDS_STRING1075));
		}
	}

	// Set to Percent as default; 090414 p�d
	m_wndRadio1.SetCheck(TRUE);
	m_wndLbl2.SetWindowTextW(xml.str(IDS_STRING1076));

	m_wndBtn1.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
	m_wndBtn1.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndBtn2.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
	m_wndBtn2.SetXButtonStyle( BS_XT_WINXP_COMPAT );


	return 0;
}


// CChangePrlPriceDlg message handlers

void CChangePrlPriceDlg::OnBnClickedRadio1()
{
	m_wndLbl2.SetWindowTextW(xml.str(IDS_STRING1076));
}

void CChangePrlPriceDlg::OnBnClickedRadio2()
{
	m_wndLbl2.SetWindowTextW(xml.str(IDS_STRING1077));
}

// Recalculate price, add in percent or kronor; 090414 p�d
void CChangePrlPriceDlg::OnBnClickedButton21()
{
	if (m_wndEdit1.getInt() <= 0) 
	{
		::MessageBox(this->GetSafeHwnd(),xml.str(IDS_STRING1078),xml.str(IDS_STRING151),MB_ICONEXCLAMATION | MB_OK);
		return;
	}
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->calcPriceToPricelist(TRUE,(m_wndRadio1.GetCheck() ? TRUE : FALSE),m_wndEdit1.getInt());	// True = Add, TRUE = In percent, False = In kronor
		pView = NULL;
	}	// if (pView != NULL)
}

// Recalculate price, draw in percent or kronor; 090414 p�d
void CChangePrlPriceDlg::OnBnClickedButton22()
{
	if (m_wndEdit1.getInt() <= 0) 
	{
		::MessageBox(this->GetSafeHwnd(),xml.str(IDS_STRING1078),xml.str(IDS_STRING151),MB_ICONEXCLAMATION | MB_OK);
		return;
	}
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->calcPriceToPricelist(FALSE,(m_wndRadio1.GetCheck() ? TRUE : FALSE),m_wndEdit1.getInt());	// True = Add, TRUE = In percent, False = In kronor
		pView = NULL;
	}	// if (pView != NULL)
}
