// MDITabbedView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIPricelistsFrame.h"
#include "PricelistsView.h"
#include "SelectSpeciesDlg.h"
#include "QualitedescDlg.h"

#include "ResLangFileReader.h"

#include "Resource.h"


//////////////////////////////////////////////////////////////////////////////////
// CPricelistsView

IMPLEMENT_DYNCREATE(CPricelistsView, CView)

BEGIN_MESSAGE_MAP(CPricelistsView, CView)
	//{{AFX_MSG_MAP(CPricelistsView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	//}}AFX_MSG_MAP
	ON_NOTIFY(TCN_SELCHANGE, IDC_TABCONTROL_1, OnSelectedChanged)

	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
END_MESSAGE_MAP()

CPricelistsView::CPricelistsView()
{
	m_bInitialized = FALSE;
	m_bQDescChanged = FALSE;
	m_bConnected = FALSE;
	m_bIsEnabledData = FALSE;
	
	m_bIspricelistLocked = FALSE;

	m_enumPricelistState = PRICELIST_OPEN;

}

CPricelistsView::~CPricelistsView()
{
	m_fntTab.DeleteObject();
}

int CPricelistsView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	LOGFONT lf;
	VERIFY(m_fntTab.CreateFont(
   24,                        // nHeight
   0,                         // nWidth
   0,                         // nEscapement
   0,                         // nOrientation
   FW_NORMAL,                 // nWeight
   FALSE,                     // bItalic
   FALSE,                     // bUnderline
   0,                         // cStrikeOut
   ANSI_CHARSET,              // nCharSet
   OUT_DEFAULT_PRECIS,        // nOutPrecision
   CLIP_DEFAULT_PRECIS,       // nClipPrecision
   DEFAULT_QUALITY,           // nQuality
   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
   _T("Times New Roman")));   // lpszFacename

	m_fntTab.GetLogFont( &lf );
	// Tab control
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0, 0, 0, 0), this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	return 0;
}

void CPricelistsView::OnInitialUpdate( )
{
	CView::OnInitialUpdate();

	// Setup language filename; 051214 p�d
	m_sAbrevLangSet = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	if (!m_bInitialized)
	{

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sMsgCap =	(xml.str(IDS_STRING151));
				m_sStatusLblOK = 	(xml.str(IDS_STRING1630));
				m_sStatusLblDevelop = 	(xml.str(IDS_STRING1631));
				m_sMissingInfoMsg.Format(_T("%s\n%s\n%s\n\n%s\n"),
					(xml.str(IDS_STRING159)),
					(xml.str(IDS_STRING1593)),
					(xml.str(IDS_STRING1594)),
					(xml.str(IDS_STRING1595)));
				m_sMissingInfoMsg2.Format(_T("%s\n%s\n%s\n\n%s\n"),
					(xml.str(IDS_STRING159)),
					(xml.str(IDS_STRING1593)),
					(xml.str(IDS_STRING1594)),
					(xml.str(IDS_STRING1596)));

				m_sMissingInfoMsg3.Format(_T("%s\n%s\n%s\n\n%s\n"),
					(xml.str(IDS_STRING1597)),
					(xml.str(IDS_STRING1598)),
					(xml.str(IDS_STRING1599)),
					(xml.str(IDS_STRING1596)));

				m_sMissingInfoMsg4.Format(_T("%s\n%s\n%s\n"),
					(xml.str(IDS_STRING1597)),
					(xml.str(IDS_STRING1598)),
					(xml.str(IDS_STRING1599)));

				m_sMissingInfoMsg1.Format(_T("%s\n%s\n\n%s\n\n"), 
																(xml.str(IDS_STRING1590)),
																(xml.str(IDS_STRING1591)),
																(xml.str(IDS_STRING1592)));
				m_sPricelistExistsMsg.Format(_T("%s\n%s\n"),
						(xml.str(IDS_STRING1800)),
						(xml.str(IDS_STRING1801)));
				m_sNoPricelistExistsMsg.Format(_T("%s\n%s\n%s\n\n"),
						(xml.str(IDS_STRING2000)),
						(xml.str(IDS_STRING2001)),
						(xml.str(IDS_STRING2002)));
				m_sPricelistErrMsg.Format(_T("%s\n%s\n%s\n\n"),
						(xml.str(IDS_STRING2200)),
						(xml.str(IDS_STRING2201)),
						(xml.str(IDS_STRING2202)));
			}	// if (xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))

		getPricelists(m_dbConnectionData,m_vecPricelist,ID_RUNE_OLLAS_TYPEOF_PRICELIST_1);
		m_nDBNavigationCounter = (int)m_vecPricelist.size() - 1;
		setupPricelistOnStartup();
		m_bInitialized = TRUE;
		m_enumPricelistState = PRICELIST_OPEN;
		
		m_bInitialized = TRUE;
	}
}

BOOL CPricelistsView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CView::OnCopyData(pWnd, pData);
}


void CPricelistsView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	if (m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

BOOL CPricelistsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if (!CView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;

}

void CPricelistsView::doSetNavigationBar()
{
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

	setupDBNaivgationToolBar(m_nDBNavigationCounter > 0, m_nDBNavigationCounter < (m_vecPricelist.size()-1) && m_vecPricelist.size() > 0);
}

// CPricelistsView drawing

void CPricelistsView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
}

void CPricelistsView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;

	UpdateDocTitle();

	CFrameWnd* pFrame = GetParentFrame();
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		CView* pView = DYNAMIC_DOWNCAST(CView, CWnd::FromHandle(m_wndTabControl.GetSelectedItem()->GetHandle()));
		ASSERT_KINDOF(CView, pView);

		pFrame->SetActiveView(pView);
	}

}

// CPricelistsView diagnostics

#ifdef _DEBUG
void CPricelistsView::AssertValid() const
{
	CView::AssertValid();
}

void CPricelistsView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMDIFrameDoc* CPricelistsView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDIFrameDoc)));
	return (CMDIFrameDoc*)m_pDocument;
}

#endif //_DEBUG

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPricelistsView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		case ID_MSG_SELECT_PRICELIST :	// SendMessageToDecendants()
		{
			m_recSelectedPricelist = *(CTransaction_pricelist *)lParam;
			setupOpenPricelist();
			break;
		}	// case ID_MSG_FROM :

		case ID_WPARAM_VALUE_FROM + 0xFA:
		{
			setDataEnterDlgValues();
			setChangePrlPriceDlgValues();
// Commented out 2008-10-10 p�d
// No need to set general info here; 081010 p�d
//			setGeneralInfoDlgValues();
			break;
		}

		case ID_WPARAM_VALUE_FROM + 0xFB:
		{
			openPopupMenuForQualityTab();
			break;
		}

		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			setToDoOnSaveBtn();	// Save before adding new pricelist; 090505 p�d
			setToDoOnNewBtn();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			setToDoOnSaveBtn();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			setToDoOnDelBtn();
			break;
		}	// case ID_NEW_ITEM :
		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		case ID_DBNAVIG_PREV :
		case ID_DBNAVIG_NEXT :
		case ID_DBNAVIG_END :
		{
			// Only goto next item if THIS item's OK; 071217 p�d
			if (saveData(CPricelistsView::ON_PREV_NEXT))
			{
				actionDBNavigationMessage(wParam);
			}
			break;
		}	// case ID_NEW_ITEM :
		case ID_DBNAVIG_LIST :
		{
			actionDBNavigationMessage(wParam);
			break;
		}	// case ID_NEW_ITEM :
	};

	return 0L;
}

// PUBLIC Method; ask if it's ok to close window; 070206 p�d
BOOL CPricelistsView::isOKToClose(void)
{
	BOOL bOKToClose = FALSE,bNoFubPrice=FALSE,bZeroDiaPrice=FALSE,bAssSameName=FALSE;;
	CString sMsg;
	CString sCapMsg;
CString csSpecie2=_T(""),csAssName2=_T("");
	CString csAssName=_T("");
	CString csSpecie=_T("");
	int nNumOfPages = m_wndTabControl.getNumOfTabPages();
	m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT_2;
	CGeneralInfoDlg *pDlg = NULL;

	// Get status of pricelist, set in ComboBox on GeneralInfoDialog; 080114 p�d
	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		// Set default value at first; 081216 p�d
		m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT_1;

		CXTPDockingPaneManager *pPaneManager = pMDIChild->GetDockingPaneManager();
		if (pPaneManager != NULL)
		{
			CXTPDockingPane *pPane = pPaneManager->FindPane(IDC_GENERAL_INFO_PANE_1);
			if (pPane != NULL)
			{
				pDlg = (CGeneralInfoDlg *)pPane->GetChild();
				if (pDlg != NULL)
				{
					if (pDlg->getStatus() == 1)	// Pricelist still in development
					{
						m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT_1;
					}	
					else if (pDlg->getStatus() == 0)	// Pricelist set as "Ready to use"
					{
						m_nPricelistStatus = ID_RUNE_OLLAS_TYPEOF_PRICELIST_1;
					}
				}	// if (pDlg != NULL)
			}	// if (pPane != NULL)
		}	// if (pPaneManager != NULL)
		// Display data read from database; 060410 p�d
	}	// if (pPrlParser->LoadFromBuffer(m_recSelectedPricelist.getPricelistFile()))

	// if m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT we don't need to check
	// if pricelist is ok or not (STILL IN DEVELOPMENT); 080114 p�d
	if (m_nPricelistStatus == ID_PRICELIST_IN_DEVELOPEMENT_1)
	{
		return TRUE;
	}

	if (nNumOfPages > 0)
	{
		// Only look at active page; 071206 p�d
		// Look at all pages; 080703 p�d
		for (int i = 0;i < nNumOfPages;i++)
		{
			CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getTabPage(i);
			if (pSelectedPage)
			{
				// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
				CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
				if (pView)
				{
					bOKToClose = pView->isQDescDataValid() && checkAssortments();
					if (!bOKToClose)
					{
						// Force status if pricelist isn't OK; 080114 p�d
						m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT_1;
						if (pDlg != NULL)
							pDlg->setStatusOfPricelist(m_nPricelistStatus,1);
						
						if (fileExists(m_sLangFN))
						{
							RLFReader *xml = new RLFReader;
							if (xml->Load(m_sLangFN))
							{
								sCapMsg.Format(_T("%s - %s"),
									(xml->str(IDS_STRING151)),
									pSelectedPage->GetCaption());
								sMsg.Format(_T("\n%s\n\n%s\n%s\n\n%s\n%s\n\n%s"),
									(xml->str(IDS_STRING1540)),
									(xml->str(IDS_STRING1541)),
									(xml->str(IDS_STRING1542)),
									(xml->str(IDS_STRING1543)),
									(xml->str(IDS_STRING1544)),
									(xml->str(IDS_STRING1545)));
								if (MessageBox(sMsg,sCapMsg,MB_YESNO | MB_DEFBUTTON2 | MB_ICONINFORMATION) == IDYES)
								{
									bOKToClose = TRUE;
								}
								else
									bOKToClose = FALSE;
							}	// if (xml->Load(m_sLangFN))
							delete xml;
						}	// if (xml->Load(m_sLangFN))
					}	// if (!bOKToClose)			
					//Kolla om det finns massa sortiment utan fub pris, 20111122 Bug #2583 J�
					if(pView->getPulpFubPriceIsSet()==FALSE)
						bNoFubPrice=TRUE;

					//Kolla om det finns flera sortiment med mindia samt priser =0 Bug #2717 J�
					if(pView->getNumZeroDiaPrice(csAssName2)==TRUE)
					{
						bZeroDiaPrice=TRUE;
						csSpecie2=pSelectedPage->GetCaption();
					}
					//Kolla om det finns sortiment med samma namn, 20111227 Bug #2719 J�
					if(pView->getAssSameName(csAssName)==TRUE)
					{
						bAssSameName=TRUE;
						//pView->GetWindowText(csSpecie);
						csSpecie=pSelectedPage->GetCaption();
					}

				}	// if (pView)
			}	// if (pSelectedPage)
		}	// for (int i = 0;i < nNumOfPages;i++)
		//Saknas fub pris p� n�got massa sortiment 20111122 Bug #2583 J�
		if(bNoFubPrice)
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				sCapMsg.Format(_T("%s"),
					(xml->str(IDS_STRING2310)));
				sMsg.Format(_T("\n%s\n%s\n%s\n%s"),
					(xml->str(IDS_STRING2311)),
					(xml->str(IDS_STRING2312)),
					(xml->str(IDS_STRING2313)),
					(xml->str(IDS_STRING2314)));
				if (MessageBox(sMsg,sCapMsg,MB_YESNO | MB_DEFBUTTON2 | MB_ICONINFORMATION) == IDYES)
				{
					bOKToClose = TRUE;
					pDlg->setDoSave(0);
				}
				else
				{
					// Force status if pricelist isn't OK; 080114 p�d
					pDlg->setDoSave(1);
					m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT_1;
					if (pDlg != NULL)
						pDlg->setStatusOfPricelist(m_nPricelistStatus,1);
					bOKToClose = FALSE;
				}
			}
			delete xml;
		}
		if(bZeroDiaPrice)
		{		
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				sCapMsg.Format(_T("%s"),
					(xml->str(IDS_STRING127)));
				sMsg.Format(_T("%s\n%s\n\n(%s,%s)"),
					(xml->str(IDS_STRING2325)),
					(xml->str(IDS_STRING2326)),csSpecie2,csAssName2);
				MessageBox(sMsg,sCapMsg,MB_OK | MB_ICONEXCLAMATION);
				pDlg->setDoSave(1);
				m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT_1;
				if (pDlg != NULL)
					pDlg->setStatusOfPricelist(m_nPricelistStatus,1);
				bOKToClose = FALSE;
			}
			delete xml;
		}
		//20111227 Bug #2719 J�
		if(bAssSameName)
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				sCapMsg.Format(_T("%s"),
					(xml->str(IDS_STRING127)));
				sMsg.Format(_T("%s\n%s\n%s\n\n(%s,%s)"),
					(xml->str(IDS_STRING2322)),
					(xml->str(IDS_STRING2323)),
					(xml->str(IDS_STRING2324)),csSpecie,csAssName);
				MessageBox(sMsg,sCapMsg,MB_OK | MB_ICONEXCLAMATION);
				pDlg->setDoSave(1);
				m_nPricelistStatus = ID_PRICELIST_IN_DEVELOPEMENT_1;
				if (pDlg != NULL)
					pDlg->setStatusOfPricelist(m_nPricelistStatus,1);
				bOKToClose = FALSE;
			}
			delete xml;
		}


	}	// if (nNumOfPages > 0)

	
	if (pDlg != NULL)
		pDlg = NULL;


	return (bOKToClose);
}

BOOL CPricelistsView::isPriceListInStandTemplate()
{
	vecTransactionTemplate m_vecTransactionTemplate;
	CTransaction_template recTmpl;
	int nPrlID=0,nPrlTypeof=0,nRet=FALSE;
	TCHAR szFuncName[50];

	if( m_enumPricelistState == PRICELIST_NEW ) return FALSE; // New pricelist
	
	//??Vilken prislista skall man egentligen kolla mot? T�nkte om det �r en ny prislista och inte en befintlig
	//Nu tas den man st�r i i navigationslistan, kanske inte korrekt.

	if( m_vecPricelist.empty() == true ) return FALSE;

	CTransaction_pricelist recPrl = m_vecPricelist[m_nDBNavigationCounter];
	if (m_bConnected)
	{
		CUMPricelistDB *pDB = new CUMPricelistDB(m_dbConnectionData);
		if (pDB != NULL)
			if (pDB->getTemplates(m_vecTransactionTemplate))

				// Is there any data to check by; 081215 p�d
				if (m_vecTransactionTemplate.size() > 0)
				{
					TemplateParser pars;
					for (UINT i = 0;i < m_vecTransactionTemplate.size();i++)
					{
						recTmpl = m_vecTransactionTemplate[i];

						if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
						{
							pars.getTemplatePricelist(&nPrlID,szFuncName);
							pars.getTemplatePricelistTypeOf(&nPrlTypeof);
							if (nPrlID == recPrl.getID() && nPrlTypeof == recPrl.getTypeOf())
							{
								nRet=TRUE;
								break;
							}	
						}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))

					}	// for (UINT i = 0;i < vecTmpl.size();i++)
				}	// if (vecTmpl.size() > 0)
				if (pDB) delete pDB;
	}
	return nRet;
}

BOOL CPricelistsView::saveData(enumSaveMsg show_msg_type)
{
	short nRet = -1;
	CString sName;
	int nNumOfPages = m_wndTabControl.getNumOfTabPages();
	// There's no data, i.e. no data in DB; 090127 p�d
	if (!m_bIsEnabledData) return TRUE;



	if (m_bConnected)	
	{

		CMDIPricelistsFrame *pWnd = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
		if (pWnd)
		{
			if(pWnd->m_wndGeneralInfoDlg.getDoSave()==FALSE)
				return TRUE;
			sName = pWnd->m_wndGeneralInfoDlg.getNameOfPricelist();
			// Added 080520 p�d
			nRet = isPricelistNameOK(sName,show_msg_type);
			//�ndrat 20110824 J� bug #2271, returnerar false om man skall avsluta utan att spara
			/*
			if (show_msg_type == CPricelistsView::ON_SAVE && nRet < 2)	// 
				return FALSE;
			else if (show_msg_type == CPricelistsView::ON_QUIT && nRet < 2)	// 
				return (nRet == 1);
			else if (show_msg_type == CPricelistsView::ON_PREV_NEXT && nRet < 2)	// 
				return TRUE;*/
			switch(nRet)
			{
			case 0:
				return FALSE;
			case 1:
				return TRUE;
			}
		}

		// Check if data is valid also; 071206 p�d
		// Message to user id data isn't valid; 071206 p�d
		// I.e. user answers NO do not move on; 071219 p�d
		if (!isOKToClose())	return FALSE;
				
		if (!saveXMLFileFromPricelistInfoToDB())
		{
			::MessageBox(this->GetSafeHwnd(),m_sPricelistErrMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		// Reload the pricelist(s) from database, to make sure the
		// data we compare with, is the latest data. E.g. the user
		// might have pressed the Save button on the HMSShell toolbar; 060424 p�d
		getPricelists(m_dbConnectionData,m_vecPricelist,ID_RUNE_OLLAS_TYPEOF_PRICELIST_1);

		if (m_enumPricelistState == PRICELIST_NEW)
		{
			// Set DB pointer to last entry; 080703 p�d
			m_nDBNavigationCounter = (int)m_vecPricelist.size() - 1;
			// Display last entry; 080703 p�d
			setupPricelistOnStartup();
		}

		m_enumPricelistState = PRICELIST_OPEN;
		m_bQDescChanged = FALSE;
	}

	doSetNavigationBar();

	return TRUE;
}

BOOL CPricelistsView::getXMLHeaderData(LPTSTR name,LPTSTR done_by,LPTSTR date,LPTSTR notes,int *price_in,int *status)
{
	BOOL bReturn = FALSE;
	PricelistParser *pPrlParser = new PricelistParser();
	if (pPrlParser)
	{
		if (pPrlParser->LoadFromBuffer(m_recSelectedPricelist.getPricelistFile()))
		{
			pPrlParser->getHeaderName(name);
			pPrlParser->getHeaderDoneBy(done_by);
			pPrlParser->getHeaderDate(date);
			pPrlParser->getHeaderNotes(notes);
			pPrlParser->getHeaderPriceIn(price_in);
			// Get type; i.e. if typeof = -1 the prislist isn't ready to be used; 071219 p�d
			*status = m_recSelectedPricelist.getTypeOf();
			bReturn = TRUE;
		}	// if (pPrlParser->LoadFromBuffer(m_recSelectedPricelist.getPricelistFile()))
	}	// if (pPrlParser && pMDIChild != NULL)
	delete pPrlParser;

	return bReturn;
}

// Events controld by Database navigation bar in HMSShell; 060112 p�d
void CPricelistsView::actionDBNavigationMessage(WPARAM id)
{
	switch (id)
	{
		case ID_DBNAVIG_START :		// Go to first item in list
		{
			m_nDBNavigationCounter = 0;
			setupDBNaivgationToolBar(FALSE,TRUE);
			setupPricelistOnStartup();
			break;
		}

		case ID_DBNAVIG_PREV :		// Go to previous item in list
		{
			m_nDBNavigationCounter--;
			if (m_nDBNavigationCounter < 0)
				m_nDBNavigationCounter = 0;
			if (m_nDBNavigationCounter == 0)
			{
				setupDBNaivgationToolBar(FALSE,TRUE);
			}
			else
			{
				setupDBNaivgationToolBar(TRUE,TRUE);
			}
			setupPricelistOnStartup();
			break;
		}

		case ID_DBNAVIG_NEXT :		// Go to previous item in list
		{
			m_nDBNavigationCounter++;
			if (m_nDBNavigationCounter > (m_vecPricelist.size() - 1))
				m_nDBNavigationCounter = m_vecPricelist.size() - 1;

			if (m_nDBNavigationCounter == (int)m_vecPricelist.size() - 1)
			{
				setupDBNaivgationToolBar(TRUE,FALSE);
			}
			else
			{
				setupDBNaivgationToolBar(TRUE,TRUE);
			}
			setupPricelistOnStartup();
			break;
		}

		case ID_DBNAVIG_END :		// Go to last item in list
		{
			m_nDBNavigationCounter = (int)m_vecPricelist.size() - 1;
			setupDBNaivgationToolBar(TRUE,FALSE);
			setupPricelistOnStartup();
			break;
		}

		case ID_DBNAVIG_LIST :		// Show ALL items in pricelist database table; 060412 p�d
		{
			openPricelistListFormView();
			break;
		}
	}
}

void CPricelistsView::setupDBNaivgationToolBar(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}


// CPricelistsView message handlers

BOOL CPricelistsView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int spc_id, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();
	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	CXTPTabManagerItem *pItem =	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	pItem->SetData(spc_id);
	

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CPricelistsView::UpdateDocTitle()
{
	GetDocument()->UpdateFrameCounts();
}

// PUBLIC
void CPricelistsView::doPouplate(UINT idx)
{
	if (idx >= 0 && idx < m_vecPricelist.size())
	{
		m_recSelectedPricelist = (CTransaction_pricelist)m_vecPricelist[idx];
		m_nDBNavigationCounter = idx;
		setupOpenPricelist();
		setDataEnterDlgValues();
		setGeneralInfoDlgValues(TRUE);
		setupDBNaivgationToolBar(m_nDBNavigationCounter > 0,
			m_nDBNavigationCounter < (m_vecPricelist.size()-1));
		setGlobalLock_matris(m_recSelectedPricelist.getTypeOf() == 1); // LOCKED = 1
	}
}

int CPricelistsView::getDBNavigationCounter(void)
{
	return m_nDBNavigationCounter;
}

int CPricelistsView::getNumOfItemsInPricelist(void)
{
	return (int)m_vecPricelist.size();
}

BOOL CPricelistsView::setupNewPricelist(void)
{
	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		pMDIChild->m_wndGeneralInfoDlg.clearDlg();
	}

	int nNumOfPages = m_wndTabControl.getNumOfTabPages();
	if (nNumOfPages > 0)
	{
		for (int i = 0;i < nNumOfPages;i++)
		{
			CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

			if (pSelectedPage)
			{
				// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
				CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
				if (pView)
				{
					pView->removePricelistAndQualDesc();
					pView->resetDiamterClassEntries();
					pSelectedPage->Remove();
				}	// if (pView)
			}	// if (pSelectedPage)
		}	// for (int i = 0;i < nNumOfPages;i++)
	}	// if (nNumOfPages > 0)

	m_recSelectedPricelist = CTransaction_pricelist();
	m_enumPricelistState = PRICELIST_NEW;

	m_nDBNavigationCounter = m_vecPricelist.size();
	doSetNavigationBar();

	return TRUE;
}

// Pricelist from Database; 060420 p�d
void CPricelistsView::setupOpenPricelist(void)
{
	CString sMsg;
	CString sSpcNameInDB;
	int nNumOfTabs = 0;
	int nSpcID = 0;
	TCHAR szName[127];
	TCHAR szDoneBy[127];
	TCHAR szDate[127];
	TCHAR szNotes[512];
	int nRetVal;
	int nPriceIn;
	CTransaction_diameterclass rec;

	m_enumPricelistState = PRICELIST_OPEN;

	getXMLHeaderData(szName,szDoneBy,szDate,szNotes,&nPriceIn,&nRetVal);

	PricelistParser *pPrlParser = new PricelistParser();
	if (pPrlParser)
	{

		if (pPrlParser->LoadFromBuffer(m_recSelectedPricelist.getPricelistFile()))
		{
			m_wndTabControl.SetRedraw( FALSE );
			//---------------------------------------------------------------------------------
			// Clear TabControl; 060410 p�d
			m_wndTabControl.DeleteAllItems();
			// Get species in pricelist and create Tabs from it; 060410 p�d
			pPrlParser->getSpeciesInPricelistFile(m_vecSpecies);

			// Get species in datbasetable "fst_species_table"; 090608 p�d
			getSpecies(m_dbConnectionData,m_vecSpeciesInDB);

			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				sSpcNameInDB.Empty();
				// Try to match specid in m_vecSpecies (Pricelist) to m_vecSpeciesinDB and
				// set speciename from Species in "fst_species_table"; 090608 p�d
				for (UINT i1 = 0;i1 < m_vecSpeciesInDB.size();i1++)
				{
					if (m_vecSpeciesInDB[i1].getSpcID() == m_vecSpecies[i].getSpcID()) 
					{
						sSpcNameInDB = m_vecSpeciesInDB[i1].getSpcName();	
						break;
					}
				}

				AddView(RUNTIME_CLASS(CPricelistsFormView), sSpcNameInDB,m_vecSpecies[i].getSpcID(), 3);
			}

			//---------------------------------------------------------------------------------
			// Get assortments per specie; 060410 p�d
			pPrlParser->getAssortmentPerSpecie(m_vecAssort);
			
			// Add assortment data per speice (tab); 060410 p�d
			nNumOfTabs = m_wndTabControl.getNumOfTabPages();
			if (nNumOfTabs > 0)
			{
				for (int i = 0;i < nNumOfTabs;i++)
				{
					CXTPTabManagerItem *pPage = m_wndTabControl.getTabPage(i);
					if (pPage)
					{

						// Setup data per specie. I.e. per tab in m_wndTabControl; 060410 p�d
						nSpcID = (int)pPage->GetData();												
						// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060410 p�d
						CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pPage->GetHandle()));
						if (pView)
						{
							pView->addAssortmentsToSelectedReportPage(nSpcID,m_vecAssort);
						}	// if (pView)

						// Setup diameterclasses in Pricelist and Qualdesc reports (grids); 060410 p�d
						pPrlParser->getDCLSForSpecie(nSpcID,m_vecDiamterclass);
						for (UINT i = 0;i < m_vecDiamterclass.size();i++)
						{
							rec = m_vecDiamterclass[i];
							pView->setupOpenPricelist(rec);
						}
						// Setup pricelist data; 060411 p�d
						pPrlParser->getPriceForSpecie(nSpcID,m_vecPrlData);
						if (m_vecPrlData.size() > 0)
						{
							for (UINT i = 0;i < m_vecPrlData.size();i++)
							{
								CTransaction_prl_data recPrlData = m_vecPrlData[i];
								pView->setupOpenPricelistAddRow(recPrlData);
							}	// for (UINT i = 0;i < m_vecPrlData.size();i++)
						}	// if (m_vecPrlData.size() > 0)

						// Setup Quality tabs and name of tabs; 060411 p�d
						pPrlParser->getQualDescNameForSpecie(nSpcID,m_arrQualDescNames);
						pView->setQualDescName(m_arrQualDescNames);
						pView->delQualDescName(-1,m_arrQualDescNames);

						pPrlParser->getQualDescPercentForSpecie(nSpcID,m_vecQualDescData);
						if (m_vecQualDescData.size() > 0)
						{
							for (UINT i = 0;i < m_vecQualDescData.size();i++)
							{
								CTransaction_prl_data recQualDescData = m_vecQualDescData[i];
								pView->setupOpenQualDescAddRow(recQualDescData);
							}	// for (UINT i = 0;i < m_vecQualDescData.size();i++)
						}	// if (m_vecQualDescData.size() > 0)
						if (pView)
							pView->updateSplitter();

						pView = NULL;
					}	// if (pPage)
				}	// for (int i = 0;i < nNumOfTabs;i++)
			}	// if (nNumOfTabs > 0)
			m_wndTabControl.SetRedraw( TRUE );
			m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
		}	// if (pPrlParser->LoadFromBuffer(m_recSelectedPricelist.getPricelistFile()))
		else
		{
//			m_wndTabControl.SetRedraw( FALSE );
			//---------------------------------------------------------------------------------
			// Clear TabControl; 060410 p�d
			m_wndTabControl.DeleteAllItems();
//			m_wndTabControl.SetRedraw( TRUE );
			m_wndTabControl.RedrawControl(NULL,TRUE); // RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);
		}
		
		delete pPrlParser;
	}	// if (pPrlParser)

	doSetNavigationBar();
}
// Add an Empty row to the selected report page; can be Assortment, Pricelist and Qualitydescription(s); 060420 p�d
void CPricelistsView::addRowToSelectedReportPage(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->addEmptyRowToSelectedReportPage();
		}
	}	// if (pSelectedPage)
}

// Add an row on the selected report page; can be Assortment, Pricelist and Qualitydescription(s); 060420 p�d
void CPricelistsView::updRowOnSelectedReportPage(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->updRowOnSelectedReportPage();
		}
	}	// if (pSelectedPage)
}

// Deletes a row from the selected report page; can be Assortment, Pricelist and Qualitydescription(s); 060420 p�d
void CPricelistsView::delRowFromSelectedReportPage(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->delRowFromSelectedReportPage();
		}
	}	// if (pSelectedPage)
}

// Creates the pricelist XML-file and saves it to a database (BLOB-field); 060410 p�d
BOOL CPricelistsView::saveXMLFileFromPricelistInfoToDB(bool only_check)
{
	CString sBuffer, S, csBuf;
	CTransaction_pricelist rec;
	vecTransactionPricelist vec;
	CStringArray arrPricelistFile;
	CStringArray arrAssort;
	CStringArray arrPricelist;
	CStringArray arrQualDesc;
	CString sData,sPricelistName,sCreatedBy;
	TCHAR szDiamClasses[512];

	TCHAR szName[127];
	TCHAR szDoneBy[127];
	TCHAR szDate[127];
	TCHAR szNotes[512];

	int nNumOfTabs,cnt,nRetVal,nPriceIn;

	vec.clear();

	nNumOfTabs = m_wndTabControl.getNumOfTabPages();

	arrPricelistFile.RemoveAll();
	arrPricelistFile.Add(TAG_FIRST_ROW);
	arrPricelistFile.Add(TAG_START_PRICELIST);

	CMDIPricelistsFrame *pMDIChild = DYNAMIC_DOWNCAST(CMDIPricelistsFrame,GetParent());
	// Setup Header information
	if (pMDIChild)
	{
		// Also get header information from XML-file. Use this to compare with; 070213 p�d
		szName[0] = '\0';
		szDoneBy[0] = '\0';
		szDate[0] = '\0';
		szNotes[0] = '\0';
		getXMLHeaderData(szName,szDoneBy,szDate,szNotes,&nPriceIn,&nRetVal);


		arrPricelistFile.Add(TAG_START_HEADER);
		
		sPricelistName = pMDIChild->m_wndGeneralInfoDlg.getNameOfPricelist();
		CString csTmp = sPricelistName;
		TextToHtml(&csTmp);
		sData.Format(TAG_HEADER_NAME, csTmp);
		arrPricelistFile.Add(sData);

		sCreatedBy = pMDIChild->m_wndGeneralInfoDlg.getNameOfCreator();
		TextToHtml(&sCreatedBy);

		// Check if Editbox's empty. If so set creator equal to header in XML-file; 070213 p�d
		if (sCreatedBy.IsEmpty())
			sCreatedBy = getUserName();
		sData.Format(TAG_HEADER_DONE_BY,sCreatedBy);
		arrPricelistFile.Add(sData);

		sData.Format(TAG_HEADER_DATE,pMDIChild->m_wndGeneralInfoDlg.getCreateDate());
		arrPricelistFile.Add(sData);

		csBuf = pMDIChild->m_wndGeneralInfoDlg.getNotes();
		TextToHtml(&csBuf);
		sData.Format(TAG_HEADER_NOTES, csBuf);
		arrPricelistFile.Add(sData);

		sData.Format(TAG_HEADER_PRICE_IN,pMDIChild->m_wndGeneralInfoDlg.getTimberPriceIn());
		arrPricelistFile.Add(sData);

		arrPricelistFile.Add(TAG_END_HEADER);
	}
	else
		return FALSE;

	if (nNumOfTabs > 0)
	{
		for (int i = 0;i < nNumOfTabs;i++)
		{

			CXTPTabManagerItem *pPage = m_wndTabControl.getTabPage(i);

			if (pPage)
			{

				// Setup data per specie. I.e. per tab in m_wndTabControl; 060405 p�d
				sData.Format(TAG_START_SPECIE,pPage->GetData(),(pPage->GetCaption()));
				arrPricelistFile.Add(sData);
											
				// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
				CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pPage->GetHandle()));
				if (pView)
				{
					if (pView->getAssortments(arrAssort))
					{
						arrPricelistFile.Append(arrAssort);
					}
					
					if (pView->getDiamClasses(szDiamClasses))
					{
						arrPricelistFile.Add(szDiamClasses);
					}

					if (pView->getPricelist(arrPricelist))
					{
						arrPricelistFile.Append(arrPricelist);
					}

					if (pView->getQualDesc(arrQualDesc))
					{
						arrPricelistFile.Append(arrQualDesc);
					}
					// On save pricelist, set NOT Dirty; 061120 p�d
					pView->setIsDirty(FALSE);
				}	// if (pView)

				arrPricelistFile.Add(TAG_END_SPECIE);
			}	// if (pPage)
		}	// for (int i = 0;i < nNumOfTabs;i++)

		arrPricelistFile.Add(TAG_END_PRICELIST);

		//////////////////////////////////////////////////////
		// Export file to disk
		for (cnt = 0;cnt < arrPricelistFile.GetCount();cnt++)
		{
			sBuffer += arrPricelistFile.GetAt(cnt);
		}
	}	// if (nNumOfTabs > 0)

	// Only check if pricelist is set to "Klar att anv�nda"; 101018 p�d
	if (!checkPricelist(sBuffer)) 
	{
		return FALSE;
	}
	if (!only_check)
	{
		if (m_enumPricelistState == PRICELIST_NEW)
		{
			rec = CTransaction_pricelist(-1,(sPricelistName),m_nPricelistStatus,(sBuffer),(sCreatedBy));
		}
		else if (m_enumPricelistState == PRICELIST_OPEN && m_recSelectedPricelist.getID() > -1 /* Just to be sure */)
		{
			rec = CTransaction_pricelist(m_recSelectedPricelist.getID(),(sPricelistName),m_nPricelistStatus,(sBuffer),(sCreatedBy));
		}

		savePricelistToDB(m_dbConnectionData,rec);
	}

	return TRUE;
}

// Check assortments in pricelist. E.g. that diamter for exchange calculations
// must not be duplicated; 061117 p�d
BOOL CPricelistsView::checkAssortments(void)
{
	CString sCaption;
	CString sMsg1;
	CString sMsg2;
	CString sMsg3;
	CString sMsg;
	CString sSpecies;
	BOOL bReturn = TRUE;
	BOOL bFirstSpc = FALSE;
	int	nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	if (nNumOfTabs == 0)
	{
		return FALSE;
	}
	else
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				sCaption = xml->str(IDS_STRING147);
				sMsg1	= xml->str(IDS_STRING148);
				sMsg2	= xml->str(IDS_STRING149);
				sMsg3 = xml->str(IDS_STRING150);
			}
			delete xml;
		}
		for (int i = 0;i < nNumOfTabs;i++)
		{

			CXTPTabManagerItem *pPage = m_wndTabControl.getTabPage(i);

			if (pPage)
			{
										
				// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
				CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pPage->GetHandle()));
				if (pView)
				{
					if (!pView->checkAssortments(m_nPricelistStatus))
					{
						if (!bFirstSpc)
						{
							sSpecies += pPage->GetCaption();
							bFirstSpc = TRUE;
						}
						else
							sSpecies += _T(", ") + pPage->GetCaption();

						bReturn = FALSE;
					}
				
				}	// if (pView)
			}	// if (pPage)
		}	// for (int i = 0;i < nNumOfTabs;i++)
		if (bReturn == FALSE)
		{
			sMsg.Format(_T("%s : %s\n\n%s\n%s\n"),sMsg1,sSpecies,sMsg2,sMsg3);
			::MessageBox(0,sMsg,sCaption,MB_ICONEXCLAMATION | MB_OK);
		}
	}	// if (nNumOfTabs > 0)
	return bReturn;
}

// Saves a pricelist to a file (xml). Saves the BLOB-field in the database; 060420 p�d
BOOL CPricelistsView::savePricelistToXMLFile(LPCTSTR fn,LPCTSTR data)
{
	BOOL bIsOK = FALSE;
	PricelistParser prlParser;
	if (prlParser.LoadFromBuffer(data))
	{
		prlParser.SaveToFile(fn);
		bIsOK = TRUE;
	}
	
	return bIsOK;
}

// Saves an existing xml-file (on disk) to the database.
// Method used on Import of pricelist; 060420 p�d
BOOL CPricelistsView::saveFromXMLFileToPricelistDB(LPCTSTR xml_fn)
{
	int nCntEqNames = 0;
	vecTransactionPricelist vec;
	BOOL bIsOK = FALSE;
	TCHAR szHeaderName[128];
	TCHAR szHeaderDoneBy[128];
	CString sXMLFile;
	PricelistParser *pPrlParser = new PricelistParser();
	if (pPrlParser && fileExists(xml_fn))
	{
		if (pPrlParser->LoadFromFile(xml_fn))
		{
			pPrlParser->getHeaderName(szHeaderName);
			pPrlParser->getHeaderDoneBy(szHeaderDoneBy);
			// Try to count number of names in Pricelist table
			// that equals the name of the imported pricelist; 081212 p�d
			if (m_vecPricelist.size())
			{
				for (UINT i = 0; i < m_vecPricelist.size();i++)
				{
					CTransaction_pricelist rec = m_vecPricelist[i];
					if (rec.getName().Left(_tcslen(szHeaderName)).CompareNoCase(szHeaderName) == 0)
					{
						nCntEqNames++;
					}	// if (rec.getName().Left(_tcslen(szHeaderName)).CompareNoCase(szHeaderName) == 0)
				}	// for (UINT i = 0; i < m_vecPricelist.size();i++)
			}	// if (m_vecPricelist.size())
			// Check if name of pricelist already exists, if so
			// add a tag to the name; 081212 p�d
			if (m_vecPricelist.size())
			{
				for (UINT i = 0; i < m_vecPricelist.size();i++)
				{
					CTransaction_pricelist rec = m_vecPricelist[i];
					if (rec.getName().CompareNoCase(szHeaderName) == 0)
					{
						_stprintf(szHeaderName,_T("%s(%d)"),szHeaderName,nCntEqNames);
						pPrlParser->setHeaderName(szHeaderName);
						break;
					}	// if (rec.getName().CompareNoCase(szHeaderName) == 0)
				}	// for (UINT i = 0; i < m_vecPricelist.size();i++)
			}	// if (m_vecPricelist.size())


			vec.clear();

			pPrlParser->getXML(sXMLFile);

//			vec.push_back(CTransaction_pricelist(-1,szHeaderName,ID_RUNE_OLLAS_TYPEOF_PRICELIST_1,sXMLFile,getUserName()));
			vec.push_back(CTransaction_pricelist(-1,szHeaderName,ID_PRICELIST_IN_DEVELOPEMENT_1,sXMLFile,getUserName()));

			if (m_bConnected)	
			{
				if (savePricelistToDB(m_dbConnectionData,vec))
				{
					getPricelists(m_dbConnectionData,m_vecPricelist,ID_RUNE_OLLAS_TYPEOF_PRICELIST_1);
				}
				m_nDBNavigationCounter = (int)m_vecPricelist.size() - 1;
				setupPricelistOnStartup();
				// Set List toolbar button			
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,m_vecPricelist.size() > 0);

				bIsOK = TRUE;
			}
		}
	}
	delete pPrlParser;

	return TRUE;
}
// Adds a specie to the active priclist; 060420 p�d
BOOL CPricelistsView::addSpecieToPricelist(void)
{
	BOOL bIsOK = FALSE;
	int nNumOfTabPages = 0;
	CSelectSpeciesDlg *dlg = new CSelectSpeciesDlg();
	int nNumOfPages = m_wndTabControl.getNumOfTabPages();

	// Get species already used in pircelist, and add them to
	// the m_vecSpeciesUsed vector in CSelectSpeciesDlg; 061120 p�d
	if (nNumOfPages > 0)
	{
		for (int i = 0;i < nNumOfPages;i++)
		{
			CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getTabPage(i);

			if (pSelectedPage)
			{
				dlg->setSpeciesUsed((int)pSelectedPage->GetData());
			}	// if (pSelectedPage)
		}	// for (int i = 0;i < nNumOfPages;i++)
	}	// if (nNumOfPages > 0)

	if (dlg->DoModal() == IDOK)
	{
		m_wndTabControl.SetRedraw( FALSE );
		nNumOfTabPages = m_wndTabControl.getNumOfTabPages();

		// Setup new tab(s); 060327 p�d
		m_vecSpecies = dlg->getSpeciesSelected();

		for (UINT i = nNumOfTabPages;i < m_vecSpecies.size()+nNumOfTabPages;i++)
		{
			AddView(RUNTIME_CLASS(CPricelistsFormView), m_vecSpecies[i-nNumOfTabPages].getSpcName(),
																									m_vecSpecies[i-nNumOfTabPages].getSpcID(), 3);

			CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getTabPage(i);
			if (pSelectedPage)
			{
				// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
				CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
				if (pView)
				{
					pView->addEmptyRowToSelectedReportPage();
				}
			}	// if (pSelectedPage)

		}
		m_wndTabControl.SetRedraw( TRUE );
		m_wndTabControl.RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

		bIsOK = TRUE;
	}
	delete dlg;
	
	setupDBNaivgationToolBar(m_nDBNavigationCounter > 0,(int)m_nDBNavigationCounter < ((int)m_vecPricelist.size()-1));
	return bIsOK;
}
// Removes a specie (tab), from the active specie.
// The specie that is removed is the selected tab on the control; 060420 p�d
BOOL CPricelistsView::removeSpecieFromPricelist(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		pSelectedPage->Remove();
		return TRUE;
	}

	return FALSE;
}

// Determin what to do when user clicks the New button alt. Ctrl+N alt. F3; 060329 p�d
void CPricelistsView::setToDoOnNewBtn(void)
{
	m_bIspricelistLocked = FALSE;
	setupNewPricelist();
	addSpecieToPricelist();
	// Get status of pricelist, set in ComboBox on GeneralInfoDialog; 080114 p�d
	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		pMDIChild->setToolbarBtns(TRUE,FALSE);
		pMDIChild->m_wndGeneralInfoDlg.setStatus();
		pMDIChild->m_wndGeneralInfoDlg.setDoSave(1);
	}
	// And enable windows for GeneralData and Diameterclass etc.; 080703 p�d
	setEnableWindows(TRUE,TRUE,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
}

// Determin what to do when user clicks the Save button; 060405 p�d
void CPricelistsView::setToDoOnSaveBtn(void)
{
	if (saveData(CPricelistsView::ON_SAVE))
	{
		getPricelists(m_dbConnectionData,m_vecPricelist,ID_RUNE_OLLAS_TYPEOF_PRICELIST_1);
		// Also reload "m_recSelectedPricelist"; 081010 p�d
		if (m_nDBNavigationCounter >= 0 && m_nDBNavigationCounter < m_vecPricelist.size())
			m_recSelectedPricelist = m_vecPricelist[m_nDBNavigationCounter];
	}	// if (saveData())
}

// Determin what to do when user clicks the Delete button alt. Ctrl+N alt. F3; 060329 p�d
void CPricelistsView::setToDoOnDelBtn(void)
{
	BOOL bOkToRemove = TRUE;
	if (m_bIspricelistLocked)
	{
		::MessageBox(this->GetSafeHwnd(),m_sPricelistExistsMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		bOkToRemove = FALSE;
	}

	if (bOkToRemove)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				// Ask user if he realy wnt's to remove pricelist; 060424 p�d
				if (::MessageBox(0,(xml->str(IDS_STRING146)),(xml->str(IDS_STRING151)),MB_DEFBUTTON2 | MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
				{				
					removePricelistFromDB();
					m_nDBNavigationCounter = (int)m_vecPricelist.size() - 1;
					setupPricelistOnStartup();
				} // if (::MessageBox(0,(xml->str(IDS_STRING146)),(xml->str(IDS_STRING151)),MB_DEFBUTTON2 | MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
			}	// if (bOkToRemove)
			delete xml;
		}	// if (xml->Load(m_sLangFN))
	}
}

// PUBLIC

// Method used in DataEnter dialog, to set diamterclasses on active report; 060420 p�d
BOOL CPricelistsView::setupNewPricelist(CTransaction_diameterclass &rec)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{

		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->setupNewPricelist(rec);
			return TRUE;
		}	// if (pView)
	}	// if (pSelectedPage)
	
	return FALSE;
}

// Removes columns from pricelist and quality description(s); 060420 p�d
BOOL CPricelistsView::removePricelist(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->removePricelistAndQualDesc();
			return TRUE;
		}
	}	// if (pSelectedPage)
	
	return FALSE;
}

// When user clicks a Speice-tab, set values for that tad in the
// CDataEnterDlg; 060329 p�d

BOOL CPricelistsView::setDataEnterDlgValues(void)
{
	CXTPTabManagerItem *pSelectedPage = NULL;
	pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->setDataEnterDlgValues();
			return TRUE;
		}
	}	// if (pSelectedPage)
	
	return FALSE;
}

BOOL CPricelistsView::setChangePrlPriceDlgValues(void)
{
	CXTPTabManagerItem *pSelectedPage = NULL;
	pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->setPrlPriceDlgValues();
			return TRUE;
		}
	}	// if (pSelectedPage)
	
	return FALSE;
}

BOOL CPricelistsView::setGeneralInfoDlgValues(BOOL lock)
{
	BOOL bReturn = FALSE;
	TCHAR szName[127];
	TCHAR szDoneBy[127];
	TCHAR szDate[127];
	TCHAR szNotes[512];
	int nPriceIn,nTypeOf = 1;
	int nRetVal = 1;

	CString S;

	BOOL bFoundInPricelist = FALSE;
	int prlID;
	CTransaction_template recTmpl;
	vecTransactionTemplate vecTmpl;
	TCHAR szFuncName[50];

	if (m_enumPricelistState == PRICELIST_OPEN)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				if (m_nDBNavigationCounter >= 0 && m_nDBNavigationCounter < m_vecPricelist.size())
				{
					CTransaction_pricelist recPrl = m_vecPricelist[m_nDBNavigationCounter];
					// Add a check to see if pricelist, to be deleted, maybe in a stand-template.
					// Is so, don't delete, tell user; 081215 p�d
					CUMPricelistDB *pDB = new CUMPricelistDB(m_dbConnectionData);
					if (pDB != NULL) if (pDB->getTemplates(vecTmpl))
					
					// Is there any data to check by; 081215 p�d
					if (vecTmpl.size() > 0)
					{
						TemplateParser pars;
						for (UINT i = 0;i < vecTmpl.size();i++)
						{
							recTmpl = vecTmpl[i];

							if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
							{
								pars.getTemplatePricelist(&prlID,szFuncName);
								if (prlID == recPrl.getID())
								{
									bFoundInPricelist = TRUE;
									break;
								}	// if (prlID == recTmpl.getID())
							}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
						}	// for (UINT i = 0;i < vecTmpl.size();i++)
					}	// if (vecTmpl.size() > 0)
					// If not found in StandTemplates, check in "esti_trakt_misc_data_table"; 100119 p�d
					if (!bFoundInPricelist)
					{
						if (pDB != NULL) bFoundInPricelist = pDB->isPricelistInStand(recPrl.getName(),nTypeOf);
					}

					if (pDB) delete pDB;
				} // if (m_nDBNavigationCounter >= 0 && m_nDBNavigationCounter < m_vecPricelist.size())
			}	// if (xml->Load(m_sLangFN))
		} // if (fileExists(m_sLangFN))

		m_bIspricelistLocked = bFoundInPricelist;

		CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
		if (pMDIChild != NULL)
		{
			szDoneBy[0] = '\0';
			szNotes[0] = '\0';
			szName[0] = '\0';
			getXMLHeaderData(szName,szDoneBy,szDate,szNotes,&nPriceIn,&nRetVal);
 			CXTPDockingPaneManager *pPaneManager = pMDIChild->GetDockingPaneManager();
			if (pPaneManager != NULL)
			{
				CXTPDockingPane *pPane = pPaneManager->FindPane(IDC_GENERAL_INFO_PANE_1);
				if (pPane != NULL)
				{
					// We need to check if Pane is hidden. If so, show pane.
					// This is because data isn't added to a hidden pane; 090127 p�d
					if (pPane->IsHidden()) pPaneManager->ToggleAutoHide(pPane);
					CGeneralInfoDlg *pDlg = (CGeneralInfoDlg *)pPane->GetChild();
					if (pDlg != NULL)
					{
						if (nRetVal > -1)
							pDlg->setStatusOfPricelist(nRetVal,0);
						else if (nRetVal == -1)
							pDlg->setStatusOfPricelist(nRetVal,0);						
						pDlg->setNameOfPricelist(szName);
						pDlg->enableName(bFoundInPricelist);
						pDlg->setNameOfCreator(szDoneBy);
						pDlg->setNotes(szNotes);
						pDlg->setTimberPriceIn(nPriceIn);
						pDlg->setLock(lock);
					}	// if (pDlg != NULL)
				}	// if (pPane != NULL)
			}	// if (pPaneManager != NULL)
			// Display data read from database; 060410 p�d
			bReturn = TRUE;
		}	// if (pPrlParser->LoadFromBuffer(m_recSelectedPricelist.getPricelistFile()))
	}
	return bReturn;
}

// Adds or edits a qualitydescription. I.e. add a tab or changes the name of an quslitydescription tab; 060420 p�d
BOOL CPricelistsView::setAddOrEditQualDesc(void)
{
	CStringArray arr;
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();

	if (pSelectedPage)
	{
		// Start by savin' pricelist; 081215 p�d
		//setToDoOnSaveBtn();
		CQualitedescDlg *dlg = new CQualitedescDlg();
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView)
		{
			pView->getQualDescName(arr);
			dlg->setQualDescFromArray(arr);
		
			m_bQDescChanged = FALSE;
			if (dlg->DoModal() == IDOK)
			{
				dlg->getQualDescFromArray(arr);
				pView->delQualDescName(dlg->getRemovedQualityDescIdx(),arr);
				pView->setQualDescName(arr);
				m_bQDescChanged = TRUE;
			}	// if (dlg->DoModal() == IDOK)
	
		}	// if (pView)
		delete dlg;

		return TRUE;
	}	// if (pSelectedPage)
	return FALSE;
}


void CPricelistsView::setEnableWindows(BOOL enabled,BOOL w1_also,BOOL lock)
{
	CString S;
	CXTPDockingPaneManager *pPaneManager = NULL;
	CXTPDockingPane *pPane1 = NULL;
	CGeneralInfoDlg *pDlg1 = NULL;
	CXTPDockingPane *pPane2 = NULL;
	CXTPDockingPane *pPane3 = NULL;
	CDataEnterDlg *pDlg2 = NULL;
	CChangePrlPriceDlg *pDlg3 = NULL;
	// Get status of pricelist, set in ComboBox on GeneralInfoDialog; 080114 p�d
	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame*)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		pPaneManager = pMDIChild->GetDockingPaneManager();
		if (pPaneManager != NULL)
		{
			pPane1 = pPaneManager->FindPane(IDC_GENERAL_INFO_PANE_1);
			if (pPane1 != NULL)
			{
				if (enabled)
					pPane1->SetEnabled(xtpPaneEnabled);
				else
					pPane1->SetEnabled(xtpPaneDisabled);

				pDlg1 = (CGeneralInfoDlg *)pPane1->GetChild();
				if (pDlg1 != NULL)
				{
					pDlg1->setEnableWindows(enabled,w1_also,lock);
				}

			}
			pPane2 = pPaneManager->FindPane(IDC_SETTINGS_PANE);
			if (pPane2 != NULL)
			{
				if (enabled)
					pPane2->SetEnabled(xtpPaneEnabled);
				else
					pPane2->SetEnabled(xtpPaneDisabled);

				pDlg2 = (CDataEnterDlg *)pPane2->GetChild();
				if (pDlg2 != NULL)
				{
					if (lock)
						pDlg2->setEnableWindows(!lock);
					else
						pDlg2->setEnableWindows(enabled);
				}
			}

			pPane3 = pPaneManager->FindPane(IDC_CHANGE_PRICE_PANE);
			if (pPane3 != NULL)
			{
				if (enabled)
					pPane3->SetEnabled(xtpPaneEnabled);
				else
					pPane3->SetEnabled(xtpPaneDisabled);
				if (lock)
					pPane3->SetEnabled(xtpPaneDisabled);
			}
		}
	}
	m_bIsEnabledData = enabled;

	pPaneManager = NULL;
	pPane1 = NULL;
	pDlg1 = NULL;
	pPane2 = NULL;
	pDlg2 = NULL;
	pMDIChild = NULL;
}

// Set the priclist to show the last pricelist in database; 060420 p�d
BOOL CPricelistsView::setupPricelistOnStartup(void)
{
	// Check if there's any items in Pricelist; 060518 p�d
	if (m_vecPricelist.size() > 0)
	{
		if (m_vecPricelist.size() == 1)
		{
			m_nDBNavigationCounter = 0;	// Only one item in list; 060420 p�d
			m_recSelectedPricelist = m_vecPricelist[m_nDBNavigationCounter];
			setupOpenPricelist();
			setDataEnterDlgValues();
			setGeneralInfoDlgValues(m_recSelectedPricelist.getTypeOf() == 1);

			setGlobalLock_matris(m_recSelectedPricelist.getTypeOf() == 1);	// LOCK

		}
		else if (m_nDBNavigationCounter >= 0 && m_nDBNavigationCounter < m_vecPricelist.size())
		{
			m_recSelectedPricelist = m_vecPricelist[m_nDBNavigationCounter];
			setupOpenPricelist();
			setDataEnterDlgValues();
			setGeneralInfoDlgValues(m_recSelectedPricelist.getTypeOf() == 1);
			setGlobalLock_matris(m_recSelectedPricelist.getTypeOf() == 1);	// LOCK
		}
		setEnableWindows(TRUE,FALSE,m_recSelectedPricelist.getTypeOf() == 1);
	}
	else
	{
		setupNewPricelist();
		m_recSelectedPricelist = CTransaction_pricelist();
		setEnableWindows(FALSE,TRUE,FALSE);
		// Get status of pricelist, set in ComboBox on GeneralInfoDialog; 080114 p�d
		CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
		if (pMDIChild != NULL)
		{
			pMDIChild->setToolbarBtns(FALSE,FALSE);
			pMDIChild->m_wndGeneralInfoDlg.setStatus();
		}

		// Not used: 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),m_sNoPricelistExistsMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
	}
/*
	// Get status of pricelist, set in ComboBox on GeneralInfoDialog; 080114 p�d
	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		pMDIChild->setToolbarBtns(m_vecPricelist.size() > 0 ,m_recSelectedPricelist.getTypeOf() == 1);
	}
*/
	return TRUE;
}

// Get the name of a pricelist, and the xml-file in blob-field; 060420 p�d
BOOL CPricelistsView::getPricelistXML(CString &name,CString &data)
{
	if (m_nDBNavigationCounter >= 0 && m_nDBNavigationCounter < m_vecPricelist.size())
	{
		name = m_vecPricelist[m_nDBNavigationCounter].getName();
		data = m_vecPricelist[m_nDBNavigationCounter].getPricelistFile();
		return TRUE;
	}	// if (m_nDBNavigationCounter >= 0 && m_nDBNavigationCounter < m_vecPricelist.size())

	return FALSE;
}

// Open the formview to show a list of entered priclists in database.
// Can select a pricelist from this view; 060420 p�d
void CPricelistsView::openPricelistListFormView(void)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CString sDocTitle;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(IDD_FORMVIEW1);

	RLFReader *xml = new RLFReader();
	if (xml->Load(m_sLangFN))
	{
		sCaption = xml->str(IDD_FORMVIEW1);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			POSITION posDOC = pTemplate->GetFirstDocPosition();

			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
				}

				break;
			}	// if (posDOC == NULL)
		}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
	}	// while(pos != NULL)

}


void CPricelistsView::openPopupMenuForQualityTab(void)
{
	CPoint curPos;
	CMenu menu;
	CString sMenu1;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sMenu1 = (xml->str(IDS_STRING116));
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}

	VERIFY(menu.CreatePopupMenu());
	GetCursorPos(&curPos);
	// create main menu items
	menu.AppendMenu(MF_STRING, ID_POPUPMENU_QUALITY, (sMenu1));
	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, 
							curPos.x, curPos.y, this, NULL);
	// other general items
	switch (nMenuResult)
	{
		case ID_POPUPMENU_QUALITY :
		{
			setAddOrEditQualDesc();
			break;
		}
	}

}

// Remove a pricelist from the database; 060420 p�d
BOOL CPricelistsView::removePricelistFromDB(void)
{
	if (m_bConnected)	
	{

		// Add species in Database to ListControl; 060328 p�d
		if (m_vecPricelist.size() > 0)
		{
			if (m_nDBNavigationCounter >= 0 && m_nDBNavigationCounter < m_vecPricelist.size())
			{
				if (removePricelistFromDatabase(m_dbConnectionData,m_vecPricelist[m_nDBNavigationCounter]))
				{
					// Reload data; 060419 p�d
					getPricelists(m_dbConnectionData,m_vecPricelist,ID_RUNE_OLLAS_TYPEOF_PRICELIST_1);
				}
			}
		}
	}
	return FALSE;
}


// Check if entered name havn't already been used; 071217 p�d
short CPricelistsView::isPricelistNameOK(LPCTSTR name,enumSaveMsg show_msg_type)
{
	int nPricelistID = -1;
	CString sPricelistName;
	CMDIPricelistsFrame *pMDIChild = DYNAMIC_DOWNCAST(CMDIPricelistsFrame,GetParent());

	int	nNumOfTabs = m_wndTabControl.getNumOfTabPages();

	// Setup Header information
	if (pMDIChild)
	{
		sPricelistName = pMDIChild->m_wndGeneralInfoDlg.getNameOfPricelist();
		// Check if Editbox's empty or no species added; 081023 p�d
		if (sPricelistName.IsEmpty() || nNumOfTabs == 0)
		{
			//�ndrat, tycker att det skall vara samma beteende om man avslutar eller stegar mellan 20110824 J� Bug #2271
			/*
			if (show_msg_type == CPricelistsView::ON_SAVE)
			{
			::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
			return 0;	// 0 == FALSE
			}
			if (show_msg_type == CPricelistsView::ON_PREV_NEXT)
			{
			::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
			return 1;	// 1 = ... without saving
			}
			else if (show_msg_type == CPricelistsView::ON_QUIT)
			{
			if (::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg2),(m_sMsgCap),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			return 1;	// 1 = Quit without saving
			else
			return 0;	// Don't quit
			}*/
			switch(show_msg_type)
			{
			case CPricelistsView::ON_SAVE:
				::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
				return 0;	// 0 == FALSE
			case CPricelistsView::ON_PREV_NEXT:
			case CPricelistsView::ON_QUIT:
				if (::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg2),(m_sMsgCap),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					return 1;	// 1 = Quit without saving
				else
					return 0;	// Don't quit
				break;
			}

		}
		//	Kanske l�gga in en koll h�r om prislistan ing�r i en best�ndsmall Kolla Id och Typ i xml f�lt i tabellen tmpl_template_table
		//	och i s� fall s�ga till om att den bara kan sparas med status klar att anv�nda samt fr�ga om man skall avsluta utan att spara �ndringar
		// 20110826 J� Bug #2271
		if(isPriceListInStandTemplate() && pMDIChild->m_wndGeneralInfoDlg.getStatus()!=0)
		{

			switch(show_msg_type)
			{
			case CPricelistsView::ON_SAVE:
				::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg4),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
				return 0;	// 0 == FALSE
			case CPricelistsView::ON_PREV_NEXT:
			case CPricelistsView::ON_QUIT:
				if (::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg3),(m_sMsgCap),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					return 1;	// 1 = Quit without saving
				else
					return 0;	// Don't quit
			}
		}
	}
	// Add species in Database to ListControl; 060328 p�d
	if (m_vecPricelist.size() > 1)
	{
		if (m_vecPricelist.size() >= 1)
			nPricelistID = m_recSelectedPricelist.getID();
		for (UINT i = 0;i < m_vecPricelist.size();i++)
		{
			if (m_vecPricelist[i].getName().CompareNoCase(name) == 0 &&
					m_vecPricelist[i].getID() != nPricelistID && 
					nPricelistID > -1)
			{
				::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg1),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
				return 0;
			}
		}	// for (UINT i = 0;i < m_vecPricelist.size();i++)
	}	// if (m_vecPricelist.size() > 0)
	else if (m_vecPricelist.size() == 1 && 	m_enumPricelistState == PRICELIST_NEW)
	{
		if (m_recSelectedPricelist.getName().CompareNoCase(name) == 0)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMissingInfoMsg1),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			return 0;
		}
	}	// else if (m_vecPricelist.size() == 1)

	return 2;
}

void CPricelistsView::addRowToAssortments(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();
	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView != NULL)
		{	
			pView->addEmptyRowToAssortment();
			pView = NULL;
		}
	}
}

void CPricelistsView::delRowFromAssortments(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();
	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView != NULL)
		{	
			pView->delRowFromAssortment();
			pView = NULL;
		}
	}
}

void CPricelistsView::addRowToPricelistAndQualities(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();
	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView != NULL)
		{	
			pView->addEmptyRowToPricelistAndQuality();
			pView = NULL;
		}
	}
}

void CPricelistsView::delRowFromPricelistAndQualities(void)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();
	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView != NULL)
		{	
			pView->delRowFromPricelistAndQuality();
			pView = NULL;
		}
	}
}

void CPricelistsView::calcPriceToPricelist(BOOL calc_up,BOOL in_percent,int value)
{
	CXTPTabManagerItem *pSelectedPage = m_wndTabControl.getSelectedTabPage();
	if (pSelectedPage)
	{
		// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
		CPricelistsFormView *pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()));
		if (pView != NULL)
		{	
			if (calc_up)
				pView->recalculatePricelist_up(in_percent,value);
			else
				pView->recalculatePricelist_down(in_percent,value);
			pView = NULL;
		}
	}
}
