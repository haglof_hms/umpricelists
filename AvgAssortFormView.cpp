// PricelistsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "AvgAssortView.h"
#include "AvgAssortFormView.h"

#include "ResLangFileReader.h"

CTextItem::CTextItem(CString sValue,CWnd *wnd) : CXTPReportRecordItemText(sValue)
{
	m_wnd = wnd;
	m_sText = sValue;
}

void CTextItem::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
{
	m_sText = szText;
	SetValue(m_sText);
}

CString CTextItem::getTextItem(void)	
{ 
	return m_sText; 
}

void CTextItem::setTextItem(LPCTSTR text)	
{ 
	m_sText = text; 
	SetValue(m_sText);
}

// CAvgAssortFormView

IMPLEMENT_DYNCREATE(CAvgAssortFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CAvgAssortFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEACTIVATE()
	ON_NOTIFY(NM_KEYDOWN, IDC_ASSORTMENTS_2, OnAssortment2KeyDown)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_ASSORTMENTS_2, OnReportItemValueClick)
END_MESSAGE_MAP()

CAvgAssortFormView::CAvgAssortFormView()
	: CXTResizeFormView(CAvgAssortFormView::IDD)
{
	m_bInitialized = FALSE;
	m_sAbrevLangSet = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

}

CAvgAssortFormView::~CAvgAssortFormView()
{
}

void CAvgAssortFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP

}

BOOL CAvgAssortFormView::OnEraseBkgnd(CDC *pDC)
{

	CRect clip;
	m_wndAssortSettings.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_3DFACE));

	return FALSE;
}

BOOL CAvgAssortFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CAvgAssortFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	int iSel = 0;
	BOOL bResize = FALSE;
	if (!	m_bInitialized )
	{
		setupAssortSettings();

		m_bInitialized = TRUE;

	}	// if (!	m_bInitialized )

}

void CAvgAssortFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndAssortSettings.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndAssortSettings,1,1,rect.right - 1,rect.bottom-1);
	}
}

int CAvgAssortFormView::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CAvgAssortFormView::OnAssortment2KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
/* COMMENTED OUT 2011-09-23 P�D; USED BUTTONS INSTEAD (#1637)
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	// TAB key'll result in a new Row, if a name of quality
	// is added on previous row;  071206 p�d
	if (lpNMKey->nVKey == VK_DOWN || lpNMKey->nVKey == VK_TAB)
	{
		// Get Rows and Columns already in m_wndPricelist and use
		// this to check if user's at the end of a column and that he's
		// on the last row, before adding a new row; 070618 p�d
		// OBS! Also check that user have entered something in the first
		// column. i.e he must add a name of the Assortment; 070618 p�d
		CXTPReportRows *pRows = m_wndAssortSettings.GetRows();
		CXTPReportColumns *pCols = m_wndAssortSettings.GetColumns();
		CXTPReportRecords *pRecs = m_wndAssortSettings.GetRecords();
		if (pCols != NULL && pRows != NULL && pRecs != NULL)
		{
			CXTPReportColumn *pCol = m_wndAssortSettings.GetFocusedColumn();
			CXTPReportRow *pRow = m_wndAssortSettings.GetFocusedRow();
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecs->GetAt(pRow->GetIndex());
			if (pCol != NULL && pRow != NULL && pRec != NULL)
			{
//				if (pCol->GetIndex() == 0 /*pCols->GetCount() - 1* / && 
//						pRow->GetIndex() == pRows->GetCount() - 1 &&
//						pRec->getColumnText(0) != _T(""))
				if (pRec->getColumnText(0) != _T("") && 
						pRow->GetIndex() == pRows->GetCount()-1)
				{
					addEmptyRowToSelectedReportPage();			
					m_wndAssortSettings.setIsDirty( TRUE );
				}	// if (pCol->GetIndex() == pCols->GetCount() - 1)
			}	// if (pCol != NULL)
		}	// if (pCols != NULL)
	}	// if (lpNMKey->nVKey == VK_TAB && GetKeySTate(VK_SHIFT) == 0)
	
	if (lpNMKey->nVKey == VK_DELETE || 
			 lpNMKey->nVKey == VK_F4)// || 
//			 lpNMKey->nVKey == VK_UP)
	{
		if (delRow())
		{
			m_wndAssortSettings.setIsDirty( TRUE );
			m_wndAssortSettings.SetFocus();
		}
	}	// else	if (lpNMKey->nVKey == VK_TAB && GetKeySTate(VK_SHIFT) == 1)
*/
}

/*
ASSORTMENT_PULPWOOD_MINDIAM	= 5.0;
ASSORTMENT_TIMBER_MINDIAM		= 10.0;
ASSORTMENT_TIMBER_MAXDIAM		= 25.0;
*/
void CAvgAssortFormView::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bDiamOK = TRUE;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{	
		// Only do this check if we are on Column 1; 071204 p�d
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_2)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pItemNotify->pItem->GetRecord();
			int nAssortTypeIndex = findPulpWoodTypeIndexAsInt(pRec->getColumnsInText(COLUMN_1));
			if (nAssortTypeIndex > -1)
			{
				double fMinDiam = pRec->getColumnFloat(COLUMN_2);
				if (nAssortTypeIndex == 0)	// "Timmer"
				{
					if (fMinDiam < ASSORTMENT_TIMBER_MINDIAM || fMinDiam > ASSORTMENT_TIMBER_MAXDIAM)
					{
						bDiamOK = FALSE;
					}
				}
				else	// Pulpwood
				{
					if (fMinDiam < ASSORTMENT_PULPWOOD_MINDIAM || fMinDiam > ASSORTMENT_PULPWOOD_MAXDIAM)
					{
						bDiamOK = FALSE;
					}
				}
				// Tell user that he has entered a value outside the bouderies for
				// Timber and Pulpwood min,max diamters; 071204 p�d
				if (!bDiamOK)
				{
					::MessageBox(this->GetSafeHwnd(),m_sMinMaxDiameterMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					bDiamOK = TRUE;
					m_wndAssortSettings.SetFocus();
				}
			}	// if (nAssortTypeIndex > -1)
		}	// if (pItemNotify->pColumn->GetItemIndex() == 1)
		//L�gg in en koll h�r s� att sortimentsnamnet inte anv�nds i n�got annat sortiment redan f�r tr�dslaget
		//(Kollar bara namnet ej typen)
		//Bug #2719 20111227 J�
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0 || pItemNotify->pColumn->GetItemIndex() == COLUMN_1)
		{			
				
			CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
			CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
			if (pRecords && pColumns)
			{
				CString strAssName=_T(""),csBuf=_T("");
				int nRow=0,nFound=0;
				//CString csCalcType1=0,csCalcType2=0;
				CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pItemNotify->pItem->GetRecord();
				strAssName=pRec->getColumnText(COLUMN_0);
				//csCalcType1=findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1));
				for (nRow = 0;nRow < pRecords->GetCount();nRow++)
				{
					CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(nRow);
					if (pRec)
					{
						
						if (!pRec->getColumnsInText(COLUMN_0).IsEmpty() || pRecords->GetCount() > 1)
						{
							csBuf=_T("");
							csBuf = pRec->getColumnsInText(COLUMN_0);
							//csCalcType2=findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1));
							//if(strAssName==csBuf && csCalcType1==csCalcType2)
							if(strAssName==csBuf)
								nFound++;

						}	// if (!pRec->getColumnsInText(COLUMN_0).IsEmpty())
					}	// if (pRec)
				}	// for (row = 0;row < pRecords->GetCount();row++)	
				if(nFound>1)
				{
					CString csMsg=_T("");
					csMsg.Format(_T("%s %s %s"),m_sMsgAssName,strAssName,m_sMsgAssName2);
					::MessageBox(this->GetSafeHwnd(),csMsg,m_sMsgAssNameCaption,MB_ICONEXCLAMATION | MB_OK);					
				}
			}
		}

		// HMS-114 kontroll av otill�tna tecken i sortimentsnamn 20250205 J�
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
		{
			CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pItemNotify->pItem->GetRecord();
			CString csBuf=pRec->getColumnText(COLUMN_0);
			if (!csBuf.IsEmpty())
			{
				int nIndex = csBuf.FindOneOf(_T("< > ' \" & /"));
				if (nIndex > -1)
				{
					csBuf.Delete(nIndex);
					do
					{
						nIndex = csBuf.FindOneOf(_T("< > ' \" & /"));
						if (nIndex > -1)
							csBuf.Delete(nIndex);
						if(csBuf.IsEmpty() || csBuf.GetLength()<=0)
							nIndex=0;
					}while(nIndex > -1);
					pRec->setColumnText(COLUMN_0,csBuf);
					CString csMsg=_T("");
					csMsg.Format(_T("%s < > ' \" & /"),m_sMsgTeckenfel);
					::MessageBox(this->GetSafeHwnd(),csMsg,m_sMsgAssNameCaption,MB_ICONEXCLAMATION | MB_OK);	
					
				}
			}
		}


		m_wndAssortSettings.setIsDirty( TRUE );
	}
}

// CAvgAssortFormView diagnostics

#ifdef _DEBUG
void CAvgAssortFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CAvgAssortFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// PROTECTED


// Create and add Assortment settings reportwindow
BOOL CAvgAssortFormView::setupAssortSettings(void)
{
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndAssortSettings.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndAssortSettings.Create(this, IDC_ASSORTMENTS_2, FALSE, FALSE ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndAssortSettings.GetSafeHwnd() != NULL)
				{
					// Add string data here; 070130 p�d
					m_sMsgCap	= (xml->str(IDS_STRING151));
					m_sDeleteAssortmentRow = (xml->str(IDS_STRING153));
					m_sRemoveAssortmentMsg = (xml->str(IDS_STRING156));
					m_sAssortmentNameMissingMsg.Format(_T("%s\n%s"),
						(xml->str(IDS_STRING1570)),
						(xml->str(IDS_STRING1571)));

					m_sMsgAssName	= (xml->str(IDS_STRING2320));
					m_sMsgAssName2	= (xml->str(IDS_STRING2321));
					m_sMsgAssNameCaption= (xml->str(IDS_STRING127));
					m_sMsgTeckenfel.Format(_T("%s"),xml->str(IDS_STRING2100));

					m_sMinMaxDiameterMsg.Format(_T("%s\n%s\n\n%s\n%s"),
						(xml->str(IDS_STRING1580)),
						(xml->str(IDS_STRING1581)),
						(xml->str(IDS_STRING1582)),
						(xml->str(IDS_STRING1583)));

					m_sNotDeleteFirstRowMsg = (xml->str(IDS_STRING161));

					// Add Pulpwood types to this StringArray; 070608 p�d
					m_sarrPulpWoodTypes.RemoveAll();
					m_sarrPulpWoodTypes.Add((xml->str(IDS_STRING1552)));
					m_sarrPulpWoodTypes.Add((xml->str(IDS_STRING1550)));
					m_sarrPulpWoodTypes.Add((xml->str(IDS_STRING1551)));

					m_wndAssortSettings.ShowWindow( SW_NORMAL );
          
					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING127)), 100));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING131)), 100));
					pCol->GetEditOptions()->AddConstraint(m_sarrPulpWoodTypes.GetAt(0),0);
					pCol->GetEditOptions()->AddConstraint(m_sarrPulpWoodTypes.GetAt(1),1);
					pCol->GetEditOptions()->AddConstraint(m_sarrPulpWoodTypes.GetAt(2),2);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING128)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING129)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					pCol = m_wndAssortSettings.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING130)), 100));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					m_wndAssortSettings.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndAssortSettings.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndAssortSettings.GetReportHeader()->AllowColumnResize( TRUE );
					m_wndAssortSettings.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndAssortSettings.GetReportHeader()->SetAutoColumnSizing( TRUE );

					m_wndAssortSettings.SetMultipleSelection( FALSE );
					m_wndAssortSettings.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndAssortSettings.AllowEdit( TRUE );
					m_wndAssortSettings.FocusSubItems(TRUE);

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
					setResize(&m_wndAssortSettings,1,1,rect.right - 1,rect.bottom-1);
				}	// if (m_wndAssortSettings.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

BOOL CAvgAssortFormView::isDirty(void)
{
	return m_wndAssortSettings.isDirty();
}

void CAvgAssortFormView::setIsDirty(BOOL dirty)
{
	m_wndAssortSettings.setIsDirty(dirty);
}

// Do a check, top see if user's entered at least one
// assortment for specie; 080114 p�d
BOOL CAvgAssortFormView::isThereAssortments(void)
{
	if (m_wndAssortSettings.GetRows()->GetCount() == 0)
		return FALSE;

	CXTPReportRecords *pRecs = 	m_wndAssortSettings.GetRecords();
	if (pRecs != NULL)
	{
		if (pRecs->GetCount() >= 1)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*) pRecs->GetAt(0);	// Get first record
			if (pRec != NULL)
			{
				if (pRec->getColumnText(0).IsEmpty())
					return FALSE;
			}	// if (pRec != NULL)
		}	// if (pRecs->GetCount() >= 1)
	}	// if (pRecs != NULL)
	return TRUE;
}


// Add an empty row to Assortments or Pricelist report and Qualitydescription(s)
// depending on which window got the focus; 060410 p�d
BOOL CAvgAssortFormView::addRow(BOOL clear)
{
	if (m_wndAssortSettings.GetSafeHwnd())
	{
		m_wndAssortSettings.AddRecord(new CAssortSettingsDataRec());
		m_wndAssortSettings.Populate();
		m_wndAssortSettings.UpdateWindow();
	
		return TRUE;
	}	// if (m_wndAssortSettings.GetSafeHwnd())
	return FALSE;
}

// Remove a row from Pricelist and Qualitydescription(s); 060331 p�d
BOOL CAvgAssortFormView::delRow(void)
{
	int nRows;
	CXTPReportRecords *pRecsAssortSettings = m_wndAssortSettings.GetRecords();
	if (pRecsAssortSettings)
	{
		nRows = pRecsAssortSettings->GetCount();
		if (nRows == 1)
		{
			::MessageBox(this->GetSafeHwnd(),m_sNotDeleteFirstRowMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		// Delete Assortments; 070130 p�d
		if (::MessageBox(0,m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			CXTPReportRecords *pRecsAssortSettings = m_wndAssortSettings.GetRecords();
			if (pRecsAssortSettings)
			{
				pRecsAssortSettings->RemoveAt(nRows-1);
				m_wndAssortSettings.Populate();
				m_wndAssortSettings.UpdateWindow();
				return TRUE;
			}	// if (pRecsAssortSettings)
		}	// if (::MessageBox(0,m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	}	// if (pRecsAssortSettings)
	return FALSE;
}

// PUBLIC METHOD

// Check, what to do whan user clicks the New button in HMSShell alt. F3 or Ctrl+N; 060327 p�d
void CAvgAssortFormView::addEmptyRowToSelectedReportPage(void)
{
	if (m_wndAssortSettings.GetSafeHwnd())
	{
		m_wndAssortSettings.AddRecord(new CAssortSettingsDataRec());
		m_wndAssortSettings.Populate();
		m_wndAssortSettings.UpdateWindow();
	}	// if (m_wndAssortSettings.GetSafeHwnd())
}

// Check, what to do whan user clicks the New button in HMSShell alt. F3 or Ctrl+N; 060327 p�d
void CAvgAssortFormView::delRowFromSelectedReportPage(void)
{
	if (::MessageBox(this->GetSafeHwnd(),m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		CXTPReportRow *pRow = m_wndAssortSettings.GetFocusedRow();
		CXTPReportRecords *pRecs = m_wndAssortSettings.GetRecords();
		if (pRecs != NULL && pRow != NULL)
		{
			pRecs->RemoveRecord(pRow->GetRecord());
			m_wndAssortSettings.Populate();
			m_wndAssortSettings.UpdateWindow();
		}
	}	// if (::MessageBox(0,m_sDeleteAssortmentRow,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
}

// Get data set for Assortments and add to CStringArray arr; 060405 p�d
BOOL CAvgAssortFormView::getAssortments(CStringArray& arr,int prl_status)
{
	BOOL bOK = TRUE;
	int row;
	CString sData,S, csAssortname;
	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();

	if (pRecords && pColumns)
	{
		arr.RemoveAll();	// Clear array
		m_wndAssortSettings.Populate();	// Update data entered in ReportControl; 090210 p�d
		for (row = 0;row < pRecords->GetCount();row++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(row);
			if (pRec)
			{
				if (prl_status == ID_PRICELIST_IN_DEVELOPEMENT_2)
					bOK	= TRUE;
				else if (prl_status == ID_RUNE_OLLAS_TYPEOF_PRICELIST_2)
				{
/*
					if (pRec->getColumnText(COLUMN_0).IsEmpty() && 
							pRec->getColumnFloat(COLUMN_2) == 0.0) 
					{
							bOK = FALSE;
					}
					else
					{
						bOK = TRUE;
					}
*/
				}
				// Check that there's a assortment name entered; 071214 p�d
				if (bOK)
				{
					// Setup xml tag for Assortments; 060405 p�d
					sData = _T("<Assortments ");
					// Check that there's a assortment name entered; 071214 p�d
					// We'll add the assortment, even if there's no name added; 080703 p�d
					//
					// Or, if there's only one row in assortments, save it anyway; 080930 p�d
					if (!pRec->getColumnsInText(COLUMN_0).IsEmpty() || pRecords->GetCount() == 1)
					{

						csAssortname = pRec->getColumnsInText(COLUMN_0);
						TextToHtml(&csAssortname);

						sData += _T("assortname=\"") + csAssortname + _T("\" ");
						sData += _T("min_diam=\"") + pRec->getColumnsInText(COLUMN_2) + _T("\" ");
						sData += _T("price_m3fub=\"") + pRec->getColumnsInText(COLUMN_3) + _T("\" ");
						sData += _T("price_m3to=\"") + pRec->getColumnsInText(COLUMN_4) + _T("\" ");
						sData += _T("massa=\"") + findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1)) + _T("\" ");
						sData += _T("/>");
						arr.Add(sData);
					}
				}
				else
				{
					pRecords->RemoveAt(row);
				}
			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}	// if (pRecords && pColumns)

	return (arr.GetCount() > 0);
}

int CAvgAssortFormView::checkAssortments(CString spc_name)
{
	int row;
	double fMinDiam = 0.0;
	CString sMsg;
	CStringArray sArrSpecies;
	std::vector<double> checkValue;
	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns)
	{
		sArrSpecies.RemoveAll();
		for (row = 0;row < pRecords->GetCount()-1/* Don't check the last row*/;row++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(row);
			if (pRec)
			{
				fMinDiam = pRec->getColumnFloat(COLUMN_2);	// Min diamter for exchange in this column; 061117 p�d
				// Check if fMinDiam already is set in checkValue vector; 061117 p�d
				if (checkValue.size() > 0)
				{
					for (UINT i = 0;i < checkValue.size();i++)
					{
						if (fMinDiam == checkValue[i])
						{
							return -1;
						}	// if (fMinDiam == checkValue[i])
					}	// for (UINT i = 0;i < checkValue.size();i++)
				}	// if (checkValue.size() > 0)
				// Allow user to add zero values in Pricelist assortments.
				// A zero value equals don't use this value; 061117 p�d
				if (fMinDiam > 0.0)
					checkValue.push_back(fMinDiam);

				// Setup kriteria for an unvalid Assortment; 080304 p�d
				// Column 0 : Name of assortment
				// Column 1 : Name of Assortmenttype
				if (pRec->getColumnText(0).IsEmpty())
				{
					sArrSpecies.Add(spc_name);				
				}
			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}	// if (pRecords && pColumns)
	// Check if there's Species with assortments missing name; 071105 p�d
	if (sArrSpecies.GetCount() > 0)
	{
		sMsg = m_sAssortmentNameMissingMsg + _T("\n");
		for (int i = 0;i < sArrSpecies.GetCount();i++)
		{
			sMsg += _T(" - ") + sArrSpecies.GetAt(i) + _T("\n");
		}
		::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}
	
	return 1;
}

void CAvgAssortFormView::addAssortmentsToSelectedReportPage(int spc_id,vecTransactionAssort &vec)
{
	CString sPulpwoodTypeOf;

	if (m_wndAssortSettings.GetSafeHwnd() && vec.size() > 0)
	{
		// Clear report
		m_wndAssortSettings.ClearReport();
		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_assort rec = vec[i];		
			if (rec.getSpcID() == spc_id)
			{
				// Try to set Pulpwood type of, set in XML-file; 070608 p�d
				if (rec.getPulpType() < m_sarrPulpWoodTypes.GetCount() && rec.getPulpType() > -1)
				{
					sPulpwoodTypeOf = m_sarrPulpWoodTypes.GetAt(rec.getPulpType());
				}
				m_wndAssortSettings.AddRecord(new CAssortSettingsDataRec(this,
																																 rec.getAssortName(),
																																 rec.getMinDiam(),
																																 rec.getPriceM3fub(),
																																 rec.getPriceM3to(),
																																 sPulpwoodTypeOf));
			}	// if (rec.getSpcID() == spc_id)
		}	// for (UINT i = 0;i < vec.size();i++)

		m_wndAssortSettings.Populate();
		m_wndAssortSettings.UpdateWindow();
	}	// if (m_wndAssortSettings.GetSafeHwnd())

}

CString CAvgAssortFormView::findPulpWoodTypeIndexAsStr(LPCTSTR name)
{
	CString sValue = _T("");
	for (int i = 0;i < m_sarrPulpWoodTypes.GetCount();i++)
	{
		if (m_sarrPulpWoodTypes.GetAt(i).Compare(name) == 0)
		{
			sValue.Format(_T("%d"),i);
			return sValue;
		};
	}

	return sValue;
}

int CAvgAssortFormView::findPulpWoodTypeIndexAsInt(LPCTSTR name)
{
	for (int i = 0;i < m_sarrPulpWoodTypes.GetCount();i++)
	{
		if (m_sarrPulpWoodTypes.GetAt(i).Compare(name) == 0)
		{
			return i;
		};
	}

	return -1;
}

//Lagt in en kollfunktion som returnerar false n�got massa sortiment saknar fub pris, ber�knas det bara med fub pris
//20111122 Bug #2583 J�
BOOL CAvgAssortFormView::getPulpFubPriceIsSet()
{
	int row,nAssortTypeIndex=-1;
	BOOL bFubPriceSet=TRUE;
	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns)
	{
		for (row = 0;row < pRecords->GetCount();row++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(row);
			if (pRec)
			{
				nAssortTypeIndex=findPulpWoodTypeIndexAsInt(pRec->getColumnsInText(COLUMN_1));
				if(nAssortTypeIndex>0)
				{
					if(pRec->getColumnFloat(COLUMN_3)<=0.0)
						bFubPriceSet=FALSE;
				}
			}
		}
	}
	return bFubPriceSet;
}

//Lagt in en kollfunktion som returnerar TRUE om n�got sortiment har samma namn som ett annat
//20111228 Bug #2719 J�
BOOL CAvgAssortFormView::getAssSameName(CString &csAssName)
{
	CString strAssName=_T(""),csBuf=_T("");
	int nRow=0,nFound=0,nRow2=0;
	//CString csCalcType1=_T(""),csCalcType2=_T("");

	CXTPReportColumns *pColumns =  m_wndAssortSettings.GetColumns();
	CXTPReportRecords *pRecords = m_wndAssortSettings.GetRecords();
	if (pRecords && pColumns)
	{						
		for (nRow = 0;nRow < pRecords->GetCount();nRow++)
		{
			CAssortSettingsDataRec *pRec = (CAssortSettingsDataRec*)pRecords->GetAt(nRow);
			if (pRec)
			{
				strAssName=pRec->getColumnsInText(COLUMN_0);
				//csCalcType1=findPulpWoodTypeIndexAsStr(pRec->getColumnsInText(COLUMN_1));
				if (!strAssName.IsEmpty() || pRecords->GetCount() > 1)
				{
					nFound=0;
					for (nRow2 = 0;nRow2 < pRecords->GetCount();nRow2++)
					{
						CAssortSettingsDataRec *pRec2 = (CAssortSettingsDataRec*)pRecords->GetAt(nRow2);
						if (pRec2)
						{
							csBuf=pRec2->getColumnsInText(COLUMN_0);
							//csCalcType2=findPulpWoodTypeIndexAsStr(pRec2->getColumnsInText(COLUMN_1));
							if(!csBuf.IsEmpty())
							{
								//if(strAssName==csBuf && csCalcType1==csCalcType2)
								if(strAssName==csBuf)
									nFound++;
							}
						}
					}
					if(nFound>1)
					{
						csAssName=strAssName;
						return TRUE;
					}
				}	// if (!pRec->getColumnsInText(COLUMN_0).IsEmpty())
			}	// if (pRec)
		}	// for (row = 0;row < pRecords->GetCount();row++)	
	}
	return FALSE;	
}