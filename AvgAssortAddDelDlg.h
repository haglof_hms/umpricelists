#pragma once

#include "Resource.h"
#include "bpctrlanchormap.h"

// AvgAssortAddDelDlg dialog

class AvgAssortAddDelDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(AvgAssortAddDelDlg)

	CString m_sLangFN;

	CXTResizeGroupBox m_wndGrp1;
	CXTButton m_wndBtn1;
	CXTButton m_wndBtn2;

public:
	AvgAssortAddDelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~AvgAssortAddDelDlg();
	// Prevent Enter and Cancel on this Dialog; 060403 p�d
	virtual void OnOK()	{}
	virtual void OnCancel() {}

	void setEnable(BOOL enable)	{ m_wndBtn1.EnableWindow(enable); m_wndBtn2.EnableWindow(enable); }

// Dialog Data
	enum { IDD = IDD_DIALOGBAR4 };

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
