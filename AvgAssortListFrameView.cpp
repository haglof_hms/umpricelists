// PricelistsListFrameView.cpp : implementation file
//

#include "stdafx.h"
#include "UMPricelists.h"
#include "AvgAssortView.h"
#include "AvgAssortFormView.h"
#include "AvgAssortListFrameView.h"

#include "ResLangFileReader.h"

// CAvgAssortListFrameView

IMPLEMENT_DYNCREATE(CAvgAssortListFrameView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CAvgAssortListFrameView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_ERASEBKGND()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, IDC_PRICELISTS_LIST, OnReportClick)
END_MESSAGE_MAP()

CAvgAssortListFrameView::CAvgAssortListFrameView()
	: CXTResizeFormView(CAvgAssortListFrameView::IDD)
{
	m_bInitialized = FALSE;
}

CAvgAssortListFrameView::~CAvgAssortListFrameView()
{
	setupDoPopulate();
}

void CAvgAssortListFrameView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP

}

void CAvgAssortListFrameView::OnClose()
{
}

BOOL CAvgAssortListFrameView::OnEraseBkgnd(CDC *pDC)
{

	CRect clip;
	m_wndPricelistsList.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_BTNFACE));

	return FALSE;
}

void CAvgAssortListFrameView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndPricelistsList.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndPricelistsList,1,1,rect.right - 1,rect.bottom - 2);
	}
	
}

BOOL CAvgAssortListFrameView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CAvgAssortListFrameView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (!	m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
		m_sAbrevLangSet = getLangSet();
		CAvgAssortView *pView = (CAvgAssortView *)getFormViewByID(IDD_FORMVIEW2);
		if (pView)
		{
			m_nDBIndex = pView->getDBNavigationCounter();
			pView->saveData(CAvgAssortView::ON_SAVE);
		}

		if (setupPricelistsListReport())
		{
			addPricelistDataToReport();
		}

		m_bInitialized = TRUE;
	}	// if (!	m_bInitialized )

}

BOOL CAvgAssortListFrameView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CAvgAssortListFrameView diagnostics

#ifdef _DEBUG
void CAvgAssortListFrameView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CAvgAssortListFrameView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// Protected
void CAvgAssortListFrameView::setupDoPopulate(void)
{
	CAvgAssortView *pView = (CAvgAssortView *)getFormViewByID(IDD_FORMVIEW2);
	if (pView)
	{
		pView->doPouplate(m_nDBIndex);
	}
}

// Create and add PricelistsList reportwindow
BOOL CAvgAssortListFrameView::setupPricelistsListReport(void)
{

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndPricelistsList.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndPricelistsList.Create(this, IDC_PRICELISTS_LIST ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndPricelistsList.GetSafeHwnd() != NULL)
				{

					m_wndPricelistsList.ShowWindow( SW_NORMAL );
					pCol = m_wndPricelistsList.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING137)), 200));
					pCol->AllowRemove(FALSE);
					pCol = m_wndPricelistsList.AddColumn(new CXTPReportColumn(1, (xml->str(IDS_STRING138)), 100));
					pCol = m_wndPricelistsList.AddColumn(new CXTPReportColumn(2, (xml->str(IDS_STRING139)), 140));

					m_wndPricelistsList.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndPricelistsList.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndPricelistsList.GetReportHeader()->AllowColumnResize( TRUE );
					m_wndPricelistsList.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndPricelistsList.GetReportHeader()->SetAutoColumnSizing( FALSE );
					m_wndPricelistsList.SetMultipleSelection( FALSE );
					m_wndPricelistsList.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndPricelistsList.AllowEdit(FALSE);
					m_wndPricelistsList.FocusSubItems(TRUE);

					RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
					setResize(&m_wndPricelistsList,1,1,rect.right - 1,rect.bottom - 2);

				}	// if (m_wndPricelistsList.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;

	}	// if (fileExists(m_sLangFN))

	return TRUE;

}

BOOL CAvgAssortListFrameView::addPricelistDataToReport(void)
{

	CXTPReportRecord *pRec = NULL;
	if (m_bConnected)	
	{
		// Add species in Database to ListControl; 060328 p�d
		m_bConnected = getPricelists(m_dbConnectionData,m_vecPricelist,ID_RUNE_OLLAS_TYPEOF_PRICELIST_2);

		if (m_vecPricelist.size() > 0)
		{
			for (UINT i = 0;i < m_vecPricelist.size();i++)
			{
				if (i == m_nDBIndex)
					pRec = m_wndPricelistsList.AddRecord(new CPricelistsListReportDataRec(m_vecPricelist[i]));
				else
					m_wndPricelistsList.AddRecord(new CPricelistsListReportDataRec(m_vecPricelist[i]));
			}	// for (UINT i = 0;i < m_vecSpecies.size();i++)

			m_wndPricelistsList.Populate();
			m_wndPricelistsList.UpdateWindow();
			if (pRec)
			{
				CXTPReportRow *pRow = m_wndPricelistsList.GetRows()->Find(pRec);
				if (pRow)
				{
					m_wndPricelistsList.SetFocusedRow(pRow);
				}
			}
			return TRUE;
		}	// if (m_vecPricelist.size() > 0)
	}
	return FALSE;
}

// CAvgAssortListFrameView message handlers

void CAvgAssortListFrameView::OnReportClick(NMHDR* pNMHDR, LRESULT* pResult)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNMHDR;

	if (m_wndPricelistsList.GetSafeHwnd() == NULL)
		return;

	if (pItemNotify->pRow)
	{
		CPricelistsListReportDataRec *pRec = (CPricelistsListReportDataRec *)pItemNotify->pItem->GetRecord();
		if (pRec)
		{
			m_nDBIndex = pRec->GetIndex();
			setupDoPopulate();
		}

	}
	*pResult = 0;
}
