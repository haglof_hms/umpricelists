// DataEnterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PricelistsView.h"
#include "UMPricelists.h"
#include "DataEnterDlg.h"

#include "ResLangFileReader.h"
#include ".\dataenterdlg.h"

// CDataEnterDlg

IMPLEMENT_DYNCREATE(CDataEnterDlg, CDialog)

BEGIN_MESSAGE_MAP(CDataEnterDlg, CDialog)
	ON_WM_SIZE()
  ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, OnBnClickedButton7)
END_MESSAGE_MAP()

// ------------------------------------------------
// The following code declares our AnchorMap
// and defines how each control should be handled
// ------------------------------------------------

BEGIN_ANCHOR_MAP(CDataEnterDlg)
    ANCHOR_MAP_ENTRY(IDC_LBL1,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL2,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL3,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_EDIT1,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_EDIT2,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_EDIT3,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_BUTTON1,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_BUTTON2,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_BUTTON3,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_BUTTON4,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_BUTTON5,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_BUTTON6,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_BUTTON7,  ANF_TOP | ANF_LEFT)
END_ANCHOR_MAP()


CDataEnterDlg::CDataEnterDlg()
	: CDialog(CDataEnterDlg::IDD)
{
	m_bEnableWindows = TRUE;
}

CDataEnterDlg::~CDataEnterDlg()
{
}

void CDataEnterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GRP1, m_wndGrp1);
	DDX_Control(pDX, IDC_GRP2, m_wndGrp2);
	DDX_Control(pDX, IDC_GRP3, m_wndGrp3);
	DDX_Control(pDX, IDC_GRP4, m_wndGrp4);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtn2);
	DDX_Control(pDX, IDC_BUTTON3, m_wndBtn3);
	DDX_Control(pDX, IDC_BUTTON4, m_wndBtn4);
	DDX_Control(pDX, IDC_BUTTON5, m_wndBtn5);
	DDX_Control(pDX, IDC_BUTTON6, m_wndBtn6);
	DDX_Control(pDX, IDC_BUTTON7, m_wndBtn7);
	//}}AFX_DATA_MAP
}


BOOL CDataEnterDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CDataEnterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	setLanguage();

	m_wndEdit1.SetEnabledColor( BLACK, WHITE );
	m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit2.SetEnabledColor( BLACK, WHITE );
	m_wndEdit2.SetDisabledColor(  BLACK, COL3DFACE );
//	m_wndEdit3.SetEnabledColor( BLACK, WHITE );
//	m_wndEdit3.SetDisabledColor(  BLACK, COL3DFACE );

	m_wndBtn1.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
	m_wndBtn1.SetXButtonStyle( BS_XT_WINXP_COMPAT);

	m_wndBtn2.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
	m_wndBtn2.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndBtn3.SetBitmap(CSize(16,16),IDB_BMP_MAGNI_GLASS);
	m_wndBtn3.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndBtn4.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
	m_wndBtn4.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndBtn5.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
	m_wndBtn5.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndBtn6.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
	m_wndBtn6.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	m_wndBtn7.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
	m_wndBtn7.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	// --------------------------------------
	// At this point, we�ve set everything
	// up for our dialog except the 
	// anchoring/docking. We�ll do this now
	// with a call to InitAnchors()
	// --------------------------------------
	InitAnchors();


	return 0;
}

// CDataEnterDlg diagnostics

#ifdef _DEBUG
void CDataEnterDlg::AssertValid() const
{
	CDialog::AssertValid();
}

void CDataEnterDlg::Dump(CDumpContext& dc) const
{
	CDialog::Dump(dc);
}
#endif //_DEBUG


void CDataEnterDlg::OnSize(UINT nType,int cx,int cy)
{
  CDialog::OnSize(nType, cx, cy);
  
  CRect rcWnd;
  GetWindowRect(&rcWnd);
  
  HandleAnchors(&rcWnd);     // you can alternatively pass NULL for &rcWnd
  Invalidate(false);         // this is for ensuring the controls to be redrawn correctly 
}

BOOL CDataEnterDlg::OnEraseBkgnd(CDC* pDC) 
{

  // Here we call the EraseBackground-Handler from the
  // anchor-map which will reduce the flicker.  

  return(m_bpfxAnchorMap.EraseBackground(pDC->m_hDC));

}

// PUBLIC
void CDataEnterDlg::setDiamClass(int dcls)
{
	m_wndEdit2.setInt(dcls);
}

void CDataEnterDlg::setStartDiam(int start)
{
	m_wndEdit1.setInt(start);
}

void CDataEnterDlg::setNumOfDCLS(int num_of)
{
	m_wndEdit3.setInt(num_of);
}


// PROTECTED
void CDataEnterDlg::setLanguage(void)
{
	CString sAbrevLangSet;
	// Setup language filename; 051214 p�d
  CString sLangFN;
	sAbrevLangSet = getLangSet();
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,sAbrevLangSet,LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			m_wndGrp1.SetWindowText(xml->str(IDS_STRING1675));
			m_wndGrp2.SetWindowText(xml->str(IDS_STRING1674));
			m_wndGrp3.SetWindowText(xml->str(IDS_STRING1080));
			m_wndGrp4.SetWindowText(xml->str(IDS_STRING1676));

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING103)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING102)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING104)));

//			m_wndBtn1.SetWindowText((xml->str(IDS_STRING105)));
//			m_wndBtn2.SetWindowText((xml->str(IDS_STRING106)));
//			m_wndBtn3.SetWindowText((xml->str(IDS_STRING116)));

//			m_wndBtn4.SetWindowText((xml->str(IDS_STRING1670)));
//			m_wndBtn5.SetWindowText((xml->str(IDS_STRING1671)));
//			m_wndBtn6.SetWindowText((xml->str(IDS_STRING1672)));
//			m_wndBtn7.SetWindowText((xml->str(IDS_STRING1673)));
		}
		delete xml;
	}

}
// CPageOneFormView message handlers
// CPricelistsFormView message handlers

void CDataEnterDlg::OnBnClickedButton1()
{
	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if (pFrameWnd)
	{
		CMDIFrameDoc *pDoc = (CMDIFrameDoc*)pFrameWnd->GetActiveFrame()->GetActiveDocument();
		if (pDoc)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			if (pos != NULL)
			{
				CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
				if (pView)
				{
					pView->setupNewPricelist(CTransaction_diameterclass(0,
																															m_wndEdit1.getInt(),
												 																		  m_wndEdit2.getInt(),
																														  1 )); //m_wndEdit3.getInt()));
				}	// if (pView)
			}	// if (pos != NULL)
		}	// if (pDoc)
	}	// if (pFrameWnd)
}

void CDataEnterDlg::OnBnClickedButton2()
{
	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if (pFrameWnd)
	{
		CMDIFrameDoc *pDoc = (CMDIFrameDoc*)pFrameWnd->GetActiveFrame()->GetActiveDocument();
		if (pDoc)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			if (pos != NULL)
			{
				CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
				if (pView)
				{
					pView->removePricelist();
				}	// if (pView)
			}	// if (pos != NULL)
		}	// if (pDoc)
	}	// if (pFrameWnd)
}

void CDataEnterDlg::OnBnClickedButton3()
{
	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if (pFrameWnd)
	{
		CMDIFrameDoc *pDoc = (CMDIFrameDoc*)pFrameWnd->GetActiveFrame()->GetActiveDocument();
		if (pDoc)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			if (pos != NULL)
			{
				CPricelistsView *pView = (CPricelistsView*)pDoc->GetNextView(pos);
				if (pView)
				{
					pView->setAddOrEditQualDesc();
				}	// if (pView)
			}	// if (pos != NULL)
		}	// if (pDoc)
	}	// if (pFrameWnd)
}

void CDataEnterDlg::OnBnClickedButton4()
{
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->addRowToAssortments();

		pView = NULL;
	}	// if (pView != NULL)
}

void CDataEnterDlg::OnBnClickedButton5()
{
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->delRowFromAssortments();
		pView = NULL;
	}	// if (pView != NULL)
}

void CDataEnterDlg::OnBnClickedButton6()
{
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->addRowToPricelistAndQualities();
		pView = NULL;
	}	// if (pView != NULL)
}

void CDataEnterDlg::OnBnClickedButton7()
{
	CPricelistsView *pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->delRowFromPricelistAndQualities();
		pView = NULL;
	}	// if (pView != NULL)
}
