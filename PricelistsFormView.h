#pragma once

#include "Resource.h"

#include "SimpleSplitter.h"

// CPricelistsFormView form view

class CPricelistsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPricelistsFormView)

	//private:
	BOOL m_bInitialized;
	BOOL m_bInitializedSplitter;

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CString m_sMsgCap;
	CString m_sMsgCharError;
	CString m_sDeletePrlAndQDescRow;
	CString m_sDeleteAssortmentRow;
	CString m_sMinMaxDiameterMsg;
	CString m_sNotDeleteFirstRowMsg1;
	CString m_sNotDeleteFirstRowMsg2;

	CString m_sMsgAssNameCaption;
	CString m_sMsgAssName;
	CString m_sMsgAssName2;

protected:

	CSimpleSplitter m_wndSplitter;
	int m_nOrientation;
	int m_SplitterPaneSizes[NUMBER_OF_PANES];

//	CXTExcelTabCtrl m_wndQualDescTabCtrl;
	CMyExcelTabControl m_wndQualDescTabCtrl;
	
	CMyReportCtrl m_wndAssortSettings;
	CMyReportCtrl m_wndPricelist;
	std::vector<CMyReportCtrl *> m_vecQualDesc;
	std::vector<CMyReportCtrl *>::iterator m_vecQualDesc_iterator;

	CStringArray m_sarrPulpWoodTypes;

	// Methods
	BOOL setupAssortSettings(void);
	BOOL setupPricelistReport(void);
	BOOL setupQualDescReport(void);

	BOOL addQualDescToTabCtrl(LPCTSTR caption);

	BOOL addRow(BOOL clear = TRUE);
	BOOL updRows(void);
	BOOL delRow(void);

	BOOL addPricelistRow(CTransaction_prl_data &rec);
	BOOL addQualDescRow(CTransaction_prl_data &rec);

	BOOL addQualDescColumnsAndRows(void);

	BOOL setupAQualDescReportWnd(void);

	int getWhoHasFocus(void);

	CString findPulpWoodTypeIndexAsStr(LPCTSTR name);
	int findPulpWoodTypeIndexAsInt(LPCTSTR name);

	void getQualityNamesSetInPricelist(CStringArray& names);

public:

	CPricelistsFormView();           // protected constructor used by dynamic creation
	virtual ~CPricelistsFormView();

	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// My methods
	void setupNewPricelist(CTransaction_diameterclass &rec);
	void setupOpenPricelist(CTransaction_diameterclass &rec);
	void setupOpenPricelistAddRow(CTransaction_prl_data &rec);
	void setupOpenQualDescAddRow(CTransaction_prl_data &rec);
	void removePricelistAndQualDesc(void);
	void resetDiamterClassEntries(void);
	void setDataEnterDlgValues(void);
	void addEmptyRowToSelectedReportPage(void);
	void updRowOnSelectedReportPage(void);
	void delRowFromSelectedReportPage(void);

	void addEmptyRowToAssortment(void);
	void delRowFromAssortment(void);
	void addEmptyRowToPricelistAndQuality(void);
	void delRowFromPricelistAndQuality(void);

	void getQualDescName(CStringArray&);
	void setQualDescName(CStringArray&);
	void delQualDescName(int tab_idx,CStringArray&);

	BOOL getAssortments(CStringArray&);
	BOOL checkAssortments(int prl_status);
	BOOL getDiamClasses(LPTSTR);
	BOOL getPricelist(CStringArray&);
	BOOL getQualDesc(CStringArray&);
	//Lagt in en kollfunktion som returnerar false n�got massa sortiment saknar fub pris, ber�knas det bara med fub pris
	//20111122 Bug #2583 J�
	BOOL getPulpFubPriceIsSet();

	//Lagt in en kollfunktion som returnerar TRUE om det finns fler �n ett sortiment som har mindia samt priser satt till 0
	//20111228 Bug #2717 J�
	BOOL CPricelistsFormView::getNumZeroDiaPrice(CString &csAss);
	//Lagt in en kollfunktion som returnerar TRUE om n�got sortiment har samma namn som ett annat
	//20111227 Bug #2719 J�
	BOOL getAssSameName(CString &csAssName);
	void setPrlPriceDlgValues(void);

	void addAssortmentsToSelectedReportPage(int spc_id,vecTransactionAssort &);

	BOOL isDirty(void);
	void setIsDirty(BOOL);

	BOOL isQDescDataValid(void);

	CMyExcelTabControl* getQDescTabCtrl(void)
	{
		return &m_wndQualDescTabCtrl;
	}

	void updateSplitter(void)
	{
		if (m_wndSplitter.GetSafeHwnd())
		{
			Invalidate();
			UpdateWindow();
		}
	}

	void recalculatePricelist_up(BOOL in_percnt,int value);
	void recalculatePricelist_down(BOOL in_percnt,int value);

	void setLockAssortView(BOOL lock)	{	m_wndAssortSettings.EnableWindow(!lock); }
	void setLockPricelistView(BOOL lock)	{	m_wndPricelist.EnableWindow(!lock); }
	void setLockQualdescView(BOOL lock)	
	{	 
		if (m_vecQualDesc.size() > 0)
		{
			for (UINT i = 0;i  < m_vecQualDesc.size();i++)
			{
				m_vecQualDesc[i]->EnableWindow(!lock);
			}
		}
	}


	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnPricelistItemValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnAssortItemValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnAssortment1KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnPricelistKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


