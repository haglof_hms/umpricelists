
// UMPricelists.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "Resource.h"
#include "UMPricelists.h"
#include "MDIPricelistsFrame.h"
#include "MDIAvgAssortFrame.h"

#include "PricelistsView.h"
#include "PricelistsFormView.h"
#include "PricelistsListFrameView.h"

#include "AvgAssortView.h"
#include "AvgAssortFormView.h"
#include "AvgAssortListFrameView.h"

#include "PrlViewAndPrint.h"

static AFX_EXTENSION_MODULE UMPricelistsDLL = { NULL, NULL };

HINSTANCE hInst = NULL;

// Added: EXPORTED function; creates database tables for Pricelist; 080131 p�d
extern "C" AFX_EXT_API void DoDatabaseTables(LPCTSTR);

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMPricelists.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMPricelistsDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMPricelists.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMPricelistsDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMPricelistsDLL);
	CString sLangFN;
	CString sAdminIniFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	// Form view to enter data

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIPricelistsFrame),
			RUNTIME_CLASS(CPricelistsView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW1,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIPricelistsListFrame),
			RUNTIME_CLASS(CPricelistsListFrameView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW1,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,
			RUNTIME_CLASS(CMDIAvgAssortDoc),
			RUNTIME_CLASS(CMDIAvgAssortFrame),
			RUNTIME_CLASS(CAvgAssortView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW2,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
			RUNTIME_CLASS(CMDIAvgAssortDoc),
			RUNTIME_CLASS(CMDIAvgAssortListFrame),
			RUNTIME_CLASS(CAvgAssortListFrameView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW3,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW4,
			RUNTIME_CLASS(CMDIPrlViewAndPrintDoc),
			RUNTIME_CLASS(CMDIPrlViewAndPrintFrame),
			RUNTIME_CLASS(CPrlViewAndPrint)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW4,suite,sLangFN,TRUE));


	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo((HMODULE)hInst,VER_NUMBER);
	sCopyright	= getVersionInfo((HMODULE)hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo((HMODULE)hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,2 /* User module */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));

	// Do a check to see if database tables are created; 080131 p�d
	DoDatabaseTables(_T(""));	// Empty arg = use deafult database; 081001 p�d
}

void DoDatabaseTables(LPCTSTR db_name)
{
	// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{

		runSQLScriptFileEx((TBL_PRICELISTS),(table_Pricelist),(db_name));
/*
		CString sPath;
		sPath.Format("%s\\%s\\%s\\%s",getProgDir(),SUBDIR_SCRIPTS,SQL_SERVER_SQRIPT_DIRECTORY,PRICELIST_TABLE);
		runSQLScriptFile(sPath,TBL_PRICELISTS);
*/
	}

}


