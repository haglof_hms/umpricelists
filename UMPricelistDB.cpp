#include "StdAfx.h"

#include "UMPricelistDB.h"

//////////////////////////////////////////////////////////////////////////////////
// CUMPricelistDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d

CUMPricelistDB::CUMPricelistDB(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CUMPricelistDB::CUMPricelistDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CUMPricelistDB::CUMPricelistDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

// PRIVATE
BOOL CUMPricelistDB::pricelistExist(CTransaction_pricelist &rec)
{
	CString sSQL;
	sSQL.Format(_T("select * from %s where id=%d"),
		TBL_PRICELISTS,rec.getID());
	return exists(sSQL);
}


// PUBLIC
BOOL CUMPricelistDB::getPricelists(vecTransactionPricelist &vec,int prl_type)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where type_of=:1 or type_of=:2"),
			TBL_PRICELISTS);
		m_saCommand.setCommandText((SAString)sSQL);
		// Check which type of pricelist was called; 080114 p�d
		m_saCommand.Param(1).setAsShort()		= prl_type;
		if (prl_type == ID_RUNE_OLLAS_TYPEOF_PRICELIST_1)
		{
			m_saCommand.Param(2).setAsShort()		= ID_PRICELIST_IN_DEVELOPEMENT_1;	// Also get not finished pricelists; 071219 p�d
		}
		if (prl_type == ID_RUNE_OLLAS_TYPEOF_PRICELIST_2)
		{
			m_saCommand.Param(2).setAsShort()		= ID_PRICELIST_IN_DEVELOPEMENT_2;	// Also get not finished pricelists; 071219 p�d
		}
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(6).asDateTime();
			vec.push_back(CTransaction_pricelist(m_saCommand.Field(1).asShort(),
																	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMPricelistDB::addPricelist(CTransaction_pricelist &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!pricelistExist(rec))
		{
			sSQL.Format(_T("insert into %s (name,type_of,pricelist,created_by) values(:1,:2,:3,:4)"),
									TBL_PRICELISTS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getName();
			m_saCommand.Param(2).setAsShort()		= rec.getTypeOf();
			m_saCommand.Param(3).setAsLongChar()	= rec.getPricelistFile();
			m_saCommand.Param(4).setAsString()	= rec.getCreatedBy();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			m_saConnection.Commit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMPricelistDB::updPricelist(CTransaction_pricelist &rec)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
		if (pricelistExist(rec))
		{
/*
			S.Format("updPricelist\nName %s\nID %d\n%s",
				rec.getName(),
				rec.getID(),
				rec.getPricelistFile());
			AfxMessageBox(S);
*/
			sSQL.Format(_T("update %s set name=:1,created_by=:2,pricelist=:3,type_of=:4 where id=:5"),
									 TBL_PRICELISTS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getName();
			m_saCommand.Param(2).setAsString()	= rec.getCreatedBy();
			m_saCommand.Param(3).setAsLongChar()	= rec.getPricelistFile();
			m_saCommand.Param(4).setAsShort()		= rec.getTypeOf();
			m_saCommand.Param(5).setAsShort()		= rec.getID();

			m_saCommand.Execute();	
			m_saConnection.Commit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMPricelistDB::delPricelist(CTransaction_pricelist &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (pricelistExist(rec))
		{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_PRICELISTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.getID();

			m_saCommand.Execute();	
			m_saConnection.Commit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMPricelistDB::updGeneralData(CTransaction_pricelist &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (pricelistExist(rec))
		{

			sSQL.Format(_T("update %s set name=:1,created_by=:2 where id=:3"),
									 TBL_PRICELISTS);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getName();
			m_saCommand.Param(2).setAsString()	= rec.getCreatedBy();
			m_saCommand.Param(3).setAsShort()		= rec.getID();

			m_saCommand.Execute();	
			m_saConnection.Commit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

// Get species in species table in active database; 061116 p�d
BOOL CUMPricelistDB::getSpecies(vecTransactionSpecies &vec)
{
	CString sSQL;
	int nCnt = 0;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by spc_id"),TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
		
			vec.push_back(CTransaction_species(nCnt,
				m_saCommand.Field(1).asShort(),
				//#HMS-94 l�sa in p30spec info 20220919 J�
				m_saCommand.Field(_T("p30_spec")).asShort(),
				(LPCTSTR)m_saCommand.Field(2).asString(),
				(LPCTSTR)m_saCommand.Field(3).asString(),
				(LPCTSTR)m_saCommand.Field(4).asString() ));
			nCnt++;

		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}


	return TRUE;
}


BOOL CUMPricelistDB::getTemplates(vecTransactionTemplate &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asShort(),
																	(LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
																	(LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	(LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	(LPCTSTR)m_saCommand.Field(6).asString(),
																  (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMPricelistDB::isPricelistInStand(LPCTSTR prl_name,int type_of)
{
	CString sSQL;
	BOOL bFound = FALSE;
	try
	{
	
		sSQL.Format(_T("select 1 from %s where tprl_name=\'%s\' and tprl_typeof=%d"),TBL_TRAKT_MISC_DATA,prl_name,type_of);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			bFound = TRUE;
			break;
		}

		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return bFound;
}
