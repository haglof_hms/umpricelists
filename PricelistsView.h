#if !defined(__PRICELISTSVIEW_H__)
#define __PRICELISTSVIEW_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MDIPricelistsFrame.h"

#include "PricelistsFormView.h"
#include "PricelistsListFrameView.h"

#include "UMPricelistDB.h"

// CPricelistsView view
class CMDIFrameDoc;

class CPricelistsView : public CView
{
	DECLARE_DYNCREATE(CPricelistsView)



//private:
	BOOL m_bInitialized;
	BOOL m_bIsEnabledData;

	BOOL m_bIspricelistLocked;

	CFont m_fntTab;

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMissingInfoMsg;
	CString m_sMissingInfoMsg1;
	CString m_sMissingInfoMsg2;
	CString m_sMissingInfoMsg3;
	CString m_sMissingInfoMsg4;
	CString m_sPricelistExistsMsg;
	CString m_sNoPricelistExistsMsg;
	CString m_sPricelistErrMsg;

	CString m_sStatusLbl;
	CString m_sStatusLblOK;
	CString m_sStatusLblDevelop;

	CTransaction_pricelist m_recSelectedPricelist;
	vecTransactionPricelist m_vecPricelist;
	vecTransactionSpecies m_vecSpecies;
	vecTransactionAssort m_vecAssort;
	vecTransactionDiameterclass m_vecDiamterclass;
	vecTransactionPrlData m_vecPrlData;
	vecTransactionPrlData m_vecQualDescData;

	vecTransactionSpecies m_vecSpeciesInDB;

	CStringArray m_arrQualDescNames;

	enumPricelistState m_enumPricelistState;

	/*UINT*/int m_nDBNavigationCounter;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL m_bQDescChanged;

	int m_nPricelistStatus;

protected:
	CPricelistsView();           // protected constructor used by dynamic creation
	virtual ~CPricelistsView();

	BOOL setupNewPricelist(void);
	void setupOpenPricelist(void);

	void addRowToSelectedReportPage(void);
	void updRowOnSelectedReportPage(void);
	void delRowFromSelectedReportPage(void);

	BOOL checkAssortments(void);
	
	void setToDoOnNewBtn(void);
	void setToDoOnSaveBtn(void);
	void setToDoOnDelBtn(void);

	void actionDBNavigationMessage(WPARAM id);
	void setupDBNaivgationToolBar(BOOL start_prev,BOOL end_next);

	void openPricelistListFormView(void);

	void openPopupMenuForQualityTab(void);


public:

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate( );
	protected:
	//}}AFX_VIRTUAL
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:

	BOOL saveXMLFileFromPricelistInfoToDB(bool only_check = false);

	typedef enum {ON_SAVE,ON_PREV_NEXT,ON_QUIT} enumSaveMsg;

	CMDIFrameDoc* GetDocument();

	CMyTabControl m_wndTabControl;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int spc_id, int nIcon);
	void UpdateDocTitle();

	void setEnableWindows(BOOL enabled,BOOL w1_also,BOOL lock);

	BOOL setupNewPricelist(CTransaction_diameterclass &rec);
	BOOL removePricelist(void);
	BOOL setDataEnterDlgValues(void);
	BOOL setGeneralInfoDlgValues(BOOL lock);
	BOOL setAddOrEditQualDesc(void);
	BOOL setChangePrlPriceDlgValues(void);

	BOOL setupPricelistOnStartup(void);

	BOOL getPricelistXML(CString &name,CString &data);

	BOOL savePricelistToXMLFile(LPCTSTR fn,LPCTSTR data);
	BOOL saveFromXMLFileToPricelistDB(LPCTSTR xml_fn);

	BOOL addSpecieToPricelist(void);
	BOOL removeSpecieFromPricelist(void);

	BOOL removePricelistFromDB(void);

	void doPouplate(UINT idx);
	int getDBNavigationCounter(void);
	int getNumOfItemsInPricelist(void);

	BOOL isOKToClose(void);

	BOOL saveData(enumSaveMsg show_msg_type);
	BOOL isPriceListInStandTemplate();

	BOOL getXMLHeaderData(LPTSTR name,LPTSTR done_by,LPTSTR date,LPTSTR notes,int *price_in,int *status);

	// Check if entered name havn't already been used; 071217 p�d
	short isPricelistNameOK(LPCTSTR name,enumSaveMsg show_msg_type);

	void addRowToAssortments(void);
	void delRowFromAssortments(void);
	void addRowToPricelistAndQualities(void);
	void delRowFromPricelistAndQualities(void);

	void calcPriceToPricelist(BOOL calc_up,BOOL in_percent,int value);

	void doSetNavigationBar();
protected:
	//{{AFX_MSG(CTabbedViewView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
};


#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDIFrameDoc* CPricelistsView::GetDocument()
	{ return (CMDIFrameDoc*)m_pDocument; }
#endif


#endif