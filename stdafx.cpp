// stdafx.cpp : source file that includes just the standard includes
// UMPricelists.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "DBBaseClass_ADODirect.h"	// ... in DBTransaction_lib

#include "ResLangFileReader.h"

#include "PricelistsView.h"
#include "PricelistsFormView.h"

#include "AvgAssortView.h"
#include "AvgAssortFormView.h"

//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

CString getResStr(UINT id)
{
	CString sText;
	sText.LoadString(id);

	return sText;
}

CString getToolBarResourceFN(void)
{
	CString sPath;
	CString sToolBarResPath;
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}

CString getDBExportedFuncName(enumFuncNames enum_func)
{
	typedef char *(*Func)(enumFuncNames);
	Func proc;
	CString	sFuncName;
	CString sPath;
	sPath.Format(_T("%s%s"),getModulesDir(),UMDATABASE_NAME);
	HINSTANCE hInst = AfxLoadLibrary(sPath);
	if (hInst != NULL)
	{
		proc = (Func)GetProcAddress((HMODULE)hInst, (GET_EXPORTED_FUNC_NAME) );
		if (proc != NULL)
		{
			// call the function
			sFuncName = proc(enum_func);
		}	// if (proc != NULL)
		AfxFreeLibrary(hInst);
	}	// if (hInst != NULL)

	return sFuncName;
}

BOOL getSpecies(DB_CONNECTION_DATA &conn,vecTransactionSpecies &vec)
{
	BOOL bReturn = FALSE;
	CUMPricelistDB *pDB = new CUMPricelistDB(conn);
	if (pDB != NULL)
	{
		if (pDB->getSpecies(vec))
			bReturn = TRUE;

	}	// if (pDB->connectToDataBase())
	delete pDB;
	return (vec.size() > 0 && bReturn);
}

BOOL savePricelistToDB(DB_CONNECTION_DATA &conn,vecTransactionPricelist &vec)
{
	//CString S;
	BOOL bReturn = FALSE;
	CUMPricelistDB *pDB = new CUMPricelistDB(conn);
	if (pDB != NULL)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
		/*		
			S.Format(L"ID %d\nNamn %s",vec[i].getID(),vec[i].getName());
			AfxMessageBox(S);
		*/
			if (!pDB->addPricelist(vec[i]))
				pDB->updPricelist(vec[i]);
		}
		delete pDB;
		bReturn = TRUE;
	}	// if (pDB != NULL)
	return bReturn;
}

BOOL savePricelistToDB(DB_CONNECTION_DATA &conn,CTransaction_pricelist& rec)
{
	BOOL bReturn = FALSE;
	CUMPricelistDB *pDB = new CUMPricelistDB(conn);
	if (pDB != NULL)
	{
			if (!pDB->addPricelist(rec))
				pDB->updPricelist(rec);
		delete pDB;
		bReturn = TRUE;
	}	// if (pDB != NULL)
	return bReturn;
}

BOOL removePricelistFromDatabase(DB_CONNECTION_DATA &conn,CTransaction_pricelist &rec)
{
	BOOL bReturn = FALSE;
	CUMPricelistDB *pDB = new CUMPricelistDB(conn);
	if (pDB != NULL)
	{
		pDB->delPricelist(rec);
		delete pDB;
		bReturn = TRUE;
	}	// if (pDB != NULL)
	return bReturn;
}

BOOL getPricelists(DB_CONNECTION_DATA &conn,vecTransactionPricelist &vec,int prl_type)
{
	BOOL bReturn = FALSE;
	CUMPricelistDB *pDB = new CUMPricelistDB(conn);
	if (pDB != NULL)
	{
		if (pDB->getPricelists(vec,prl_type))
			bReturn = TRUE;
		delete pDB;
	}	// if (pDB != NULL)
	return bReturn;
}

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(MSG_IN_SUITE2,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

CWnd *getFormViewParentByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

// Create database tables form SQL scriptfiles; 061109 p�d
#define BUFFER_SIZE		1024*100		// 100 kb buffer
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	UINT nFileSize;
	TCHAR sBuffer[BUFFER_SIZE];	// 10 kb buffer

	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString S;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,check_table))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(sBuffer);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!db_name.IsEmpty())
					_tcscpy_s(sDBName,127,db_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,table_name))
								{
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}
void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), rsource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}


void setGlobalLock_matris(BOOL lock)
{
	int nNumOfPages = 0;
	CPricelistsFormView *pView = NULL;
	// Get status of pricelist, set in ComboBox on GeneralInfoDialog; 080114 p�d
	CMDIPricelistsFrame *pMDIChild = (CMDIPricelistsFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pMDIChild != NULL)
	{
		pMDIChild->setToolbarBtns(TRUE ,lock);
	}

	// Add information on End diamterclass, diameterclass and
	// number of on last entered; 070208 p�d
	CXTPTabManagerItem *pSelectedPage = NULL;
	CPricelistsView* pViewPrl = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pViewPrl != NULL)
	{
		nNumOfPages = pViewPrl->m_wndTabControl.getNumOfTabPages();
		if (nNumOfPages > 0)
		{
			for (int i = 0;i < nNumOfPages;i++)
			{
				pSelectedPage = pViewPrl->m_wndTabControl.getTabPage(i);

				if (pSelectedPage)
				{
					// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
					if ((pView = DYNAMIC_DOWNCAST(CPricelistsFormView,CWnd::FromHandle(pSelectedPage->GetHandle()))))
					{
						pView->setLockAssortView(lock);
						pView->setLockPricelistView(lock);
						pView->setLockQualdescView(lock);
					}	// if (pView)
				}	// if (pSelectedPage)
			}
		}	// if (nNumOfPages > 0)
		pViewPrl->setEnableWindows(TRUE,FALSE,lock);
	}	// if (pViewPrl != NULL)

}

void setGlobalLock_avg(BOOL lock)
{
	int nNumOfPages = 0;
	CAvgAssortFormView *pView = NULL;

	// Get status of pricelist, set in ComboBox on GeneralInfoDialog; 080114 p�d
	CMDIAvgAssortFrame *pMDIChild = (CMDIAvgAssortFrame *)getFormViewParentByID(IDD_FORMVIEW2);
	if (pMDIChild != NULL)
	{
		pMDIChild->setToolbarBtns(TRUE,lock);
	}

	// Add information on End diamterclass, diameterclass and
	// number of on last entered; 070208 p�d
	CXTPTabManagerItem *pSelectedPage = NULL;
	CAvgAssortView* pViewPrl = (CAvgAssortView*)getFormViewByID(IDD_FORMVIEW2);
	if (pViewPrl != NULL)
	{
		nNumOfPages = pViewPrl->m_wndTabControl.getNumOfTabPages();
		if (nNumOfPages > 0)
		{
			for (int i = 0;i < nNumOfPages;i++)
			{
				pSelectedPage = pViewPrl->m_wndTabControl.getTabPage(i);

				if (pSelectedPage)
				{
					// Get a pointer to CPricelistsFormView, do the actual settings of Priclist and QualityDesc; 060324 p�d
					if ((pView = DYNAMIC_DOWNCAST(CAvgAssortFormView,CWnd::FromHandle(pSelectedPage->GetHandle()))))
					{
						pView->setLockAssortView(lock);
					}	// if (pView)
				}	// if (pSelectedPage)
			}
		}	// if (nNumOfPages > 0)
		pViewPrl->setEnableWindows(TRUE,FALSE,lock);
	}	// if (pViewPrl != NULL)
}

BOOL checkPricelist(LPCTSTR prl_xml)
{
	vecTransactionSpecies vecSpc;
	CTransaction_species recSpc;
	vecTransactionAssort vecAss;
	CTransaction_assort recAss;
	std::map<short,short> mapCnt;
	BOOL bReturn = TRUE;

	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	// Check pricelist, to see that there's only one pulpassortmant for exchangecalculations; 101018 p�d
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(prl_xml))
		{
			pPrlParser->getAssortmentPerSpecie(vecAss);
			pPrlParser->getSpeciesInPricelistFile(vecSpc);

			if (vecAss.size() > 0 && vecSpc.size() > 0)
			{
				for (UINT i1 = 0;i1 < vecSpc.size();i1++)
				{
					recSpc = vecSpc[i1];
					mapCnt[recSpc.getSpcID()] = 0;
					for (UINT i2 = 0;i2 < vecAss.size();i2++)
					{
						recAss = vecAss[i2];
						if (recAss.getMinDiam() > 0.0 && recAss.getPulpType() > 0 && recAss.getSpcID() == recSpc.getSpcID())
						{
							mapCnt[recSpc.getSpcID()]++;
						}
					}	// for (UINT i2 = 0;i2 < vecAss.size();i2++)
				}	// for (UINT i1 = 0;i1 < vecSpc.size();i1++)

				//-------------------------------------------------------------------------
				for (UINT i3 = 0;i3 < vecSpc.size();i3++)
				{
					recSpc = vecSpc[i3];
					if (mapCnt[recSpc.getSpcID()] > 1)
					{
						bReturn = FALSE;
					}
				}
				//-------------------------------------------------------------------------
			}	// if (vecAss.size() > 0 && vecSpc.size() > 0)
		}
		delete pPrlParser;
	}

	return bReturn;
}

BOOL checkPricelist_matris()
{
	CPricelistsView* pView = (CPricelistsView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{	
		return pView->saveXMLFileFromPricelistInfoToDB(true);
	}	// if (pViewPrl != NULL)

	return FALSE;
}

BOOL checkPricelist_avg()
{
	CAvgAssortView* pView = (CAvgAssortView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{	
		return pView->saveXMLFileFromPricelistInfoToDB(true);
	}	// if (pViewPrl != NULL)

	return FALSE;
}
