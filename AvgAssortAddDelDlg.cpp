// AvgAssortAddDelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMPricelists.h"
#include "AvgAssortAddDelDlg.h"
#include "AvgAssortView.h"

#include "ResLangFileReader.h"

// AvgAssortAddDelDlg dialog

IMPLEMENT_DYNAMIC(AvgAssortAddDelDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(AvgAssortAddDelDlg, CXTResizeDialog)
	ON_WM_PAINT()
  ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON1, &AvgAssortAddDelDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &AvgAssortAddDelDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


AvgAssortAddDelDlg::AvgAssortAddDelDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(AvgAssortAddDelDlg::IDD, pParent)
{

}

AvgAssortAddDelDlg::~AvgAssortAddDelDlg()
{
}

void AvgAssortAddDelDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AvgAssortAddDelDlg)
	DDX_Control(pDX, IDC_GRP1, m_wndGrp1);
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtn2);
	//}}AFX_DATA_MAP
}

BOOL AvgAssortAddDelDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL AvgAssortAddDelDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	m_wndBtn1.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
	m_wndBtn1.SetXButtonStyle( BS_XT_WINXP_COMPAT);

	m_wndBtn2.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
	m_wndBtn2.SetXButtonStyle( BS_XT_WINXP_COMPAT );

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_wndGrp1.SetWindowText(xml.str(IDS_STRING1674));

			xml.clean();
		}

	}

	return 0;
}

BOOL AvgAssortAddDelDlg::OnEraseBkgnd(CDC* pDC) 
{

  // Here we call the EraseBackground-Handler from the
  // anchor-map which will reduce the flicker.  

  return TRUE;
}

void AvgAssortAddDelDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

  CRect WndRect;
  GetClientRect(WndRect);
	dc.FillSolidRect(WndRect,GetSysColor(COLOR_3DFACE));

	CXTResizeDialog::OnPaint();
}


// AvgAssortAddDelDlg message handlers
// Add empty row to assortment
void AvgAssortAddDelDlg::OnBnClickedButton1()
{
	CAvgAssortView *pView = (CAvgAssortView *)getFormViewByID(IDD_FORMVIEW2);
	if (pView)
	{
		pView->addRowToAssortmentPerSpecies();
	}
}

// Delete row from assortment
void AvgAssortAddDelDlg::OnBnClickedButton2()
{
	CAvgAssortView *pView = (CAvgAssortView *)getFormViewByID(IDD_FORMVIEW2);
	if (pView)
	{
		pView->delRowFromAssortmentPerSpecies();
	}
}
