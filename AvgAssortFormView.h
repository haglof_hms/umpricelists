#pragma once

#include "Resource.h"


class CTextItem : public CXTPReportRecordItemText
{
//private:
	CWnd *m_wnd;
	CString m_sText;
public:
	CTextItem(CString sValue,CWnd*);

	virtual	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText);

	CString getTextItem(void);

	void setTextItem(LPCTSTR text);
};

class CAssortSettingsDataRec : public CXTPReportRecord
{
	//private:
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

public:
	CAssortSettingsDataRec(void)
	{
		m_nIndex = 0;
		AddItem(new CTextItem(_T(""),NULL));
		AddItem(new CTextItem(_T(""),NULL));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz2dec));
		AddItem(new CFloatItem(0.0,sz2dec));
	}

	CAssortSettingsDataRec(CWnd *wnd,
												 LPCTSTR qual_name,
												 double min_diam,
												 double price_m3fub,
												 double price_m3to,
												 LPCTSTR pulp_type)
	{
		AddItem(new CTextItem((qual_name),wnd));
		AddItem(new CTextItem((pulp_type),wnd));
		AddItem(new CFloatItem(min_diam,sz1dec));
		AddItem(new CFloatItem(price_m3fub,sz2dec));
		AddItem(new CFloatItem(price_m3to,sz2dec));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}
	// Special method; returns all colum items as text; 060405 p�d
	CString getColumnsInText(int item)	
	{ 
		CString sData;
		switch (item)
		{
			case 0 : sData = getColumnText(item); break;
			case 1 : sData = getColumnText(item); break;
			case 2 : sData.Format(sz1dec,getColumnFloat(item)); break;
			case 3 : sData.Format(sz2dec,getColumnFloat(item)); break;
			case 4 : sData.Format(sz2dec,getColumnFloat(item)); break;
		};
		return sData;
	}

	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}
};


// CAvgAssortFormView form view

class CAvgAssortFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CAvgAssortFormView)

	//private:
	BOOL m_bInitialized;

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CString m_sMsgCap;
	CString m_sDeleteAssortmentRow;
	CString m_sRemoveAssortmentMsg;
	CString m_sAssortmentNameMissingMsg;
	CString m_sMinMaxDiameterMsg;
	CString m_sNotDeleteFirstRowMsg;

	CString m_sMsgTeckenfel;
	CString m_sMsgAssNameCaption;
	CString m_sMsgAssName;
	CString m_sMsgAssName2;

protected:

	CMyReportCtrl m_wndAssortSettings;

	CStringArray m_sarrPulpWoodTypes;

	// Methods
	BOOL setupAssortSettings(void);

	BOOL addRow(BOOL clear = TRUE);
	BOOL delRow(void);

	CString findPulpWoodTypeIndexAsStr(LPCTSTR name);
	int findPulpWoodTypeIndexAsInt(LPCTSTR name);

public:
	
	CAvgAssortFormView();           // protected constructor used by dynamic creation
	virtual ~CAvgAssortFormView();

	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// My methods
	void setupOpenPricelistAddRow(CTransaction_prl_data &rec);
	void setupOpenQualDescAddRow(CTransaction_prl_data &rec);
	void addEmptyRowToSelectedReportPage(void);
	void delRowFromSelectedReportPage(void);

	BOOL getAssortments(CStringArray&,int);
	int checkAssortments(CString spc_name);

	void addAssortmentsToSelectedReportPage(int spc_id,vecTransactionAssort &);

	BOOL isDirty(void);
	void setIsDirty(BOOL);

	BOOL isThereAssortments(void);

	//Lagt in en kollfunktion som returnerar false n�got massa sortiment saknar fub pris, ber�knas det bara med fub pris
	//20111122 Bug #2583 J�
	BOOL getPulpFubPriceIsSet();

	//Lagt in en kollfunktion som returnerar TRUE om n�got sortiment har samma namn som ett annat
	//20111228 Bug #2719 J�
	BOOL getAssSameName(CString &csAssName);

	void setLockAssortView(BOOL lock)	{	m_wndAssortSettings.EnableWindow(!lock); }

	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnAssortment2KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg LRESULT OnCmd(WPARAM,LPARAM);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


