#pragma once

#include "Resource.h"

#include "ResLangFileReader.h"

// CChangePrlPriceDlg dialog

class CChangePrlPriceDlg : public CDialog
{
	DECLARE_DYNAMIC(CChangePrlPriceDlg)

	CXTResizeGroupBox m_wndGrp1;
	CMyExtEdit m_wndEdit1;

	CStatic m_wndLbl1;
	CStatic m_wndLbl2;

	CButton m_wndRadio1;
	CButton m_wndRadio2;

	CXTButton m_wndBtn1;
	CXTButton m_wndBtn2;

	RLFReader xml;

	BOOL m_bEnableWindows;

public:
	CChangePrlPriceDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangePrlPriceDlg();

	// Prevent Enter and Cancel on this Dialog; 090414 p�d
	virtual void OnOK()	{}
	virtual void OnCancel() {}


// Dialog Data
	enum { IDD = IDD_DIALOGBAR3 };

	inline void setValue(int v)
	{
		m_wndEdit1.setInt(v);
	}

	void setEnableWindows(BOOL enabled)
	{
		m_bEnableWindows = enabled;

		m_wndEdit1.EnableWindow( m_bEnableWindows );
		m_wndEdit1.SetReadOnly(!m_bEnableWindows);
	
		m_wndRadio1.EnableWindow( m_bEnableWindows );
		m_wndRadio2.EnableWindow( m_bEnableWindows );
		m_wndBtn1.EnableWindow( m_bEnableWindows );
		m_wndBtn2.EnableWindow( m_bEnableWindows );
	}


protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CPageOneFormView)
	afx_msg void OnSize(UINT nType,int cx,int cy);
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedButton21();
	afx_msg void OnBnClickedButton22();
};
